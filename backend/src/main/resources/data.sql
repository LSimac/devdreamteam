insert into CATEGORIES
(id, name) values
    (1, 'Bars'),
    (2, 'Buildings'),
    (3, 'History'),
    (4, 'Restaurants'),
    (5, 'Hotels');


insert into ROLES
 values (1, 'normal'),
        (2, 'professional'),
        (3, 'admin');

insert into users (id, pass_hash, username, role_id)
values (1, '$2a$10$Z/g0j4889zP0O6YlE.PuMOxN4IdNzYbzDbdicOqG3pD.eZQtiH1Ra', 'admin', 3); -- pass is "admin"

insert into QUESTION_TYPES
 (id, name) values (1, 'public'), (2, 'professional');

insert into QUESTION_STATUSES
 (id, name) values (1, 'open'), (2, 'closed');
