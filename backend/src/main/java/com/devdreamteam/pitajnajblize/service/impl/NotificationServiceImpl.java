package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.NotificationDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.dto.mapper.LocationDtoMapper;
import com.devdreamteam.pitajnajblize.dto.mapper.NotificationDtoMapper;
import com.devdreamteam.pitajnajblize.entity.Notification;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.exception.ForbiddenException;
import com.devdreamteam.pitajnajblize.exception.UnauthorizedException;
import com.devdreamteam.pitajnajblize.repository.NotificationDao;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import com.devdreamteam.pitajnajblize.service.DistanceDeciderService;
import com.devdreamteam.pitajnajblize.service.NotificationService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class NotificationServiceImpl implements NotificationService {

    private final DistanceDeciderService distanceDeciderService;

    private final NotificationDao notificationDao;
    private final UserDao userDao;
    private final QuestionDao questionDao;

    private final LocationDtoMapper locationDtoMapper;
    private final NotificationDtoMapper notificationDtoMapper;

    @Autowired
    public NotificationServiceImpl(DistanceDeciderService distanceDeciderService, NotificationDao notificationDao,
                                   UserDao userDao, QuestionDao questionDao, LocationDtoMapper locationDtoMapper, NotificationDtoMapper notificationDtoMapper) {
        this.distanceDeciderService = Objects.requireNonNull(distanceDeciderService);
        this.notificationDao = Objects.requireNonNull(notificationDao);
        this.userDao = Objects.requireNonNull(userDao);
        this.questionDao = Objects.requireNonNull(questionDao);
        this.locationDtoMapper = Objects.requireNonNull(locationDtoMapper);
        this.notificationDtoMapper = Objects.requireNonNull(notificationDtoMapper);
    }

    @Override
    public void notifyForQuestion(QuestionDto question) {
        Set<User> eligibleUsers = getEligibleUsersForNotification(question);
        filterAlreadyNotified(eligibleUsers, question.getId(), "question");
        notifyUsers(
                eligibleUsers,
                question.getId(),
                "question",
                String.format(
                        "Please help %s and answer his question - question id: %d",
                        question.getMessage().getAuthor().getUsername(),
                        question.getId()
                )
        );
    }

    private Set<User> getEligibleUsersForNotification(QuestionDto question) {
        if (question.getType().equals("professional")) {
            return new HashSet<>(userDao.findAllById(question.getProfessionalIds()));
        }
        return userDao.findAll().stream().filter(u -> {
            if (u.getLocation() == null) {
                return false;
            }
            return distanceDeciderService.isQuestionNearLocation(question, locationDtoMapper.apply(u.getLocation()));
        }).collect(Collectors.toSet());
    }

    private void filterAlreadyNotified(Set<User> users, long entityReferenceId, String entityReferenceType) {
        users.removeIf(user -> notificationDao.existsByRecipientAndEntityReferenceIdAndEntityReferenceType(user, entityReferenceId, entityReferenceType));
    }

    private void notifyUsers(Set<User> users, long entityReferenceId, String entityReferenceType, String description) {
        users.forEach(u -> {
            Notification n = new Notification();
            n.setRecipient(u);
            n.setCreationTimestamp(new Date());
            n.setDescription(description);
            n.setEntityReferenceId(entityReferenceId);
            n.setEntityReferenceType(entityReferenceType);
            notificationDao.save(n);
        });
    }

    @Override
    public void notifyForNewAnswer(AnswerDto answer) {
        Set<User> eligibleUsers = getEligibleUsersForNotification(answer);
        filterAlreadyNotified(eligibleUsers, answer.getId(), "answer");
        notifyUsers(
                eligibleUsers,
                answer.getId(),
                "answer",
                String.format(
                        "%s answered your question - question id: %d",
                        answer.getMessage().getAuthor().getUsername(),
                        answer.getQuestionId()
                )
        );
    }

    @Override
    public Set<NotificationDto> getNotificationsForUser(long userId) {
        verifyCanFetchNotificationsForUser(userId);
        User user = userDao.findById(userId).orElseThrow(EntityNotFoundException::new);
        Set<NotificationDto> result = new LinkedHashSet<>();
        notificationDao.findAllByRecipientOrderByCreationTimestampDesc(user).stream()
                .map(notificationDtoMapper)
                .forEach(result::add);
        return result;
    }

    private void verifyCanFetchNotificationsForUser(long userId) {
        UserDto loggedUser = SessionUtil.getLoggedUser();
        if (loggedUser == null) {
            throw new UnauthorizedException();
        }
        if (loggedUser.getId() != userId) {
            throw new ForbiddenException();
        }
    }


    private Set<User> getEligibleUsersForNotification(AnswerDto answer) {
        Question question = questionDao.findById(answer.getQuestionId()).orElseThrow(IllegalArgumentException::new);
        User author = question.getMessage().getAuthor();
        Set<User> result = new HashSet<>();
        result.add(author);
        return result;
    }
}
