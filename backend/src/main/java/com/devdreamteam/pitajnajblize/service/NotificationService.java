package com.devdreamteam.pitajnajblize.service;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.NotificationDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;

import java.util.Set;

/**
 * @author Jan Capek
 */
public interface NotificationService {

    /**
     * @throws NullPointerException If given question is {@code null}
     */
    void notifyForQuestion(QuestionDto question);

    /**
     * @throws NullPointerException If given answer is {@code null}
     */
    void notifyForNewAnswer(AnswerDto answer);

    /**
     * @return Notifications for user with given id sorted by creation time decreasingly.
     */
    Set<NotificationDto> getNotificationsForUser(long userId);
}
