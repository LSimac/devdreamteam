package com.devdreamteam.pitajnajblize.service;

import com.devdreamteam.pitajnajblize.dto.ImageDto;

import java.awt.image.BufferedImage;

/**
 * Service interface handling anything regarding images.
 *
 * @author Jan Capek
 */
public interface ImageService {

    /**
     * @param imageId Id of the image to be returned.
     * @return ImageDto object containing real path to the image on server.
     * @throws com.devdreamteam.pitajnajblize.exception.UnauthorizedException   If logged user is unauthorized to see image.
     * @throws com.devdreamteam.pitajnajblize.exception.EntityNotFoundException If requested image does not exist.
     */
    ImageDto getImage(long imageId);

    /**
     * @param imageId Id of the image to be deleted.
     * @throws com.devdreamteam.pitajnajblize.exception.UnauthorizedException If logged user is unauthorized to delete image.
     */
    void deleteImage(long imageId);

    /**
     * @param image Multipart file containing image.
     * @return Newly created ImageDto.
     * @throws com.devdreamteam.pitajnajblize.exception.UnauthorizedException If logged user is unauthorized to create image.
     * @throws com.devdreamteam.pitajnajblize.exception.InvalidDataException  If file type is unsupported.
     */
    ImageDto createImage(BufferedImage image, String formatName);
}
