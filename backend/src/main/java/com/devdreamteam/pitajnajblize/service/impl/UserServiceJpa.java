package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.*;
import com.devdreamteam.pitajnajblize.dto.mapper.UserDtoMapper;
import com.devdreamteam.pitajnajblize.dto.validator.UserDtoValidator;
import com.devdreamteam.pitajnajblize.entity.Ban;
import com.devdreamteam.pitajnajblize.entity.Location;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.professional.Grade;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import com.devdreamteam.pitajnajblize.exception.*;
import com.devdreamteam.pitajnajblize.repository.*;
import com.devdreamteam.pitajnajblize.service.UserService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.websocket.Session;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation of {@link UserService}
 *
 * @author Marko Ivić
 * @version 1.0.0
 */
@Service
public class UserServiceJpa implements UserService {

    private final UserDao userDao;
    private final RoleDao roleDao;
    private final LocationDao locationDao;
    private final CategoryDao categoryDao;
    private final QuestionDao questionDao;
    private final BanDao  banDao;
    private final ImageDao imageDao;

    private final PasswordEncoder passwordEncoder;
    private final UserDtoMapper userDtoMapper;
    private final UserDtoValidator userDtoValidator;
    @Value("${setting.default-role}")
    private String DEFAULT_ROLE;

    @Autowired
    public UserServiceJpa(UserDao userDao, RoleDao roleDao, LocationDao locationDao, CategoryDao categoryDao,
                          QuestionDao questionDao, PasswordEncoder passwordEncoder, UserDtoMapper userDtoMapper, UserDtoValidator userDtoValidator, BanDao banDao, ImageDao imageDao) {
        this.userDao = Objects.requireNonNull(userDao);
        this.roleDao = Objects.requireNonNull(roleDao);
        this.locationDao = Objects.requireNonNull(locationDao);
        this.categoryDao = Objects.requireNonNull(categoryDao);
        this.questionDao = Objects.requireNonNull(questionDao);
        this.passwordEncoder = Objects.requireNonNull(passwordEncoder);
        this.userDtoMapper = Objects.requireNonNull(userDtoMapper);
        this.userDtoValidator = Objects.requireNonNull(userDtoValidator);
        this.banDao = banDao;
        this.imageDao = imageDao;
    }

    @Override
    public Set<UserDto> getAllProfessionalUsers() {
        Set<User> professionalUsers = userDao.findAll().stream().filter(u -> u.getRole().getName().equals("professional")).collect(Collectors.toSet());
        return mapUserSet(professionalUsers);
    }

    @Override
    public Set<BanDto> getAllBannedUsers() {
        isCurrentUserAdmin();

        Set<BanDto> listOfBannedUser = banDao.findAll().stream().map(b -> {
           BanDto dto = new BanDto();
           dto.setDate(Objects.isNull(b.getExpirationDate())? null : b.getExpirationDate().toString());
           dto.setUser(userDtoMapper.apply(b.getUser()));
           return dto;
        }).collect(Collectors.toSet());

        return listOfBannedUser;
    }

    @Override
    public void unbanUser(Long userId) {
        isCurrentUserAdmin();

        User user = userDao.getUserById(userId);

        if(Objects.isNull(user)){
            return;
        }

        Ban ban = banDao.findBanByUserId(user.getId());

        banDao.delete(ban);

    }

    @Override
    public void banUser(Long userId, String expiresOn) {
        isCurrentUserAdmin();

        User user = userDao.getUserById(userId);

        validateUserBan(user, expiresOn);

        Ban ban = new Ban();

        if(Objects.isNull(expiresOn)){
            ban.setExpirationDate(null);
        }else{
            Instant instant = Instant.parse(expiresOn);
            ban.setExpirationDate(Date.from(instant));
        }

        ban.setUser(user);

        banDao.save(ban);
    }

    private void isCurrentUserAdmin(){
        UserDto currentUser = SessionUtil.getLoggedUser();

        if(Objects.isNull(currentUser)){
            throw new UnauthorizedException();
        }

        if(currentUser.getRole().equals("admin") == false){
            throw new UnauthorizedException();
        }
    }

    private void validateUserBan(User user, String expiresOn){
        if(Objects.isNull(user)){
            throw new InvalidDataException("User does not exists");
        }

        if(Objects.isNull(expiresOn) == false){
            Instant instant = null;
            try {
                instant = Instant.parse(expiresOn);
            }catch (DateTimeParseException ex){
                throw new InvalidDataException();
            }

            if(Date.from(instant).getTime() <= new Date().getTime()){
                throw new InvalidDataException();
            }
        }

        Ban banTemp = banDao.findBanByUserId(user.getId());

        if(Objects.isNull(banTemp) == false){
            throw new InvalidDataException("");
        }
    }

    @Override
    public UserDto getUser(long userId) {
        return userDtoMapper.apply(getUserById(userId));
    }

    @Override
    public Set<UserDto> getAllUsers() {
        UserDto currentUser = SessionUtil.getLoggedUser();

        if(currentUser.getRole().equals("admin") == false){
            throw new UnauthorizedException();
        }

        Set<User> users = new HashSet<>(userDao.findAll().stream().filter(u -> !u.getRole().getName().equals("admin")).collect(Collectors.toSet())) ;
        return mapUserSet(users);
    }

    private Set<UserDto> mapUserSet(Set<User> user) {
        return user.stream().map(userDtoMapper).collect(Collectors.toSet());
    }

    @Override
    public UserDto createUser(RegisterLoginDto user) {
        if (!user.getPassword().equals(user.getRepeatedPassword())) {
            throw new InvalidDataException("Given passwords do not match.");
        } else if (userDao.getUserByUsername(user.getUsername()) != null) {
            throw new InvalidDataException("User with this username already exists.");
        }
        if (userDao.getUserByUsername(user.getUsername()) != null) {
            throw new DuplicateEntryException("Username is taken.");
        }
        User userModel = new User();
        userModel.setUsername(user.getUsername());
        userModel.setPassHash(passwordEncoder.encode(user.getPassword()));
        userModel.setRole(roleDao.getByName(DEFAULT_ROLE));
        userModel = userDao.save(userModel);
        return userDtoMapper.apply(userModel);
    }

    @Override
    public UserDto updateUser(long userId, UserDto updatedUser) {
        if (userDtoValidator.test(updatedUser) == false || updatedUser.getId() != userId) {
            throw new InvalidDataException("Updated data is invalid!");
        }

        checkCanUpdate(userId, updatedUser);
        try {
            return doUpdate(userId, updatedUser);
        } catch (HibernateException e) {
            throw new InvalidDataException(e);
        }
    }

    /**
     * @param userId Id of the user who is to be updated.
     * @throws UnauthorizedException If user is unauthorized.
     * @throws ForbiddenException    If user is forbidden to perform update.
     */
    private void checkCanUpdate(long userId, UserDto updatedUser) {
        if (roleDao.getByName(updatedUser.getRole()) == null) {
            throw new InvalidDataException("Given role does not exist.");
        }

        UserDto loggedUser = SessionUtil.getLoggedUser();

        if (loggedUser == null) {
            throw new UnauthorizedException();
        }
        if (loggedUser.getId() != userId) {
            throw new ForbiddenException();
        }
        if (updatedUser.getRole().equals("admin") && loggedUser.getRole().equals("admin") == false) {
            throw new ForbiddenException("Only admin can promote new admins");
        }

        if(updatedUser.getRole().equals("normal") && loggedUser.getRole().equals("normal") == false){
            throw new ForbiddenException("You can not demote user");
        }

    }

    /**
     * @throws org.hibernate.HibernateException If new user data is invalid from database perspective.
     */
    private UserDto doUpdate(long userId, UserDto updatedUser) {
        User user = getUserById(userId);
        user.setId(updatedUser.getId());
        user.setUsername(updatedUser.getUsername());
        user.setRole(roleDao.getByName(updatedUser.getRole()));
        if(Objects.isNull(updatedUser.getAvatarId()) == false){
            user.setImage(imageDao.findById(updatedUser.getAvatarId()).orElseThrow(InvalidDataException::new));
        }
//        todo(jcapek_): set image
        updateUserCategories(user, updatedUser);
        updateUserLocation(user, updatedUser);
        updateUserDescription(user, updatedUser);
        userDao.flush();
        return userDtoMapper.apply(userDao.findById(updatedUser.getId()).orElseThrow());
    }

    private void updateUserCategories(User user, UserDto updatedUser) {
        if (updatedUser.getCategories() == null) {
            user.setCategories(null);
            return;
        }
        user.getCategories().clear();
        user.getCategories().addAll(
                categoryDao.findAllById(updatedUser.getCategories().stream()
                        .map(CategoryDto::getId)
                        .collect(Collectors.toList()))
        );
    }

    private void updateUserLocation(User user, UserDto updatedUser) {
        if (updatedUser.getLocation() == null) {
            user.setLocation(null);
            return;
        }
        if (user.getLocation() == null) {
            user.setLocation(locationDao.save(new Location(updatedUser.getLocation().getLatitude(), updatedUser.getLocation().getLongitude())));
            return;
        }
        user.getLocation().setLatitude(updatedUser.getLocation().getLatitude());
        user.getLocation().setLongitude(updatedUser.getLocation().getLongitude());
    }

    private void updateUserDescription(User user, UserDto updatedUser) {
        user.setDescription(updatedUser.getDescription());
    }

    @Override
    public void gradeProfessional(long professionalId, long questionId, int grade) {
        User prof = userDao.findById(professionalId).orElseThrow(EntityNotFoundException::new);
        Question question = questionDao.findById(questionId).orElseThrow(EntityNotFoundException::new);
        verifyGrading(prof, question, grade);

        Grade g = new Grade();
        g.setGrade(grade);
        g.setProfessional(prof);
        g.setQuestion(question);
        prof.getGrades().remove(g);
        userDao.flush();
        prof.getGrades().add(g);
        userDao.flush();
    }

    /**
     * @throws InvalidDataException
     * @throws UnauthorizedException
     * @throws ForbiddenException
     */
    private void verifyGrading(User gradee, Question question, int grade) {
        if (gradee.getRole().getName().equals("professional") == false) {
            throw new InvalidDataException("Non professional user can not be graded!");
        }
        if (question.getProfessionalWhoClaimed().getId().equals(gradee.getId()) == false) {
            throw new InvalidDataException("Professional cannot be graded if they didn't claim the question!");
        }
        UserDto loggedUser = SessionUtil.getLoggedUser();
        if (loggedUser == null) {
            throw new UnauthorizedException();
        } else if (loggedUser.getId().equals(question.getMessage().getAuthor().getId()) == false) {
            throw new ForbiddenException("Only question's creator can grade a professional for the question!");
        }
        if (grade < 1 || grade > 5) {
            throw new InvalidDataException("Grade must be in interval [1, 5]!");
        }
    }

    /**
     * @throws com.devdreamteam.pitajnajblize.exception.EntityNotFoundException If user could not be found.
     */
    private User getUserById(long id) {
        return userDao.findById(id).orElseThrow(
                () -> {
                    throw new com.devdreamteam.pitajnajblize.exception.EntityNotFoundException();
                }
        );
    }
}
