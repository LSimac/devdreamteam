package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.ImageDto;
import com.devdreamteam.pitajnajblize.dto.mapper.ImageDtoMapper;
import com.devdreamteam.pitajnajblize.entity.Image;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.repository.ImageDao;
import com.devdreamteam.pitajnajblize.service.ImageService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Service
public class ImageServiceImpl implements ImageService {

    private final ImageDao imageDao;
    private final ImageDtoMapper imageDtoMapper;
    @Value("${setting.image.folder-path}")
    private String folderPath;
    @Value("#{'${setting.image.supported-formats}'.split(',')}")
    private List<String> supportedFormats;
    @Value("${setting.image.name-length}")
    private int LENGTH;
    @Value("${setting.image.name-has-letters}")
    private boolean HAS_LETTERS;
    @Value("${setting.image.name-has-numbers}")
    private boolean HAS_NUMBERS;

    @Autowired
    public ImageServiceImpl(ImageDao imageDao, ImageDtoMapper imageDtoMapper) {
        this.imageDao = imageDao;
        this.imageDtoMapper = imageDtoMapper;
    }


    @Override
    public ImageDto getImage(long imageId) {
        Optional<Image> image = imageDao.findById(imageId);
        if (image.isEmpty()) {
            throw new InvalidDataException("Image with this id is unavailable.");
        }
        return imageDtoMapper.apply(image.get());
    }

    @Override
    public void deleteImage(long imageId) {
        imageDao.deleteById(imageId);
    }

    @Override
    public ImageDto createImage(BufferedImage image, String formatName) {
        if (!supportedFormats.contains(formatName)) {
            throw new InvalidDataException("Unsupported image format.");
        }
        String name;
        do {
            name = RandomStringUtils.random(LENGTH, HAS_LETTERS, HAS_NUMBERS);
        } while (imageDao.findByPath(folderPath + name + "." + formatName) != null);
        try {
            Path path = Paths.get(folderPath + name + "." + formatName);
            ImageIO.write(image, "png", path.toFile());
        } catch (IOException e) {
            throw new InvalidDataException("Image cannot be saved.");
        }
        Image imageModel = new Image();
        imageModel.setPath(folderPath + name + "." + formatName);
        imageModel = imageDao.save(imageModel);
        return imageDtoMapper.apply(imageModel);
    }
}
