package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.TokenDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.dto.mapper.TokenDtoMapper;
import com.devdreamteam.pitajnajblize.dto.mapper.UserDtoMapper;
import com.devdreamteam.pitajnajblize.entity.Token;
import com.devdreamteam.pitajnajblize.repository.TokenDao;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import com.devdreamteam.pitajnajblize.security.TokenUtil;
import com.devdreamteam.pitajnajblize.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Service
public class AuthServiceImpl implements AuthService {

    private final TokenUtil tokenUtil;
    private final TokenDao tokenDao;
    private final UserDao userDao;
    private final UserDtoMapper userDtoMapper;
    private final TokenDtoMapper tokenDtoMapper;
    @Value("${security.token-validity-time}")
    private long VALIDITY_TIME;

    /**
     * @throws NullPointerException If any of the params is {@code null}
     */
    @Autowired
    public AuthServiceImpl(TokenUtil tokenUtil, TokenDao tokenDao, UserDao userDao, UserDtoMapper userDtoMapper, TokenDtoMapper tokenDtoMapper) {
        this.tokenUtil = Objects.requireNonNull(tokenUtil);
        this.tokenDao = Objects.requireNonNull(tokenDao);
        this.userDao = Objects.requireNonNull(userDao);
        this.userDtoMapper = Objects.requireNonNull(userDtoMapper);
        this.tokenDtoMapper = Objects.requireNonNull(tokenDtoMapper);
    }

    @Override
    public TokenDto generateToken(long userId) {
        String tokenValue = generateUniqueToken();
        Token token = new Token();
        token.setToken(tokenValue);
        token.setUserId(userId);
        token.setValidUntil((new Timestamp(new Date().getTime() + VALIDITY_TIME)));
        tokenDao.save(token);
        return tokenDtoMapper.apply(token);
    }

    private String generateUniqueToken() {
        String token;
        do {
            token = tokenUtil.generateTokenValue();
        } while (tokenDao.findByToken(token) != null);
        return token;
    }

    @Override
    public UserDto validateToken(String tokenValue) {
        Token token = tokenDao.findByToken(tokenValue);
        if (token == null) {
            return null;
        }
        if (dateIsInvalid(token)) {
            deleteToken(token.getToken());
            return null;
        }
        return userDtoMapper.apply(userDao.getUserById(token.getUserId()));
    }

    private boolean dateIsInvalid(Token token) {
        return token.getValidUntil().getTime() < (new Date()).getTime();
    }

    @Override
    public void deleteToken(String tokenValue) {
        Token token = tokenDao.findByToken(tokenValue);
        if (token == null) {
            return;
        }
        tokenDao.deleteByToken(tokenValue);
    }
}
