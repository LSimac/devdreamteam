package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.LocationDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import com.devdreamteam.pitajnajblize.service.DistanceDeciderService;
import com.devdreamteam.pitajnajblize.util.LocationDistance;
import org.springframework.stereotype.Service;

@Service
public class DistanceDeciderServiceImpl implements DistanceDeciderService {

    private static final double QUESTION_DEFAULT_DISTANCE = 500f;

    private final QuestionDao questionDao;

    public DistanceDeciderServiceImpl(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }

    public boolean isQuestionNearLocation(QuestionDto question, LocationDto location) {
        int questionRangeLevel = questionDao.findById(question.getId()).orElseThrow(EntityNotFoundException::new).getLevel();
        return isQuestionNearLocation(question.getLocation(), location, questionRangeLevel);
    }

    public boolean isQuestionNearLocation(LocationDto questionLocation, LocationDto otherLocation, int questionRangeLevel) {
        double distance = LocationDistance.calculateDistanceInMeters(
                questionLocation.getLongitude(),
                questionLocation.getLatitude(),
                otherLocation.getLongitude(),
                otherLocation.getLatitude()
        );
        double questionRangeDistance = QUESTION_DEFAULT_DISTANCE * Math.pow(questionRangeLevel, 2);
        return distance <= questionRangeDistance;
    }
}
