package com.devdreamteam.pitajnajblize.service;

import com.devdreamteam.pitajnajblize.dto.LocationDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;

public interface DistanceDeciderService {

    boolean isQuestionNearLocation(QuestionDto question, LocationDto location);

    boolean isQuestionNearLocation(LocationDto questionLocation, LocationDto otherLocation, int questionRangeLevel);
}
