package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.LocationDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.dto.mapper.LocationDtoMapper;
import com.devdreamteam.pitajnajblize.dto.mapper.QuestionDtoMapper;
import com.devdreamteam.pitajnajblize.dto.patcher.QuestionDtoPatcher;
import com.devdreamteam.pitajnajblize.dto.validator.QuestionDtoValidator;
import com.devdreamteam.pitajnajblize.entity.Category;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.mapper.QuestionEntityDtoMapper;
import com.devdreamteam.pitajnajblize.entity.mapper.UserEntityDtoMapper;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.exception.ForbiddenException;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.exception.UnauthorizedException;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import com.devdreamteam.pitajnajblize.service.DistanceDeciderService;
import com.devdreamteam.pitajnajblize.service.NotificationService;
import com.devdreamteam.pitajnajblize.service.QuestionService;
import com.devdreamteam.pitajnajblize.util.LocationDistance;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final NotificationService notificationService;

    private final QuestionDtoMapper questionDtoMapper;
    private final QuestionEntityDtoMapper questionEntityDtoMapper;
    private final UserEntityDtoMapper userEntityDtoMapper;
    private final LocationDtoMapper locationDtoMapper;

    private final QuestionDtoPatcher questionDtoPatcher;

    private final QuestionDao questionDao;
    private final UserDao userDao;

    private final QuestionDtoValidator questionDtoValidator;

    private final DistanceDeciderService distanceDeciderService;

    @Autowired
    public QuestionServiceImpl(NotificationService notificationService, QuestionDtoMapper questionDtoMapper,
                               QuestionDao questionDao, QuestionEntityDtoMapper questionEntityDtoMapper,
                               UserEntityDtoMapper userEntityDtoMapper, QuestionDtoPatcher questionDtoPatcher,
                               UserDao userDao, QuestionDtoValidator questionDtoValidator, LocationDtoMapper locationDtoMapper, DistanceDeciderService distanceDeciderService) {
        this.notificationService = Objects.requireNonNull(notificationService);
        this.questionDtoMapper = Objects.requireNonNull(questionDtoMapper);
        this.questionDao = Objects.requireNonNull(questionDao);
        this.questionEntityDtoMapper = Objects.requireNonNull(questionEntityDtoMapper);
        this.userEntityDtoMapper = Objects.requireNonNull(userEntityDtoMapper);
        this.questionDtoPatcher = questionDtoPatcher;
        this.userDao = Objects.requireNonNull(userDao);
        this.questionDtoValidator = questionDtoValidator;
        this.locationDtoMapper = locationDtoMapper;
        this.distanceDeciderService = distanceDeciderService;
    }

    @Override
    public List<QuestionDto> getProfessionalQuestion(Set<Long> categoryIdFilters) {
        authorizedCheck();

        return new ArrayList<>(getQuestionsFromUser(SessionUtil.getLoggedUser().getId(),categoryIdFilters)).stream().filter(q -> q.getType().equals("professional")).sorted((q1,q2) -> Long.compare(q1.getId(),q2.getId())).collect(Collectors.toList());
    }

    @Override
    public Set<QuestionDto> getProfessionalsDirectQuestions(long professionalId, Set<Long> categoryIdFilters) {
        Set<Question> questions = questionDao.findAll()
                .stream()
                .filter(q -> q.getType().getName().equals("professional"))
                .filter(q -> q.getQueriedProfessionals().stream().map(u -> u.getId()).collect(Collectors.toList()).contains(professionalId))
                .filter(q -> Objects.isNull(q.getProfessionalWhoClaimed()) || q.getProfessionalWhoClaimed().getId().equals(professionalId))
                .collect(Collectors.toSet());

        if(categoryIdFilters != null){
            questions = filterByCategories(questions, categoryIdFilters);
        }

        return mapQuestionSet(questions);
    }

    @Override
    public Set<QuestionDto> getAllQuestions(Set<Long> categoryIdFilters) {
        Set<Question> questions = new HashSet<>(questionDao.findAll(Sort.by(Sort.Direction.DESC, "id")));
        if (categoryIdFilters != null) {
            questions = filterByCategories(questions, categoryIdFilters);
        }

        return mapQuestionSet(questions);
    }

    @Override
    public Set<QuestionDto> getQuestionsFromUser(long userId, Set<Long> categoryIdFilters) {
        authorizedCheck();
        Set<Question> questionsByUser = questionDao.findQuestionsByUserId(userId);
        if (categoryIdFilters != null) {
            questionsByUser = filterByCategories(questionsByUser, categoryIdFilters);
        }

        Set<QuestionDto> questionDtoByUser = new TreeSet<>((q1, q2) -> Long.compare(q1.getId(), q2.getId()));
        questionDtoByUser.addAll(mapQuestionSet(questionsByUser));
        return questionDtoByUser;
    }

    @Override
    public List<QuestionDto> getQuestionsNearUser(long userId, Set<Long> categoryIdFilters) {
        authorizedCheck();

        List<Question> questions = questionDao.findAll();
        final LocationDto currentUserLocation = SessionUtil.getLoggedUser().getLocation();

        if (Objects.isNull(currentUserLocation)) {
            return new ArrayList<>();
        }

        List<Question> questionsNearUser = questions.stream().filter(q -> distanceDeciderService.isQuestionNearLocation(currentUserLocation, locationDtoMapper.apply(q.getLocation()), q.getLevel())).collect(Collectors.toList());
        questionsNearUser = questionsNearUser.stream().filter(q -> q.getType().getName().equals("public")).collect(Collectors.toList());

        if (categoryIdFilters != null) {
            questionsNearUser = filterByCategories(questionsNearUser, categoryIdFilters);
        }

        Collections.sort(questionsNearUser,(q1, q2) -> {
            LocationDto q1Location = locationDtoMapper.apply(q1.getLocation());
            LocationDto q2Location = locationDtoMapper.apply(q2.getLocation());
            return Double.compare(
                    LocationDistance.calculateDistanceInMeters(q1Location.getLongitude(), q1Location.getLatitude(),
                            currentUserLocation.getLongitude(), currentUserLocation.getLatitude()),
                    LocationDistance.calculateDistanceInMeters(q2Location.getLongitude(), q2Location.getLatitude(),
                            currentUserLocation.getLongitude(), currentUserLocation.getLatitude())
            );
        });

        return mapQuestionList(questionsNearUser);
    }

    @Override
    public QuestionDto getQuestion(long questionId) {
        authorizedCheck();
        QuestionDto question = questionDtoMapper.apply(getQuestionById(questionId));
        UserDto loggedUser = SessionUtil.getLoggedUser();

        return question;
    }

    @Override
    public QuestionDto createQuestion(QuestionDto question) {
        authorizedCheck();
        Long currentUserId = SessionUtil.getLoggedUser().getId();

        if (currentUserId.equals(question.getMessage().getAuthor().getId()) == false) {
            throw new UnauthorizedException("User ID is not valid");
        }

        validateQuestionDto(question);
        Question newQuestion = createNewQuestion(question);
        QuestionDto questionDto = questionDtoMapper.apply(newQuestion);
        notificationService.notifyForQuestion(questionDto);
        return questionDto;
    }

    @Override
    public QuestionDto updateQuestion(long questionId, QuestionDto updatedQuestion) {
        authorizedCheck();
        return doQuestionUpdate(questionId, updatedQuestion);
    }

    private QuestionDto doQuestionUpdate(long questionId, QuestionDto updatedQuestion) {
        QuestionDto questionDto = questionDtoMapper.apply(getQuestionById(questionId));

        QuestionDto modifiedQuestion = questionDtoPatcher.apply(questionDto, updatedQuestion);
        authorChangeValidation(questionDto, modifiedQuestion);
        validateQuestionDto(modifiedQuestion);

        Question updatedQuestionEntity = questionDao.save(questionEntityDtoMapper.apply(modifiedQuestion));

        return questionDtoMapper.apply(updatedQuestionEntity);
    }

    private void authorChangeValidation(QuestionDto question, QuestionDto updatedQuestion) {
        Long currentUserId = SessionUtil.getLoggedUser().getId();
        Set<Long> updatedProfessionalList = updatedQuestion.getProfessionalIds();
        Set<Long> professionalList = question.getProfessionalIds();

        if (updatedQuestion.getType().equals("professional")) {
            if (currentUserId.equals(updatedQuestion.getMessage().getAuthor().getId())) {
                if (professionalList.size() != updatedProfessionalList.size()) {
                    throw new UnauthorizedException("User does not permission to change that fields");
                }
            } else if (currentUserId.equals(updatedQuestion.getIdOfProfessionalWhoClaimed())) {
                if (updatedQuestion.getIdOfProfessionalWhoClaimed().equals(question.getIdOfProfessionalWhoClaimed())) {
                    throw new UnauthorizedException("User does not permission to change that fields");
                }
            } else {
                if (professionalList.size() - updatedProfessionalList.size() != 1) {
                    throw new UnauthorizedException("User does not permission to change that fields");
                }
            }
        } else {
            if (currentUserId.equals(updatedQuestion.getMessage().getAuthor().getId()) == false) {
                throw new UnauthorizedException("User does not permission to change that fields");
            }
        }
    }

    @Override
    public void deleteQuestion(long questionId) {
        authorizedCheck();

        UserDto user = SessionUtil.getLoggedUser();
        Question question = questionDao.findById(questionId).orElse(null);

        if (question != null && (question.getMessage().getAuthor().getId().equals(user.getId())
                || user.getRole().equals("admin"))) {
            questionDao.deleteById(questionId);
        }
    }

    @Override
    public Set<QuestionDto> getProfessionalClaimedQuestions(long professionalId, Set<Long> categoryIdFilters) {
        authorizedCheck();
        User user = userDao.getUserById(professionalId);

        if (user.getRole().getName().equals("professional") == false) {
            throw new ForbiddenException();
        }

        Set<Question> claimedQuestions = questionDao.findQuestionsByProfessionalWhoClaimed(professionalId);

        if (Objects.isNull(categoryIdFilters) == false) {
            claimedQuestions = filterByCategories(claimedQuestions, categoryIdFilters);
        }

        Set<QuestionDto> claimedQuestionsDto = new TreeSet<>((q1, q2) -> Long.compare(q1.getId(), q2.getId()));
        claimedQuestionsDto.addAll(mapQuestionSet(claimedQuestions));

        return claimedQuestionsDto;
    }

    private void validateQuestionDto(QuestionDto question) {
        if (!questionDtoValidator.test(question)) {
            throw new InvalidDataException();
        }
    }

    private Question createNewQuestion(QuestionDto questionDto) {
        Question question = this.questionEntityDtoMapper.apply(questionDto);
        return questionDao.save(question);
    }

    private Question getQuestionById(long questionId) {
        return questionDao.findById(questionId).orElseThrow(EntityNotFoundException::new);
    }

    private void authorizedCheck() {
        if (!isUserLogged()) {
            throw new UnauthorizedException();
        }
    }

    private boolean isUserLogged() {
        return SessionUtil.getLoggedUser() != null;
    }

    private Set<Question> filterByCategories(Set<Question> questions, Set<Long> categoriesId) {
        return questions.stream().filter(q -> {
            Set<Long> questionCategories = q.getCategory().stream()
                    .map(Category::getId).collect(Collectors.toSet());
            for (long categoryId : categoriesId) {
                if (questionCategories.contains(categoryId)) {
                    return true;
                }
            }
            return false;
        }).collect(Collectors.toSet());
    }

    private List<Question> filterByCategories(List<Question> questions, Set<Long> categoriesId) {
        return questions.stream().filter(q -> {
            Set<Long> questionCategories = q.getCategory().stream()
                    .map(Category::getId).collect(Collectors.toSet());
            for (long categoryId : categoriesId) {
                if (questionCategories.contains(categoryId)) {
                    return true;
                }
            }
            return false;
        }).collect(Collectors.toList());
    }

    private Set<QuestionDto> mapQuestionSet(Set<Question> questions) {
        return questions.stream().map(questionDtoMapper).collect(Collectors.toSet());
    }

    private List<QuestionDto> mapQuestionList(List<Question> questions) {
        return questions.stream().map(questionDtoMapper).collect(Collectors.toList());
    }
}
