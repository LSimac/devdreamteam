package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.dto.mapper.AnswerDtoMapper;
import com.devdreamteam.pitajnajblize.dto.patcher.AnswerDtoPatcher;
import com.devdreamteam.pitajnajblize.dto.validator.AnswerDtoValidator;
import com.devdreamteam.pitajnajblize.entity.mapper.AnswersEntityDtoMapper;
import com.devdreamteam.pitajnajblize.entity.mapper.MessageEntityDtoMapper;
import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.exception.UnauthorizedException;
import com.devdreamteam.pitajnajblize.repository.AnswerDao;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import com.devdreamteam.pitajnajblize.service.AnswerService;
import com.devdreamteam.pitajnajblize.service.NotificationService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tomislav Kurtovic
 */
@Service
public class AnswerServiceImpl implements AnswerService {

    private final NotificationService notificationService;

    private final AnswerDao answerDao;
    private final QuestionDao questionDao;
    private final AnswerDtoMapper answerDtoMapper;
    private final AnswerDtoPatcher answerDtoPatcher;
    private final AnswersEntityDtoMapper answersEntityDtoMapper;
    private final AnswerDtoValidator answerDtoValidator;

    @Autowired
    public AnswerServiceImpl(AnswerDao answerDao, QuestionDao questionDao, AnswerDtoMapper answerDtoMapper, MessageEntityDtoMapper messageEntityDtoMapper, NotificationService notificationService, AnswerDtoPatcher answerDtoPatcher, AnswersEntityDtoMapper answersEntityDtoMapper, AnswerDtoValidator answerDtoValidator) {
        this.answerDao = Objects.requireNonNull(answerDao);
        this.questionDao = questionDao;
        this.answerDtoMapper = Objects.requireNonNull(answerDtoMapper);
        this.notificationService = notificationService;
        this.answerDtoPatcher = answerDtoPatcher;
        this.answersEntityDtoMapper = answersEntityDtoMapper;
        this.answerDtoValidator = answerDtoValidator;
    }

    @Override
    public Set<AnswerDto> getAnswersForQuestion(long questionId) {

//        Comparator<AnswerDto> byAcceptance = (a1, a2) -> {
//            if(a1.getIsAccepted() == a2.getIsAccepted()){
//                return 0;
//            }
//            return a1.getIsAccepted() ? -1 : 1;
//        };
//
//        Comparator<AnswerDto> byCreation = Comparator.comparingLong(a -> Long.parseLong(a.getTimestamp()));

        return questionDao.findById(questionId)
                .orElseThrow(InvalidDataException::new)
                .getAnswers()
                .stream()
                .map(answerDtoMapper)
                .collect(Collectors.toSet());
    }

    @Override
    public AnswerDto createAnswer(AnswerDto answer) {
        Long currentUserId = SessionUtil.getLoggedUser().getId();

        if (currentUserId.equals(answer.getMessage().getAuthor().getId()) == false) {
            throw new UnauthorizedException("User ID is not valid");
        }

        verifyDtoValid(answer);

        Answer answerEntity = answersEntityDtoMapper.apply(answer);
        answerEntity = answerDao.save(answerEntity);

        answer = answerDtoMapper.apply(answerEntity);
        notificationService.notifyForNewAnswer(answer);
        return answer;
    }


    @Override
    public AnswerDto getAnswer(long answerId) {
        return answerDtoMapper.apply(answerDao.getOne(answerId));
    }

    @Override
    public AnswerDto updateAnswer(long answerId, AnswerDto updatedAnswer) {
        Answer tempAnswer = answerDao.findById(answerId).orElseThrow(InvalidDataException::new);

        AnswerDto answer = answerDtoMapper.apply(tempAnswer);

        AnswerDto update = answerDtoPatcher.apply(answer, updatedAnswer);
        authorChangeValidation(answer, update);

        verifyDtoValid(update);

        Answer updated = answersEntityDtoMapper.apply(update);
        updated.getMessage().setId(tempAnswer.getMessage().getId());

        Answer updateEntity = answerDao.save(updated);

        return answerDtoMapper.apply(updateEntity);
    }

    private void authorChangeValidation(AnswerDto answer, AnswerDto updatedAnswer) {
        Long currentUserId = SessionUtil.getLoggedUser().getId();

        if (updatedAnswer.getIsAccepted() != answer.getIsAccepted()) {
            Long idAuthorQuestion = questionDao.findById(updatedAnswer.getQuestionId()).map(q -> q.getMessage().getAuthor().getId()).get();

            if (currentUserId.equals(idAuthorQuestion) == false) {
                throw new UnauthorizedException("Only owner of question can accept answer!");
            }

        } else if (currentUserId.equals(answer.getMessage().getAuthor().getId()) == false) {
            throw new UnauthorizedException("User is not allowed to update answer because it is not owner");
        }
    }


    @Override
    public void deleteAnswer(long answerId) {
        UserDto currentUser= SessionUtil.getLoggedUser();
        Answer answer = answerDao.findById(answerId).orElse(null);

        if(answer != null && (answer.getMessage().getAuthor().getId().equals(currentUser.getId())
                || currentUser.getRole().equals("admin"))){
            answerDao.deleteById(answerId);
        }
    }

    private void verifyDtoValid(AnswerDto answer) {
        if (!answerDtoValidator.test(answer)) {
            throw new InvalidDataException("Answer is not valid");
        }
    }
}
