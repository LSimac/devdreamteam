package com.devdreamteam.pitajnajblize.service;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;

import java.util.Set;

/**
 * Service interface handling anything regarding categories.
 *
 * @author Jan Capek
 */
public interface CategoryService {

    /**
     * @return All available categories.
     * @throws com.devdreamteam.pitajnajblize.exception.UnauthorizedException If a user is not authorized.
     * @throws com.devdreamteam.pitajnajblize.exception.ForbiddenException    If a user is forbidden from performing an action.
     */
    Set<CategoryDto> getAll();

    /**
     * @return Requested category.
     * @throws com.devdreamteam.pitajnajblize.exception.UnauthorizedException   If a user is not authorized.
     * @throws com.devdreamteam.pitajnajblize.exception.ForbiddenException      If a user is forbidden from performing an action.
     * @throws com.devdreamteam.pitajnajblize.exception.EntityNotFoundException If the category with given id could not be found.
     */
    CategoryDto getById(long id);
}
