package com.devdreamteam.pitajnajblize.service.impl;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.dto.mapper.CategoryDtoMapper;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.exception.UnauthorizedException;
import com.devdreamteam.pitajnajblize.repository.CategoryDao;
import com.devdreamteam.pitajnajblize.service.CategoryService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Jan Capek
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDao categoryDao;
    private final CategoryDtoMapper categoryDtoMapper;

    /**
     * @throws NullPointerException If any of the parameters is {@code null}.
     */
    @Autowired
    public CategoryServiceImpl(CategoryDao categoryDao, CategoryDtoMapper categoryDtoMapper) {
        this.categoryDao = Objects.requireNonNull(categoryDao);
        this.categoryDtoMapper = Objects.requireNonNull(categoryDtoMapper);
    }

    @Override
    public Set<CategoryDto> getAll() {
        verifyLogged();
        return categoryDao.findAll().stream().map(categoryDtoMapper).collect(Collectors.toSet());
    }

    @Override
    public CategoryDto getById(long id) {
        verifyLogged();
        return categoryDtoMapper.apply(
                categoryDao.findById(id).orElseThrow(EntityNotFoundException::new)
        );
    }

    /**
     * @throws UnauthorizedException If user is not logged.
     */
    private void verifyLogged() {
        if (SessionUtil.getLoggedUser() == null) {
            throw new UnauthorizedException();
        }
    }
}
