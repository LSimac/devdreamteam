package com.devdreamteam.pitajnajblize.service;

import com.devdreamteam.pitajnajblize.dto.TokenDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
public interface AuthService {
    /**
     * Generate token for given user id.
     *
     * @param userId Id of a user who the token will be generated for
     */
    TokenDto generateToken(long userId);

    /**
     * Check validity of a token.
     *
     * @param token Token which will be checked
     * @return {@link UserDto} for corresponding token if validation is successful, {@code null} otherwise
     */
    UserDto validateToken(String token);

    /**
     * Deletes a token.
     *
     * @param tokenValue Value of the token that is to be removed
     */
    void deleteToken(String tokenValue);
}
