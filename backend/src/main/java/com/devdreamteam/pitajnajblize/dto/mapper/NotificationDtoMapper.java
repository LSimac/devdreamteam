package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.NotificationDto;
import com.devdreamteam.pitajnajblize.entity.Notification;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class NotificationDtoMapper implements Function<Notification, NotificationDto> {

    @Override
    public NotificationDto apply(Notification notification) {
        NotificationDto n = new NotificationDto();
        n.setId(notification.getId());
        n.setReferenceEntityId(notification.getEntityReferenceId());
        n.setReferenceEntityType(notification.getEntityReferenceType());
        n.setCreationTimestamp(notification.getCreationTimestamp().toInstant().toString());
        n.setIsSeen(notification.isSeen());
        n.setDescription(notification.getDescription());
        return n;
    }
}
