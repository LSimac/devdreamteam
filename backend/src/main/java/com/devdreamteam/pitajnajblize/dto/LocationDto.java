package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

@Data
public class LocationDto {
    private Double longitude;
    private Double latitude;
}
