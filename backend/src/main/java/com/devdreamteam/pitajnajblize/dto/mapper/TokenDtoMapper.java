package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.TokenDto;
import com.devdreamteam.pitajnajblize.entity.Token;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Component
public class TokenDtoMapper implements Function<Token, TokenDto> {
    @Override
    public TokenDto apply(Token token) {
        TokenDto tokenDto = new TokenDto();
        tokenDto.setId(token.getId());
        tokenDto.setValidUntil(token.getValidUntil().toString());
        tokenDto.setUserId(token.getUserId());
        tokenDto.setToken(token.getToken());
        return tokenDto;
    }
}
