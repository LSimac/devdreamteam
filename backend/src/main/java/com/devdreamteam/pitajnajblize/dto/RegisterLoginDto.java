package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

@Data
public class RegisterLoginDto {
    private String username;
    private String password;
    private String role;
    private String repeatedPassword;
}
