package com.devdreamteam.pitajnajblize.dto.validator;

import com.devdreamteam.pitajnajblize.dto.MessageDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Objects;
import java.util.function.Predicate;

@Component
public class MessageDtoValidator implements Predicate<MessageDto> {

    private final UserDao userDao;
    private final UserDtoValidator userDtoValidator;

    @Autowired
    public MessageDtoValidator(UserDao userDao, UserDtoValidator userDtoValidator) {
        this.userDao = userDao;
        this.userDtoValidator = userDtoValidator;
    }

    @Override
    public boolean test(MessageDto messageDto) {
        return Objects.isNull(messageDto) == false
                && testText(messageDto.getText())
                && testAuthor(messageDto.getAuthor())
                && testTimestamp(messageDto.getCreationTimestamp());
    }

    private boolean testText(String text) {
        if (Objects.isNull(text)) {
            return false;
        }

        return text.isEmpty() == false;
    }

    private boolean testAuthor(UserDto user) {
        Long creatorId = user.getId();

        User userEntity = userDao.getUserById(creatorId);

        if (Objects.isNull(userEntity)) {
            return false;
        }

        return userDtoValidator.test(user);
    }

    private boolean testTimestamp(String creationTimestamp) {
        try {
            Instant.parse(creationTimestamp);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }
}
