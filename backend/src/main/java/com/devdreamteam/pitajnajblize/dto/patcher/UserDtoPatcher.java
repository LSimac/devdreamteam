package com.devdreamteam.pitajnajblize.dto.patcher;

import com.devdreamteam.pitajnajblize.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.function.BinaryOperator;

/**
 * UserDto patcher used to patch missing {@link UserDto} attributes.
 *
 * @author Jan Capek
 */
@Component
public class UserDtoPatcher implements BinaryOperator<UserDto> {
    @Override
    public UserDto apply(UserDto user, UserDto patch) {
        UserDto patched = new UserDto();
        patched.setId(patch.getId() == null ? user.getId() : patch.getId());
        patched.setUsername(patch.getUsername() == null ? user.getUsername() : patch.getUsername());
        patched.setAvatarId(patch.getAvatarId() == null ? user.getAvatarId() : patch.getAvatarId());
        patched.setRole(patch.getRole() == null ? user.getRole() : patch.getRole());
        patched.setLocation(patch.getLocation() == null ? user.getLocation() : patch.getLocation());
        patched.setDescription(patch.getDescription() == null ? user.getDescription() : patch.getDescription());
        patched.setCategories(patch.getCategories() == null ? user.getCategories() : patch.getCategories());
        patched.setGrade(patch.getGrade() == null ? user.getGrade() : patch.getGrade());
        return patched;
    }
}
