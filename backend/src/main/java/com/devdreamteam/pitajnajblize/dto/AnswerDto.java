package com.devdreamteam.pitajnajblize.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnswerDto {
    private Long id;
    private MessageDto message;
    private Long questionId;
    private Boolean isAccepted;

}
