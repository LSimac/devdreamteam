package com.devdreamteam.pitajnajblize.dto.validator;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.repository.CategoryDao;
import com.devdreamteam.pitajnajblize.repository.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Validator for {@link UserDto} data content.
 *
 * @author Jan Capek
 */
@Component
public class UserDtoValidator implements Predicate<UserDto> {

    private final RoleDao roleDao;

    private final CategoryDao categoryDao;

    /**
     * @throws NullPointerException If any of the given params is {@code null}.
     */
    @Autowired
    public UserDtoValidator(RoleDao roleDao, CategoryDao categoryDao) {
        this.roleDao = Objects.requireNonNull(roleDao);
        this.categoryDao = Objects.requireNonNull(categoryDao);
    }

    /**
     * Tests dto data for validity considering user's role.
     *
     * @param userDto UserDto to test.
     * @return {@code true} if the dto is valid, {@code false} otherwise.
     */
    @Override
    public boolean test(UserDto userDto) {
        return userDto.getId() != null
                && userDto.getUsername() != null
                && userDto.getRole() != null
                && testRoleExistence(userDto.getRole())
                && testRoleSpecificData(userDto);
    }

    private boolean testRoleExistence(String role) {
        if (role == null) {
            return false;
        }
        try {
            roleDao.getByName(role);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    private boolean testRoleSpecificData(UserDto userDto) {
        String role = userDto.getRole();
        if (Objects.equals(role, "normal")) {
            return testNormalUserSpecificData(userDto);
        }
        if (Objects.equals(role, "admin")) {
            return testAdminSpecificData(userDto);
        }
        if (Objects.equals(role, "professional")) {
            return testProfessionalSpecificData(userDto) && testCategoryExistence(userDto.getCategories());
        }
        return true;
    }

    private static boolean testNormalUserSpecificData(UserDto userDto) {
        return userDto.getGrade() == null
                && userDto.getDescription() == null
                && userDto.getCategories() == null;
    }

    private static boolean testAdminSpecificData(UserDto userDto) {
        return userDto.getLocation() == null && testNormalUserSpecificData(userDto);
    }

    private static boolean testProfessionalSpecificData(UserDto userDto) {
        return userDto.getDescription() != null
                && userDto.getCategories() != null
                && userDto.getGrade() != null && userDto.getGrade() >= 0 && userDto.getGrade() <= 5;
    }

    private boolean testCategoryExistence(Collection<CategoryDto> categories) {
        for (CategoryDto c : categories) {
            if (categoryDao.findById(c.getId()).isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
