package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.ImageDto;
import com.devdreamteam.pitajnajblize.entity.Image;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Component
public class ImageDtoMapper implements Function<Image, ImageDto> {
    @Override
    public ImageDto apply(Image image) {
        ImageDto imageDto = new ImageDto();
        imageDto.setId(image.getId());
        imageDto.setRealPath(image.getPath());
        return imageDto;
    }
}
