package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.professional.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Mapper mapping {@link User} to {@link UserDto}.
 *
 * @author Jan Capek
 */
@Component
public class UserDtoMapper implements Function<User, UserDto> {

    private final LocationDtoMapper locationMapper;
    private final CategoryDtoMapper categoryMapper;

    @Autowired
    public UserDtoMapper(LocationDtoMapper locationMapper, CategoryDtoMapper categoryMapper) {
        this.locationMapper = locationMapper;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public UserDto apply(User entity) {
        if (entity == null) {
            return null;
        }
        UserDto user = new UserDto();
        user.setId(entity.getId());
        user.setUsername(entity.getUsername());
        user.setRole(entity.getRole().getName());
        user.setAvatarId(entity.getImage() == null ? null : entity.getImage().getId());
        user.setLocation(locationMapper.apply(entity.getLocation()));
        if (entity.getRole().getName().equals("professional")) {
            setProfessionalAttributes(entity, user);
        }
        return user;
    }

    private void setProfessionalAttributes(User entity, UserDto user) {
        user.setDescription(entity.getDescription());
        user.setCategories(
                entity.getCategories() == null ? null : entity.getCategories().stream()
                        .map(categoryMapper).collect(Collectors.toSet())
        );
        user.setGrade(
                entity.getGrades() == null ? null : entity.getGrades().stream()
                        .mapToInt(Grade::getGrade).average().orElse(0)
        );
    }
}
