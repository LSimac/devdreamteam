package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.entity.Category;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * {@link Category} mapper to {@link CategoryDto}.
 *
 * @author Jan Capek
 */
@Component
public class CategoryDtoMapper implements Function<Category, CategoryDto> {
    @Override
    public CategoryDto apply(Category category) {
        if (category == null) {
            return null;
        }
        CategoryDto cat = new CategoryDto();
        cat.setId(category.getId());
        cat.setName(category.getName());
        return cat;
    }
}
