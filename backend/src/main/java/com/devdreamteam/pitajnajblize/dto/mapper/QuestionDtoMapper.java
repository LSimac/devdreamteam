package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Luka Šimac
 */
@Component
public class QuestionDtoMapper implements Function<Question, QuestionDto> {

    private static String ANSWERED = "answered";
    private static String PRIVATE = "professional";
    private final LocationDtoMapper locationDtoMapper;
    private final MessageDtoMapper messageDtoMapper;
    private final CategoryDtoMapper categoryDtoMapper;

    public QuestionDtoMapper(LocationDtoMapper locationDtoMapper, MessageDtoMapper messageDtoMapper, CategoryDtoMapper categoryDtoMapper) {
        this.locationDtoMapper = locationDtoMapper;
        this.messageDtoMapper = messageDtoMapper;
        this.categoryDtoMapper = categoryDtoMapper;
    }

    @Override
    public QuestionDto apply(Question entity) {
        if (Objects.isNull(entity)) {
            return null;
        }

        QuestionDto question = new QuestionDto();
        question.setTitle(entity.getTitle());
        question.setId(entity.getId());
        question.setIsAnswered(isQuestionAnswered(entity));
        question.setLocation(locationDtoMapper.apply(entity.getLocation()));
        question.setMessage(messageDtoMapper.apply(entity.getMessage()));
        question.setType(entity.getType().getName());
        question.setStatus(entity.getStatus().getName());
        question.setCategories(entity.getCategory().stream().map(this.categoryDtoMapper).collect(Collectors.toSet()));

        if (entity.getType().getName().equals(QuestionDtoMapper.PRIVATE)) {
            question.setIdOfProfessionalWhoClaimed(Objects.isNull(entity.getProfessionalWhoClaimed()) ? null : entity.getProfessionalWhoClaimed().getId());
            question.setProfessionalIds(entity.getQueriedProfessionals().stream().map(User::getId).collect(Collectors.toSet()));
        }

        return question;
    }


    private boolean isQuestionAnswered(Question question){
        Set<Answer> answers = question.getAnswers();

        if(Objects.isNull(answers)){
            return false;
        }

        for(Answer a : answers){
            if(a.isAccepted()){
                return true;
            }
        }

        return false;
    }

}
