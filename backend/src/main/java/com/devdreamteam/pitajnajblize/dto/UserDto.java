package com.devdreamteam.pitajnajblize.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
    private Long id;
    private String username;
    private String role;
    private Long avatarId;

    //    normal and professional
    private LocationDto location;

    //    professional
    private Set<CategoryDto> categories;
    private String description;
    private Double grade;
}
