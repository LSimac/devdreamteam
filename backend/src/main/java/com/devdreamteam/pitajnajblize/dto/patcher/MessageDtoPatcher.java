package com.devdreamteam.pitajnajblize.dto.patcher;

import com.devdreamteam.pitajnajblize.dto.MessageDto;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.BinaryOperator;

@Component
public class MessageDtoPatcher implements BinaryOperator<MessageDto> {
    @Override
    public MessageDto apply(MessageDto message, MessageDto patch) {
        if (Objects.isNull(patch)) {
            return message;
        }

        MessageDto patched = new MessageDto();

        patched.setText(Objects.isNull(patch.getText()) ? message.getText() : patch.getText());
        patched.setCreationTimestamp(Objects.isNull(patch.getCreationTimestamp()) ? message.getCreationTimestamp() : patch.getCreationTimestamp());
        patched.setAuthor(message.getAuthor());
        patched.setImageIds(Objects.isNull(patch.getImageIds()) ? message.getImageIds() : patch.getImageIds());

        return patched;

    }
}
