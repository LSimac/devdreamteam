package com.devdreamteam.pitajnajblize.dto.patcher;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.BinaryOperator;

@Component
public class AnswerDtoPatcher implements BinaryOperator<AnswerDto> {

    private final MessageDtoPatcher messageDtoPatcher;

    @Autowired
    public AnswerDtoPatcher(MessageDtoPatcher messageDtoPatcher) {
        this.messageDtoPatcher = messageDtoPatcher;
    }

    @Override
    public AnswerDto apply(AnswerDto oldAnswer, AnswerDto newAnswer) {

        if (Objects.isNull(newAnswer)) {
            return oldAnswer;
        }

        AnswerDto patchedAnswer = new AnswerDto();
        patchedAnswer.setId(Objects.isNull(newAnswer.getId()) ? oldAnswer.getId() : newAnswer.getId());
        patchedAnswer.setIsAccepted(Objects.isNull(newAnswer.getIsAccepted()) ? oldAnswer.getIsAccepted() : newAnswer.getIsAccepted());
        patchedAnswer.setQuestionId(Objects.isNull(newAnswer.getQuestionId()) ? oldAnswer.getQuestionId() : newAnswer.getQuestionId());
        patchedAnswer.setMessage(messageDtoPatcher.apply(oldAnswer.getMessage(), newAnswer.getMessage()));
        return patchedAnswer;
    }
}
