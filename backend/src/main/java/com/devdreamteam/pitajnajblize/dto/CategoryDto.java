package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

import java.util.Objects;

@Data
public class CategoryDto {
    private Long id;
    private String name;

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDto that = (CategoryDto) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }
}
