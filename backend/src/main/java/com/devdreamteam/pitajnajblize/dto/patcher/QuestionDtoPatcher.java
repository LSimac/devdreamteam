package com.devdreamteam.pitajnajblize.dto.patcher;

import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.BinaryOperator;

@Component
public class QuestionDtoPatcher implements BinaryOperator<QuestionDto> {

    private final MessageDtoPatcher messageDtoPatcher;

    public QuestionDtoPatcher(MessageDtoPatcher messageDtoPatcher) {
        this.messageDtoPatcher = messageDtoPatcher;
    }

    @Override
    public QuestionDto apply(QuestionDto question, QuestionDto patch) {
        if (Objects.isNull(patch)) {
            return question;
        }

        QuestionDto patched = new QuestionDto();

        patched.setId(patch.getId() == null ? question.getId() : patch.getId());
        patched.setTitle(patch.getTitle() == null ? question.getTitle() : patch.getTitle());
        patched.setMessage(this.messageDtoPatcher.apply(question.getMessage(), patch.getMessage()));
        patched.setLocation(patch.getLocation() == null ? question.getLocation() : patch.getLocation());
        patched.setStatus(patch.getStatus() == null ? question.getStatus() : patch.getStatus());
        patched.setIsAnswered(patch.getIsAnswered() == null ? question.getIsAnswered() : patch.getIsAnswered());
        patched.setType(patch.getType() == null ? question.getType() : patch.getType());
        patched.setCategories(patch.getCategories() == null ? question.getCategories() : patch.getCategories());

        patched.setProfessionalIds(
                patch.getProfessionalIds() == null ? question.getProfessionalIds() : patch.getProfessionalIds());
        patched.setIdOfProfessionalWhoClaimed(
                patch.getIdOfProfessionalWhoClaimed() == null ? question.getIdOfProfessionalWhoClaimed() : patch.getIdOfProfessionalWhoClaimed());

        return patched;
    }
}
