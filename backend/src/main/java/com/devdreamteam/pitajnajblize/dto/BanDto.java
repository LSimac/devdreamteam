package com.devdreamteam.pitajnajblize.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BanDto {
    UserDto user;
    String date;
}
