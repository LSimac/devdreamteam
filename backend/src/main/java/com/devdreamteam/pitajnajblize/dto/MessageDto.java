package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

import java.util.Set;

@Data
public class MessageDto {
    private String text;
    private String timestamp;
    private Set<Long> imageIds;
    private UserDto author;
    private String creationTimestamp;
}
