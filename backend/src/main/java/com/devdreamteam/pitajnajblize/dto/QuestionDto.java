package com.devdreamteam.pitajnajblize.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

/**
 * Class that describes a question (public and professional).
 * <p>
 * Null fields will be ignored in responses.
 *
 * @author Jan Capek
 */
@Data
@JsonInclude(JsonInclude.Include.ALWAYS)  // todo(jcapek_): why ALWAYS instead of NON_NULL?
public class QuestionDto {
    private Long id;
    private String title;
    private MessageDto message;
    private LocationDto location;
    private String status;
    private Boolean isAnswered;
    private String type;
    private Set<CategoryDto> categories;

    //    professional part (won't be included in response if null)
    private Set<Long> professionalIds;
    private Long idOfProfessionalWhoClaimed;
}
