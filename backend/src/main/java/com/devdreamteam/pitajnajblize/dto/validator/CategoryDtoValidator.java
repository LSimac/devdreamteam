package com.devdreamteam.pitajnajblize.dto.validator;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.repository.CategoryDao;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Predicate;

@Component
public class CategoryDtoValidator implements Predicate<CategoryDto> {

    private final CategoryDao categoryDao;

    public CategoryDtoValidator(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public boolean test(CategoryDto categoryDto) {
        return Objects.isNull(categoryDto) == false
                && checkIfCategoryExists(categoryDto.getId());
    }

    private boolean checkIfCategoryExists(Long id) {
        if (Objects.isNull(id)) {
            return false;
        }

        try {
            categoryDao.findById(id);
        } catch (EntityNotFoundException e) {
            return false;
        }

        return true;
    }
}
