package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.LocationDto;
import com.devdreamteam.pitajnajblize.entity.Location;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Mapper mapping {@link Location} to {@link LocationDto}.
 *
 * @author Jan Capek
 */
@Component
public class LocationDtoMapper implements Function<Location, LocationDto> {

    @Override
    public LocationDto apply(Location location) {
        if (location == null) {
            return null;
        }
        LocationDto loc = new LocationDto();
        loc.setLatitude(location.getLatitude());
        loc.setLongitude(location.getLongitude());
        return loc;
    }
}
