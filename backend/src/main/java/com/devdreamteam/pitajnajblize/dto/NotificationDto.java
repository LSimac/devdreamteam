package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

@Data
public class NotificationDto {
    private Long id;

    private Long referenceEntityId;

    private String referenceEntityType;

    private String creationTimestamp;

    private Boolean isSeen;

    private String description;
}
