package com.devdreamteam.pitajnajblize.dto.validator;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.entity.Category;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.repository.QuestionStatusDao;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class QuestionDtoValidator implements Predicate<QuestionDto> {

    private static final String MESSAGE_PRIVATE_TYPE = "professional";
    private static final String MESSAGE_PUBLIC_TYPE = "public";
    private final QuestionStatusDao questionStatusDao;
    private final UserDao userDao;
    private final CategoryDtoValidator categoryDtoValidator;
    private final MessageDtoValidator messageDtoValidator;
    private final LocationDtoValidator locationDtoValidator;


    public QuestionDtoValidator(QuestionStatusDao questionStatusDao, CategoryDtoValidator categoryDtoValidator, MessageDtoValidator messageDtoValidator, LocationDtoValidator locationDtoValidator, UserDao userDao) {
        this.questionStatusDao = questionStatusDao;
        this.categoryDtoValidator = categoryDtoValidator;
        this.messageDtoValidator = messageDtoValidator;
        this.locationDtoValidator = locationDtoValidator;
        this.userDao = userDao;
    }

    @Override
    public boolean test(QuestionDto questionDto) {
        return Objects.isNull(questionDto) == false
                && testTitle(questionDto.getTitle())
                && testStatus(questionDto.getStatus())
                && testType(questionDto.getType())
                && testCategories(questionDto.getCategories())
                && messageDtoValidator.test(questionDto.getMessage())
                && locationDtoValidator.test(questionDto.getLocation())
                && (isQuestionPublic(questionDto.getType()) || testPrivateMessageInformation(questionDto.getProfessionalIds(), questionDto.getIdOfProfessionalWhoClaimed(), questionDto.getCategories()));
    }

    private boolean testTitle(String title) {
        return Objects.isNull(title) == false && title.isEmpty() == false;
    }

    private boolean testStatus(String status) {
        if (Objects.isNull(status)) {
            return false;
        }

        try {
            questionStatusDao.findByName(status);
        } catch (EntityNotFoundException e) {
            return false;
        }

        return true;
    }

    private boolean testType(String type) {
        if (Objects.isNull(type)) {
            return false;
        }

        try {
            questionStatusDao.findByName(type);
        } catch (EntityNotFoundException e) {
            return false;
        }

        return true;
    }

    private boolean testCategories(Set<CategoryDto> categoryDtos) {
        if (Objects.isNull(categoryDtos)) {
            return false;
        }

        if (categoryDtos.isEmpty()) {
            return false;
        }

        return categoryDtos.stream().map(categoryDtoValidator::test).filter(m -> !m).collect(Collectors.toSet()).isEmpty();
    }

    private boolean isQuestionPublic(String type) {
        return type.equals(MESSAGE_PUBLIC_TYPE);
    }

    private boolean testPrivateMessageInformation(Set<Long> proffesionals, Long proffesionalaWhoClaimed, Set<CategoryDto> categoryDtos) {
        return checkAllProfessionals(proffesionals, categoryDtos)
                && checkProffesionalWhoClaimed(proffesionalaWhoClaimed, proffesionals)
                && (Objects.isNull(proffesionalaWhoClaimed) || proffesionals.contains(proffesionalaWhoClaimed));
    }


    private boolean checkAllProfessionals(Set<Long> professionals, Set<CategoryDto> categoryDtos) {
        return checkIfAllProfessionalUsersExists(professionals) && checkIfAllUsersContainsCategories(professionals, categoryDtos.stream().map(CategoryDto::getId).collect(Collectors.toSet()));
    }

    private boolean checkProffesionalWhoClaimed(Long proffesionalId, Set<Long> proffesionals) {
        if (Objects.isNull(proffesionalId)) {
            return true;
        }

        if (proffesionals.contains(proffesionalId) == false) {
            return false;
        }

        return checkIfProffesionalUserExists(proffesionalId);
    }

    private boolean checkIfAllProfessionalUsersExists(Set<Long> professionalIds) {
        return professionalIds.stream().map(this::checkIfProffesionalUserExists).filter(m -> !m).collect(Collectors.toSet()).isEmpty();
    }

    private boolean checkIfAllUsersContainsCategories(Set<Long> professionals, Set<Long> categoriesId) {
        for (Long userId : professionals) {
            Set<Long> userCatergoriesIds = userDao.getUserById(userId).getCategories().stream().map(Category::getId).collect(Collectors.toSet());

            if (userCatergoriesIds.containsAll(categoriesId) == false) {
                return false;
            }
        }

        return true;
    }

    private boolean checkIfProffesionalUserExists(Long id) {
        User user = userDao.getUserById(id);

        if (Objects.isNull(user)) {
            return false;
        }

        return user.getRole().getName().equals("professional");

    }
}
