package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UpdateUserDto {
    private String username;
    private Long avatarId;
    private String oldPassword;
    private String password;
    private String repeatedPassword;
    //    professional
    private Set<Long> categories;
    private String description;
}
