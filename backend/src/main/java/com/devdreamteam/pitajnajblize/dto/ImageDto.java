package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

import java.util.Objects;

@Data
public class ImageDto {
    private Long id;
    private String realPath;

    @Override
    public int hashCode() {
        return Objects.hash(id, realPath);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageDto imageDto = (ImageDto) o;
        return Objects.equals(id, imageDto.id) &&
                Objects.equals(realPath, imageDto.realPath);
    }
}
