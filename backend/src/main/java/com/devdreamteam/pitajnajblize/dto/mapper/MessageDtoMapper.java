package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.MessageDto;
import com.devdreamteam.pitajnajblize.entity.Image;
import com.devdreamteam.pitajnajblize.entity.questions.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Luka Šimac
 */
@Component
public class MessageDtoMapper implements Function<Message, MessageDto> {

    private final UserDtoMapper userDtoMapper;


    @Autowired
    public MessageDtoMapper(UserDtoMapper userDtoMapper) {
        this.userDtoMapper = userDtoMapper;
    }

    @Override
    public MessageDto apply(Message entity) {
        if (Objects.isNull(entity)) {
            return null;
        }

        MessageDto message = new MessageDto();
        message.setText(entity.getContent());
        message.setImageIds(entity.getImage().stream().map(Image::getId).collect(Collectors.toSet()));
        message.setAuthor(userDtoMapper.apply(entity.getAuthor()));
        message.setCreationTimestamp(entity.getCreationDate().toInstant().toString());

        return message;
    }
}
