package com.devdreamteam.pitajnajblize.dto.validator;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import com.devdreamteam.pitajnajblize.repository.AnswerDao;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

@Component
public class AnswerDtoValidator implements Predicate<AnswerDto> {

    private final MessageDtoValidator messageDtoValidator;
    private final QuestionDao questionDao;
    private final AnswerDao answerDao;

    @Autowired
    public AnswerDtoValidator(MessageDtoValidator messageDtoValidator, QuestionDao questionDao, AnswerDao answerDao) {
        this.messageDtoValidator = messageDtoValidator;
        this.questionDao = questionDao;
        this.answerDao = answerDao;
    }

    @Override
    public boolean test(AnswerDto answerDto) {
        return !Objects.isNull(answerDto)
                && messageDtoValidator.test(answerDto.getMessage())
                && testQuestion(answerDto)
                && !Objects.isNull(answerDto.getIsAccepted());
    }

    private boolean testQuestion(AnswerDto answerDto) {
        Optional<Question> questionOptional = questionDao.findById(answerDto.getQuestionId());
        Long currentUserId = SessionUtil.getLoggedUser().getId();

        if (questionOptional.isPresent() == false) {
            return false;
        }

        Question question = questionOptional.get();

        if (question.getStatus().getName().equals("closed")) {
            return false;
        }

        if (question.getType().getName().equals("professional")) {
            User professionalWhoClaimed = question.getProfessionalWhoClaimed();

            if (Objects.isNull(professionalWhoClaimed)) {
                return false;
            }

            if (professionalWhoClaimed.getId().equals(currentUserId) == false) {
                return false;
            }
        }

        if (answerDto.getIsAccepted()) {
            List<Answer> acceptedAnswers = new ArrayList<>(answerDao.findAcceptedAnswersForQuestion(answerDto.getQuestionId()));

            if (acceptedAnswers.isEmpty()) {
                return true;
            } else if (acceptedAnswers.size() == 1) {
                return acceptedAnswers.get(0).getId().equals(answerDto.getId());
            } else {
                return false;
            }
        }

        return true;

    }

}
