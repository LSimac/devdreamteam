package com.devdreamteam.pitajnajblize.dto;

import lombok.Data;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Data
public class TokenDto {
    private Long id;
    private Long userId;
    private String token;
    private String validUntil;
}
