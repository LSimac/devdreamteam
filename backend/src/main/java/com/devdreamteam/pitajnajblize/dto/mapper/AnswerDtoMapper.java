package com.devdreamteam.pitajnajblize.dto.mapper;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AnswerDtoMapper implements Function<Answer, AnswerDto> {

    private final MessageDtoMapper messageDtoMapper;

    public AnswerDtoMapper(MessageDtoMapper messageDtoMapper) {
        this.messageDtoMapper = messageDtoMapper;
    }

    @Override
    public AnswerDto apply(Answer answer) {
        if (answer == null) {
            return null;
        }
        AnswerDto answerDto = new AnswerDto();

        answerDto.setId(answer.getId());
        answerDto.setQuestionId(answer.getQuestion().getId());
        answerDto.setIsAccepted(answer.isAccepted());
        answerDto.setMessage(messageDtoMapper.apply(answer.getMessage()));

        return answerDto;
    }
}
