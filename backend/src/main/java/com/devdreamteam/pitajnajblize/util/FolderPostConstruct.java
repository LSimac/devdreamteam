package com.devdreamteam.pitajnajblize.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Component
public class FolderPostConstruct {
    @Value("${setting.image.folder-path}")
    private String folderPath;

    @PostConstruct
    public void init() {
        Path path = Paths.get(folderPath);
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                System.out.println("Unable to create directory " + path);
            }
        }
    }
}
