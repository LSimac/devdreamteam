package com.devdreamteam.pitajnajblize.util;

public class LocationDistance {

    public static double calculateDistanceInMeters(double longitude1, double latitude1, double longitude2, double latitude2) {
        final double earthRadius = 6371e3;
        double fi1 = Math.toRadians(latitude1);
        double fi2 = Math.toRadians(latitude2);
        double dFi = Math.toRadians(latitude2 - latitude1);
        double dLambda = Math.toRadians(longitude2 - longitude1);

        double a = Math.sin(dFi / 2) * Math.sin(dFi / 2) + Math.cos(fi1) * Math.cos(fi2) * Math.sin(dLambda / 2) * Math.sin(dLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return earthRadius * c;
    }
}
