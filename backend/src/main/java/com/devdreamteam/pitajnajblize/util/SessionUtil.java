package com.devdreamteam.pitajnajblize.util;

import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.dto.mapper.CategoryDtoMapper;
import com.devdreamteam.pitajnajblize.dto.mapper.LocationDtoMapper;
import com.devdreamteam.pitajnajblize.dto.mapper.UserDtoMapper;
import com.devdreamteam.pitajnajblize.security.UserDetailsWrapperDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * Session util containing method for retrieving logged user.
 *
 * @author Jan Capek
 */
public class SessionUtil {

    //    todo(jcapek_): inject this somehow...
    private static UserDtoMapper userDtoMapper = new UserDtoMapper(new LocationDtoMapper(), new CategoryDtoMapper());

    /**
     * @return {@link UserDto} of logged user or {@code null} if user isn't logged in.
     */
    public static UserDto getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth == null ? null : userDtoMapper.apply(
                ((UserDetailsWrapperDefault) auth.getPrincipal()).getUser()
        );
    }
}
