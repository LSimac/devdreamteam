package com.devdreamteam.pitajnajblize.security;

import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;


    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null || username.length() == 0) {
            throw new IllegalArgumentException("Invalid username");
        }
        User user = userDao.getUserById(Long.parseLong(username));
        if (user == null) {
            throw new UsernameNotFoundException("User not found.");
        }
        return new UserDetailsWrapperDefault(user);
    }
}
