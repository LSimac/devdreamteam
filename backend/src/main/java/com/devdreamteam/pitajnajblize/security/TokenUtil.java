package com.devdreamteam.pitajnajblize.security;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Service
public class TokenUtil {
    @Value("${security.token-length}")
    private int LENGTH;
    @Value("${security.token-has-letters}")
    private boolean HAS_LETTERS;
    @Value("${security.token-has-numbers}")
    private boolean HAS_NUMBERS;

    public String generateTokenValue() {
        return RandomStringUtils.random(LENGTH, HAS_LETTERS, HAS_NUMBERS);
    }
}
