package com.devdreamteam.pitajnajblize.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Table(name = "TOKENS")
@Entity
@Getter
@Setter
public class Token {

    @Id
    @GeneratedValue
    private Long id;

    private Long userId;

    private String token;

    private Timestamp validUntil;
}
