package com.devdreamteam.pitajnajblize.entity.mapper;

import com.devdreamteam.pitajnajblize.dto.LocationDto;
import com.devdreamteam.pitajnajblize.entity.Location;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class LocationEntityDtoMapper implements Function<LocationDto, Location> {

    @Override
    public Location apply(LocationDto locationDto) {
        Location location = new Location();

        location.setLongitude(locationDto.getLongitude());
        location.setLatitude(locationDto.getLatitude());

        return location;
    }
}
