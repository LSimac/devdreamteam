package com.devdreamteam.pitajnajblize.entity.questions;

import com.devdreamteam.pitajnajblize.entity.Image;
import com.devdreamteam.pitajnajblize.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Table(name = "MESSAGES")
@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String content;

    @OneToMany(fetch = FetchType.EAGER)
    @CollectionTable(name = "MESSAGE_IMG")
    private Set<Image> image;

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date creationDate;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User author;
}
