package com.devdreamteam.pitajnajblize.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String description;

    @NotNull
    private Long entityReferenceId;

    @NotNull
    private String entityReferenceType;  // todo(jcapek_): should be an object but this is good for now

    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date creationTimestamp;

    private boolean isSeen;

    @NotNull
    @OneToOne(cascade = CascadeType.REFRESH)
    private User recipient;

//    @NotNull
//    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
//    private Set<User> usersWhoSaw;
//
//    @NotEmpty
//    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
//    private Set<User> recipients;
}
