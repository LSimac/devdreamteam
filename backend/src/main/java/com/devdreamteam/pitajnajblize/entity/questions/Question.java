package com.devdreamteam.pitajnajblize.entity.questions;

import com.devdreamteam.pitajnajblize.entity.Category;
import com.devdreamteam.pitajnajblize.entity.Location;
import com.devdreamteam.pitajnajblize.entity.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Table(name = "QUESTIONS")
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private String title;

    private int level = 1;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Location location;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @NotEmpty
    private Set<Category> category;

    @ManyToOne(cascade = {CascadeType.REFRESH}, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private QuestionType type;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Message message;

    @ManyToOne(cascade = {CascadeType.REFRESH}, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private QuestionStatus status;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "question", cascade = {CascadeType.ALL})
    private Set<Answer> answers;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    private Set<User> queriedProfessionals;

    @ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    private User professionalWhoClaimed;
}
