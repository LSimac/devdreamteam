package com.devdreamteam.pitajnajblize.entity.mapper;

import com.devdreamteam.pitajnajblize.dto.MessageDto;
import com.devdreamteam.pitajnajblize.entity.Image;
import com.devdreamteam.pitajnajblize.entity.questions.Message;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.repository.ImageDao;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Function;

@Component
public class MessageEntityDtoMapper implements Function<MessageDto, Message> {

    //Vjv ce trebat maknuti
    private final UserDao userDao;
    private final UserEntityDtoMapper userEntityDtoMapper;
    private final ImageDao imageDao;

    @Autowired
    public MessageEntityDtoMapper(UserDao userDao, UserEntityDtoMapper userEntityDtoMapper, ImageDao imageDao) {
        this.userDao = Objects.requireNonNull(userDao);
        this.userEntityDtoMapper = Objects.requireNonNull(userEntityDtoMapper);
        this.imageDao = Objects.requireNonNull(imageDao);
    }

    @Override
    public Message apply(MessageDto messageDto) {
        Message message = new Message();

        message.setContent(messageDto.getText());
        message.setAuthor(userEntityDtoMapper.apply(messageDto.getAuthor()));

        try {
            Instant instant = Instant.parse(messageDto.getCreationTimestamp());
            message.setCreationDate(Date.from(instant));
        } catch (DateTimeParseException e) {
            throw new InvalidDataException();
        }

        if (Objects.isNull(messageDto.getImageIds())) {
            message.setImage(new HashSet<Image>());
        } else {
            message.setImage(new HashSet<>(imageDao.findAllById(messageDto.getImageIds())));
        }

        return message;
    }


}
