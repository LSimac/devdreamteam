package com.devdreamteam.pitajnajblize.entity.professional;

import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Table(name = "GRADES")
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Grade implements Serializable {

    @Id
    @OneToOne(cascade = {CascadeType.REFRESH})
    @EqualsAndHashCode.Include
    private Question question;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull
    private User professional;

    private int grade;
}
