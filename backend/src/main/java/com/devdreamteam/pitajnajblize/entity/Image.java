package com.devdreamteam.pitajnajblize.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Table(name = "IMAGES")
@Entity
public class Image {

    @Id
    @GeneratedValue
    private long id;

    @NotEmpty
    private String path;

    public Image() {
    }

    public Image(String path) {
        this.path = path;
    }
}
