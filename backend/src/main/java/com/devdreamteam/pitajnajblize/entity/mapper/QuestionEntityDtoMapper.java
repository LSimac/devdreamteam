package com.devdreamteam.pitajnajblize.entity.mapper;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.entity.Category;
import com.devdreamteam.pitajnajblize.entity.Image;
import com.devdreamteam.pitajnajblize.entity.Location;
import com.devdreamteam.pitajnajblize.entity.questions.*;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.repository.*;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class QuestionEntityDtoMapper implements Function<QuestionDto, Question> {

    private final MessageEntityDtoMapper messageEntityDtoMapper;
    private final LocationEntityDtoMapper locationEntityDtoMapper;
    private final CategoryEntityDtoMapper categoryEntityDtoMapper;
    private final AnswersEntityDtoMapper answersEntityDtoMapper;

    private final UserDao userDao;
    private final QuestionTypeDao questionTypeDao;
    private final QuestionStatusDao questionStatusDao;
    private final QuestionDao questionDao;
    private final ImageDao imageDao;

    public QuestionEntityDtoMapper(MessageEntityDtoMapper messageEntityDtoMapper, LocationEntityDtoMapper locationEntityDtoMapper, UserDao userDao, QuestionTypeDao questionTypeDao, QuestionStatusDao questionStatusDao, CategoryEntityDtoMapper categoryEntityDtoMapper, AnswersEntityDtoMapper answersEntityDtoMapper, QuestionDao questionDao,ImageDao imageDao) {
        this.messageEntityDtoMapper = messageEntityDtoMapper;
        this.locationEntityDtoMapper = locationEntityDtoMapper;
        this.userDao = userDao;
        this.questionTypeDao = questionTypeDao;
        this.questionStatusDao = questionStatusDao;
        this.categoryEntityDtoMapper = categoryEntityDtoMapper;
        this.answersEntityDtoMapper = answersEntityDtoMapper;
        this.questionDao = questionDao;
        this.imageDao = imageDao;
    }

    @Override
    public Question apply(QuestionDto questionDto) {

        Question question = new Question();
        question.setTitle(questionDto.getTitle());
        question.setId(questionDto.getId());

        if (Objects.isNull(questionDto.getId()) == false) {
            Question oldQuestion = questionDao.findById(questionDto.getId()).get();
            question.setMessage(getMappedMessage(questionDto, oldQuestion));
            question.setLocation(getMappedLocation(questionDto, oldQuestion));
        } else {
            question.setMessage(messageEntityDtoMapper.apply(questionDto.getMessage()));
            question.setLocation(locationEntityDtoMapper.apply(questionDto.getLocation()));
        }

        question.setProfessionalWhoClaimed(Objects.isNull(questionDto.getIdOfProfessionalWhoClaimed()) ? null : userDao.findById(questionDto.getIdOfProfessionalWhoClaimed()).orElse(null));
        question.setQueriedProfessionals(Objects.isNull(questionDto.getProfessionalIds()) ? null : new HashSet<>(userDao.findAllById(questionDto.getProfessionalIds())));
        question.setType(getQuestionTypeEntity(questionDto.getType()));
        question.setStatus(getQuestionStatusEntity(questionDto.getStatus()));
        question.setCategory(getCategoriesEntities(questionDto.getCategories()));

        return question;
    }

    private Message getMappedMessage(QuestionDto questionDto, Question oldQuestion) {
        Message message = oldQuestion.getMessage();
        message.setContent(questionDto.getMessage().getText());
        Set<Image> images = new HashSet<>();
        for(Long imageId : questionDto.getMessage().getImageIds()){
            Optional<Image> image = imageDao.findById(imageId);

            image.ifPresent(images::add);
        }
        message.setImage(images);

        return message;
    }

    private Location getMappedLocation(QuestionDto questionDto, Question oldQuestion) {
        Location location = oldQuestion.getLocation();
        location.setLatitude(questionDto.getLocation().getLatitude());
        location.setLongitude(questionDto.getLocation().getLongitude());

        return location;
    }

    private QuestionType getQuestionTypeEntity(String type) {
        return this.questionTypeDao.findByName(type).orElseThrow(EntityNotFoundException::new);
    }

    private QuestionStatus getQuestionStatusEntity(String status) {
        return this.questionStatusDao.findByName(status).orElseThrow(EntityNotFoundException::new);
    }

    private Set<Category> getCategoriesEntities(Set<CategoryDto> categoriesDtos) {
        if (Objects.isNull(categoriesDtos)) {
            return new HashSet<>();
        }

        return categoriesDtos.stream().map(categoryEntityDtoMapper).collect(Collectors.toSet());
    }

    private Set<Answer> getAnswersEntities(Set<AnswerDto> answerDtos) {
        return answerDtos.stream().map(answersEntityDtoMapper).collect(Collectors.toSet());
    }
}
