package com.devdreamteam.pitajnajblize.entity.mapper;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AnswersEntityDtoMapper implements Function<AnswerDto, Answer> {

    private final MessageEntityDtoMapper messageEntityDtoMapper;
    private final QuestionDao questionDao;

    public AnswersEntityDtoMapper(MessageEntityDtoMapper messageEntityDtoMapper, QuestionDao questionDao) {
        this.messageEntityDtoMapper = messageEntityDtoMapper;
        this.questionDao = questionDao;
    }

    @Override
    public Answer apply(AnswerDto answerDto) {
        Answer answer = new Answer();

        answer.setId(answerDto.getId());
        answer.setAccepted(answerDto.getIsAccepted());
        answer.setMessage(this.messageEntityDtoMapper.apply(answerDto.getMessage()));
        answer.setQuestion(questionDao.findById(answerDto.getQuestionId()).orElseThrow(InvalidDataException::new));

        return answer;
    }
}
