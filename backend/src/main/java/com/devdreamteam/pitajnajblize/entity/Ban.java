package com.devdreamteam.pitajnajblize.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Table(name = "BANS")
@Entity
public class Ban {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    private User user;
}
