package com.devdreamteam.pitajnajblize.entity.mapper;

import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.repository.RoleDao;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class UserEntityDtoMapper implements Function<UserDto, User> {

    private final LocationEntityDtoMapper locationEntityDtoMapper;
    private final CategoryEntityDtoMapper categoryEntityDtoMapper;

    private final RoleDao roleDao;

    public UserEntityDtoMapper(LocationEntityDtoMapper locationEntityDtoMapper, RoleDao roleDao, CategoryEntityDtoMapper categoryEntityDtoMapper) {
        this.locationEntityDtoMapper = locationEntityDtoMapper;
        this.roleDao = roleDao;
        this.categoryEntityDtoMapper = categoryEntityDtoMapper;
    }

    @Override
    public User apply(UserDto userDto) {
        User user = new User();
        //zasad ce ovako biti todo(LSimac)
        user.setLocation(Objects.isNull(userDto.getLocation()) ? null : this.locationEntityDtoMapper.apply(userDto.getLocation()));
        user.setUsername(userDto.getUsername());
        user.setRole(this.roleDao.getByName(userDto.getRole()));
        user.setId(userDto.getId());

        if (userDto.getRole().equals("professional")) {
            user.setDescription(userDto.getDescription());
            user.setCategories(userDto.getCategories().stream().map(this.categoryEntityDtoMapper).collect(Collectors.toSet()));
        }

        return user;
    }
}
