package com.devdreamteam.pitajnajblize.entity;

import com.devdreamteam.pitajnajblize.entity.professional.Grade;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Getter
@Setter
@Table(name = "USERS")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotEmpty
    @Column(nullable = false, unique = true)
    private String username;

    @NotEmpty
    private String passHash;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Location location;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Image image;

    @ManyToOne(cascade = {}, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Role role;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "professional", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Grade> grades;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Category> categories;

    @Column
    private String description;
}
