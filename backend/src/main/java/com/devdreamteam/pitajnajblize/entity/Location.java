package com.devdreamteam.pitajnajblize.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "LOCATIONS")
@Entity
@Getter
@Setter
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double latitude;

    private double longitude;

    public Location() {
    }

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
