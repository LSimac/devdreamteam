package com.devdreamteam.pitajnajblize.entity.questions;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Table(name = "question_types")
@Entity
public class QuestionType {

    @Id
    @GeneratedValue
    private long id;

    @NotEmpty
    private String name;

    public QuestionType() {
    }

    public QuestionType(String name) {
        this.name = name;
    }
}
