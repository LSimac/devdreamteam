package com.devdreamteam.pitajnajblize.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Table(name = "ROLES")
@Entity
public class Role {

    @Id
    @GeneratedValue
    private long id;

    @NotEmpty
    private String name;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }
}
