package com.devdreamteam.pitajnajblize.entity.mapper;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.entity.Category;
import com.devdreamteam.pitajnajblize.repository.CategoryDao;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class CategoryEntityDtoMapper implements Function<CategoryDto, Category> {

    private final CategoryDao categoryDao;

    public CategoryEntityDtoMapper(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public Category apply(CategoryDto categoryDto) {
        return this.categoryDao.findById(categoryDto.getId()).orElse(null);
    }
}
