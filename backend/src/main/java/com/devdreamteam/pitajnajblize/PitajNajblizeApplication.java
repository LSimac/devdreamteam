package com.devdreamteam.pitajnajblize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PitajNajblizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PitajNajblizeApplication.class, args);
    }

}
