package com.devdreamteam.pitajnajblize.controller.impl;


import com.devdreamteam.pitajnajblize.controller.AnswerController;
import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.service.AnswerService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnswerControllerImpl implements AnswerController {

    private final AnswerService answerService;

    @Autowired
    public AnswerControllerImpl(AnswerService answerService) {
        this.answerService = answerService;
    }

    @Override
    public AnswerDto getAnswer(long id) {
        return answerService.getAnswer(id);
    }

    @Override
    public void deleteAnswer(long id) {
        answerService.deleteAnswer(id);
    }

    @Override
    public AnswerDto acceptAnswer(long id, boolean shouldAccept) {
        UserDto currentUser = SessionUtil.getLoggedUser();
        AnswerDto answer = answerService.getAnswer(id);

        answer.setIsAccepted(shouldAccept);
        //todo(LSimac/jcapek_): Vidjet sta sa ovim
        //moramo vidit kaj bumo s tim id-em uopce radili
//        answer.idOfUserWhoAccepted(currentUser.getId());
        return answerService.updateAnswer(id, answer);
    }

    @Override
    public AnswerDto updateAnswer(AnswerDto answer, long id) {
        return answerService.updateAnswer(id, answer);
    }

}
