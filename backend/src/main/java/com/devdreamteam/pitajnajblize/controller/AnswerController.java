package com.devdreamteam.pitajnajblize.controller;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/answers")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_NORMAL')")
public interface AnswerController {

    @GetMapping("/{id}")
    AnswerDto getAnswer(@PathVariable long id);

    @DeleteMapping("/{id}")
    void deleteAnswer(@PathVariable long id);

    @PostMapping("/{id}/accept")
    AnswerDto acceptAnswer(@PathVariable long id, @RequestParam boolean shouldAccept);

    @PatchMapping("/{id}")
    AnswerDto updateAnswer(@RequestBody AnswerDto answer, @PathVariable long id);
}
