package com.devdreamteam.pitajnajblize.controller;

import com.devdreamteam.pitajnajblize.dto.*;
import com.devdreamteam.pitajnajblize.entity.Notification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RequestMapping("/user")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_NORMAL')")
public interface UserController {

    @GetMapping("/banned")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    GenericDto<Set<BanDto>> getBannedUsers();

    @PostMapping("/{id}/ban")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void banUser(@PathVariable long id, @RequestParam(required = false) String expiresOn);

    @PostMapping("/{id}/unban")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void unbanUser(@PathVariable long id);

    @PostMapping("/location")
    UserDto updateUserLocation(@RequestBody LocationDto locationDto);

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    GenericDto<Set<UserDto>> getAllUsers();

    @GetMapping("/professionals")
    GenericDto<Set<UserDto>> getProfessionalUsers();

    @GetMapping("")
    UserDto getLoggedUser();

    @PatchMapping("")
    UserDto updateLoggedUser(@RequestBody UserDto userPatch);

    @GetMapping("/{id}")
    UserDto getUser(@PathVariable long id);

    @GetMapping("/questions")
    GenericDto<Set<QuestionDto>> getLoggedUserQuestions(@RequestBody(required = false) GenericDto<Set<Long>> categoryFilterList);

    @PostMapping("/{professionalId}/grade")
    void gradeProfessional(@PathVariable long professionalId, @RequestParam long questionId, @RequestParam int grade);

    @GetMapping("/questions/claimed")
    GenericDto<Set<QuestionDto>> getLoggedProfessionalsClaimedQuestions(@RequestBody(required = false) GenericDto<Set<Long>> categoryFilterList);

    @GetMapping("/notifications")
    GenericDto<Set<NotificationDto>> getLoggedUserNotifications();
}
