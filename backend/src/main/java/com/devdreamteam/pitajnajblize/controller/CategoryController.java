package com.devdreamteam.pitajnajblize.controller;

import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.dto.GenericDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

/**
 * REST controller for categories.
 *
 * @author Jan Capek
 */
@RequestMapping("/categories")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_NORMAL')")
public interface CategoryController {

    @GetMapping
    GenericDto<Set<CategoryDto>> getAll();

    @GetMapping("/{id}")
    CategoryDto getById(@PathVariable long id);
}
