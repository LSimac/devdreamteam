package com.devdreamteam.pitajnajblize.controller.impl;

import com.devdreamteam.pitajnajblize.controller.LoginRegisterController;
import com.devdreamteam.pitajnajblize.dto.GenericDto;
import com.devdreamteam.pitajnajblize.dto.RegisterLoginDto;
import com.devdreamteam.pitajnajblize.entity.Ban;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.exception.ForbiddenException;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.exception.UnauthorizedException;
import com.devdreamteam.pitajnajblize.repository.BanDao;
import com.devdreamteam.pitajnajblize.repository.TokenDao;
import com.devdreamteam.pitajnajblize.repository.UserDao;
import com.devdreamteam.pitajnajblize.service.AuthService;
import com.devdreamteam.pitajnajblize.service.UserService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@RestController
public class LoginRegisterControllerImpl implements LoginRegisterController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final AuthService authService;
    private final UserDetailsService userDetailsService;
    private final UserDao userDao;
    private final BanDao banDao;

    @Autowired
    public LoginRegisterControllerImpl(UserService userService, AuthenticationManager authenticationManager, AuthService authService, UserDetailsService userDetailsService, UserDao userDao, BanDao banDao) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.authService = authService;
        this.userDetailsService = userDetailsService;
        this.userDao = userDao;
        this.banDao = banDao;
    }

    @Override
    public GenericDto<String> login(RegisterLoginDto form) throws InvalidDataException {
        User user = userDao.getUserByUsername(form.getUsername());

        if (user == null) {
            throw new InvalidDataException("Invalid username or password.");
        }

        if(Objects.isNull(banDao.findBanByUserId(user.getId())) == false){
            throw new ForbiddenException("User is banned");
        }

        form.setUsername(String.valueOf(user.getId()));
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(form.getUsername(), form.getPassword())
            );
        } catch (BadCredentialsException e) {
            throw new InvalidDataException("Invalid username or password.");
        }
        UserDetails userDetails = userDetailsService
                .loadUserByUsername(form.getUsername());
        String token = authService.generateToken(Long.parseLong(userDetails.getUsername())).getToken();
        GenericDto<String> dto = new GenericDto<>();
        dto.setPayload(token);
        return dto;
    }

    @Override
    public GenericDto<String> register(RegisterLoginDto form) {
        userService.createUser(form);
        return login(form);
    }

    @Override
    public void logout(String authorizationHeader) {
        if (authorizationHeader.startsWith("Bearer ") == false) {
            throw new InvalidDataException("Given authorization header is invalid.");
        }
        String token = authorizationHeader.substring("Bearer ".length());
        authService.deleteToken(token);
    }
}
