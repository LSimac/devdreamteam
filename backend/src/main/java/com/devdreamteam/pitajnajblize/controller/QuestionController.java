package com.devdreamteam.pitajnajblize.controller;

import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.GenericDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RequestMapping("/questions")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_NORMAL')")
public interface QuestionController {

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    GenericDto<Set<QuestionDto>> getAllQuestions(GenericDto<Set<Long>> categoryIdFilters);

    @GetMapping("")
    GenericDto<List<QuestionDto>> getQuestions(GenericDto<Set<Long>> categoryIdFilters);

    @GetMapping("/prof")
    GenericDto<List<QuestionDto>> getProfessionalQuestions(GenericDto<Set<Long>> categoryIdFilters);

    @PostMapping("")
    QuestionDto postQuestion(@RequestBody QuestionDto question);

    @GetMapping("/{id}")
    QuestionDto getQuestion(@PathVariable long id);

    @PatchMapping("/{id}")
    QuestionDto updateQuestion(@RequestBody QuestionDto question, @PathVariable long id);

    @DeleteMapping("/{id}")
    void deleteQuestion(@PathVariable long id);

    @PostMapping("/{id}/close")
    QuestionDto closeQuestionForNewAnswers(@PathVariable long id);

    @PostMapping("/{id}/open")
    QuestionDto openQuestionQuestionForNewAnswers(@PathVariable long id);

    @PostMapping("/{id}/claim")
    @PreAuthorize("hasRole('ROLE_PROFESSIONAL')")
    QuestionDto claimQuestion(@PathVariable long id, @RequestParam boolean shouldClaim);

    @GetMapping("/{id}/answers")
    GenericDto<Set<AnswerDto>> getQuestionAnswers(@PathVariable long id);

    @PostMapping("/{id}/answers")
    AnswerDto postAnswerForQuestion(@PathVariable long id, @RequestBody AnswerDto answer);

    @GetMapping("/professional")
    @PreAuthorize("hasRole('ROLE_PROFESSIONAL')")
    GenericDto<Set<QuestionDto>> getProfessionalsDirectQuestions(@RequestBody(required = false) GenericDto<Set<Long>> categoryFilterList);
}
