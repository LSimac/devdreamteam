package com.devdreamteam.pitajnajblize.controller.impl;

import com.devdreamteam.pitajnajblize.controller.ImageController;
import com.devdreamteam.pitajnajblize.dto.GenericDto;
import com.devdreamteam.pitajnajblize.dto.ImageDto;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@RestController
public class ImageControllerImpl implements ImageController {
    @Autowired
    private ImageService imageService;

    @Override
    public GenericDto<Long> postImage(MultipartFile image) {
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(image.getInputStream());
        } catch (IOException e) {
            throw new InvalidDataException("Image is invalid.");
        }
        if (image.getContentType() == null) {
            throw new InvalidDataException("Invalid content type.");
        }
        String formatName = image.getContentType().split("/")[1];
        ImageDto imageDto = imageService.createImage(bufferedImage, formatName);
        GenericDto<Long> response = new GenericDto<>();
        response.setPayload(imageDto.getId());
        return response;
    }

    @Override
    public ResponseEntity<byte[]> getImage(long id) {
        try {
            ImageDto image = imageService.getImage(id);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("image/" + image.getRealPath().split("\\.")[2]))
                    .body(Files.readAllBytes(Paths.get(image.getRealPath())));
        } catch (IOException e) {
            // todo log m ivic
            throw new InvalidDataException("Image with this id is missing or is unavailable.");
        }
    }
}
