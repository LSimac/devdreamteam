package com.devdreamteam.pitajnajblize.controller.impl;

import com.devdreamteam.pitajnajblize.controller.UserController;
import com.devdreamteam.pitajnajblize.dto.*;
import com.devdreamteam.pitajnajblize.dto.patcher.UserDtoPatcher;
import com.devdreamteam.pitajnajblize.service.NotificationService;
import com.devdreamteam.pitajnajblize.service.QuestionService;
import com.devdreamteam.pitajnajblize.service.UserService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@RestController
public class UserControllerImpl implements UserController {

    private final UserService userService;
    private final QuestionService questionService;
    private final NotificationService notificationService;
    private final UserDtoPatcher userPatcher;

    @Autowired
    public UserControllerImpl(UserService userService, QuestionService questionService, NotificationService notificationService, UserDtoPatcher userPatcher) {
        this.userService = Objects.requireNonNull(userService);
        this.questionService = Objects.requireNonNull(questionService);
        this.notificationService = Objects.requireNonNull(notificationService);
        this.userPatcher = Objects.requireNonNull(userPatcher);
    }

    @Override
    public GenericDto<Set<BanDto>> getBannedUsers() {
        GenericDto<Set<BanDto>> data = new GenericDto<>();

        data.setPayload(userService.getAllBannedUsers());
        return data;
    }

    @Override
    public void unbanUser(long id) {
        userService.unbanUser(id);
    }

    @Override
    public void banUser(long id,String expiresOn) {
        userService.banUser(id,expiresOn);
    }

    @Override
    public UserDto updateUserLocation(LocationDto locationDto) {
        UserDto userPatch = new UserDto();
        userPatch.setLocation(locationDto);
        UserDto updatedUser = userPatcher.apply(getLoggedUser(), userPatch);
        return userService.updateUser(SessionUtil.getLoggedUser().getId(), updatedUser);

    }

    @Override
    public GenericDto<Set<UserDto>> getProfessionalUsers() {
        GenericDto<Set<UserDto>> data = new GenericDto<>();

        data.setPayload(userService.getAllProfessionalUsers());

        return data;
    }

    @Override
    public GenericDto<Set<UserDto>> getAllUsers() {
        GenericDto<Set<UserDto>> payload = new GenericDto<>();
        payload.setPayload(userService.getAllUsers());
        return payload;
    }

    @Override
    public UserDto getLoggedUser() {
        UserDto user = SessionUtil.getLoggedUser();
        return userService.getUser(SessionUtil.getLoggedUser().getId());
    }


    @Override
    public UserDto updateLoggedUser(UserDto userPatch) {
        UserDto updatedUser = userPatcher.apply(getLoggedUser(), userPatch);
        prepareUserDtoForUpdate(updatedUser, userPatch);
        return userService.updateUser(SessionUtil.getLoggedUser().getId(), updatedUser);
    }

    /**
     * This method is created for users to be able to switch between roles without
     * violating UserDto data integrity.
     * <p>
     * Method updates given user considering their role.
     * <p>
     * If some role specific properties are null, patch does not update them
     * but they have defaults, they will be set to the role specific defaults.
     * <p>
     * If some role specific properties aren't null and patch does not update them
     * but the role requires them to be null, they will be set to null.
     *
     * @param user  Updated user dto.
     * @param patch Patch applied before update.
     */
    private static void prepareUserDtoForUpdate(UserDto user, UserDto patch) {
        switch (user.getRole()) {
            case "professional":
                prepareProfessionalUserForUpdate(user, patch);
                break;
            case "admin":
                prepareAdminUserForUpdate(user, patch);
                break;
            case "normal":
                prepareNormalUserForUpdate(user, patch);
                break;
        }
    }

    private static void prepareProfessionalUserForUpdate(UserDto user, UserDto patch) {
        if (patch.getDescription() == null && user.getDescription() == null) {
            user.setDescription("");
        }
        if (patch.getCategories() == null && user.getCategories() == null) {
            user.setCategories(new HashSet<>(0));
        }
        if (patch.getGrade() == null && user.getGrade() == null) {
            user.setGrade(0.0);
        }
    }

    private static void prepareAdminUserForUpdate(UserDto user, UserDto patch) {
        if (patch.getLocation() == null && user.getLocation() != null) {
            user.setLocation(null);
        }
        prepareNormalUserForUpdate(user, patch);
    }

    private static void prepareNormalUserForUpdate(UserDto user, UserDto patch) {
        if (patch.getDescription() == null && user.getDescription() != null) {
            user.setDescription(null);
        }
        if (patch.getCategories() == null && user.getCategories() != null) {
            user.setCategories(null);
        }
        if (patch.getGrade() == null && user.getGrade() != null) {
            user.setGrade(null);
        }
    }

    @Override
    public UserDto getUser(long id) {
        return userService.getUser(id);
    }

    @Override
    public GenericDto<Set<QuestionDto>> getLoggedUserQuestions(GenericDto<Set<Long>> categoryFilterList) {
        GenericDto<Set<QuestionDto>> result = new GenericDto<>();
        result.setPayload(questionService.getQuestionsFromUser(SessionUtil.getLoggedUser().getId(), Objects.isNull(categoryFilterList) ? null : categoryFilterList.getPayload()));
        return result;
    }

    @Override
    public void gradeProfessional(long professionalId, long questionId, int grade) {
        userService.gradeProfessional(professionalId, questionId, grade);
    }

    @Override
    public GenericDto<Set<QuestionDto>> getLoggedProfessionalsClaimedQuestions(GenericDto<Set<Long>> categoryFilterList) {
        GenericDto<Set<QuestionDto>> result = new GenericDto<>();
        result.setPayload(questionService.getProfessionalClaimedQuestions(SessionUtil.getLoggedUser().getId(),Objects.isNull(categoryFilterList) ? null : categoryFilterList.getPayload()));
        return result;
    }

    @Override
    public GenericDto<Set<NotificationDto>> getLoggedUserNotifications() {
        GenericDto<Set<NotificationDto>> result = new GenericDto<>();
        result.setPayload(notificationService.getNotificationsForUser(SessionUtil.getLoggedUser().getId()));
        return result;
    }
}
