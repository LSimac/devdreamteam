package com.devdreamteam.pitajnajblize.controller.impl;

import com.devdreamteam.pitajnajblize.controller.QuestionController;
import com.devdreamteam.pitajnajblize.dto.AnswerDto;
import com.devdreamteam.pitajnajblize.dto.GenericDto;
import com.devdreamteam.pitajnajblize.dto.QuestionDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.exception.UnauthorizedException;
import com.devdreamteam.pitajnajblize.service.AnswerService;
import com.devdreamteam.pitajnajblize.service.QuestionService;
import com.devdreamteam.pitajnajblize.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Domagoj Lokner
 */
@RestController
public class QuestionControllerImpl implements QuestionController {

    private final QuestionService questionService;
    private final AnswerService answerService;

    @Autowired
    public QuestionControllerImpl(QuestionService questionService, AnswerService answerService) {
        this.questionService = questionService;
        this.answerService = answerService;
    }

    @Override
    public GenericDto<List<QuestionDto>> getProfessionalQuestions(GenericDto<Set<Long>> categoryIdFilters) {
        List<QuestionDto> questions = questionService.getProfessionalQuestion(categoryIdFilters.getPayload());
        GenericDto<List<QuestionDto>> data = new GenericDto<>();
        data.setPayload(questions);
        return data;
    }

    @Override
    public GenericDto<Set<QuestionDto>> getAllQuestions(GenericDto<Set<Long>> categoryFilterList) {
        Set<QuestionDto> questions = questionService.getAllQuestions(categoryFilterList.getPayload());
        GenericDto<Set<QuestionDto>> data = new GenericDto<>();
        data.setPayload(questions);
        return data;
    }

    @Override
    public GenericDto<Set<QuestionDto>> getProfessionalsDirectQuestions(GenericDto<Set<Long>> categoryFilterList) {
        UserDto currentUser = SessionUtil.getLoggedUser();
        Set<QuestionDto> questions = questionService.getProfessionalsDirectQuestions(currentUser.getId(),Objects.isNull(categoryFilterList) ? null : categoryFilterList.getPayload());

        GenericDto<Set<QuestionDto>> data = new GenericDto<>();
        data.setPayload(questions);
        return data;
    }

    @Override
    public GenericDto<List<QuestionDto>> getQuestions(GenericDto<Set<Long>> categoryIdFilters) {
        UserDto currentUser = SessionUtil.getLoggedUser();
        List<QuestionDto> questions = questionService
                .getQuestionsNearUser(currentUser.getId(), categoryIdFilters.getPayload());
        GenericDto<List<QuestionDto>> data = new GenericDto<>();
        data.setPayload(questions);
        return data;
    }

    @Override
    public QuestionDto postQuestion(QuestionDto question) {

        return questionService.createQuestion(question);
    }

    @Override
    public QuestionDto getQuestion(long id) {
        return questionService.getQuestion(id);
    }

    @Override
    public QuestionDto updateQuestion(QuestionDto updatedQuestion, long id) {
        return questionService.updateQuestion(id, updatedQuestion);
    }

    @Override
    public void deleteQuestion(long id) {
        questionService.deleteQuestion(id);
    }

    @Override
    public QuestionDto closeQuestionForNewAnswers(long id) {
        QuestionDto question = new QuestionDto();
        question.setStatus(QuestionService.STATUS_CLOSED);
        return questionService.updateQuestion(id, question);
    }

    @Override
    public QuestionDto openQuestionQuestionForNewAnswers(long id) {
        QuestionDto question = new QuestionDto();
        question.setStatus(QuestionService.STATUS_OPENED);

        return questionService.updateQuestion(id, question);
    }

    @Override
    public QuestionDto claimQuestion(long id, boolean shouldClaim) {
        UserDto currentUser = SessionUtil.getLoggedUser();
        QuestionDto question = questionService.getQuestion(id);
        QuestionDto updatedQuestion = new QuestionDto();

        if (question.getProfessionalIds().contains(currentUser.getId()) == false) {
            throw new UnauthorizedException();
        } else if (shouldClaim && Objects.isNull(question.getIdOfProfessionalWhoClaimed())) {
            updatedQuestion.setIdOfProfessionalWhoClaimed(currentUser.getId());
        } else {
            Set<Long> professionalIds = question.getProfessionalIds();
            professionalIds.remove(currentUser.getId());
            updatedQuestion.setProfessionalIds(professionalIds);
        }

        return questionService.updateQuestion(id, updatedQuestion);
    }

    @Override
    public GenericDto<Set<AnswerDto>> getQuestionAnswers(long id) {
        Set<AnswerDto> answers = new TreeSet<>((q1, q2) -> Long.compare(q1.getId(),q2.getId()));
        answers.addAll(answerService.getAnswersForQuestion(id));
        GenericDto<Set<AnswerDto>> data = new GenericDto<>();
        data.setPayload(answers);
        return data;
    }

    @Override
    public AnswerDto postAnswerForQuestion(long id, AnswerDto answer) {
        return answerService.createAnswer(answer);
    }

}
