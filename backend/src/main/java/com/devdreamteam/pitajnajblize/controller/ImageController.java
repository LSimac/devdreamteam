package com.devdreamteam.pitajnajblize.controller;

import com.devdreamteam.pitajnajblize.dto.GenericDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/image")
@CrossOrigin(origins = "*")
public interface ImageController {

    @PostMapping("")
    GenericDto<Long> postImage(@RequestParam("image") MultipartFile image);

    @GetMapping("/{id}")
    ResponseEntity<byte[]> getImage(@PathVariable long id);

}
