package com.devdreamteam.pitajnajblize.controller;

import com.devdreamteam.pitajnajblize.dto.GenericDto;
import com.devdreamteam.pitajnajblize.dto.RegisterLoginDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RequestMapping("/")
@CrossOrigin(origins = "*")
public interface LoginRegisterController {

    @PostMapping("/login")
    @PreAuthorize("authentication.principal == 'anonymousUser'")
    GenericDto<String> login(@RequestBody RegisterLoginDto form);

    @PostMapping("/register")
    @PreAuthorize("hasRole('ROLE_ADMIN') or authentication.principal == 'anonymousUser'")
    GenericDto<String> register(@RequestBody RegisterLoginDto form);

    @PostMapping("/logoff")
    @PreAuthorize("authentication.principal != 'anonymousUser'")
    void logout(@RequestHeader("Authorization") String authorizationHeader);
}
