package com.devdreamteam.pitajnajblize.controller.impl;

import com.devdreamteam.pitajnajblize.controller.CategoryController;
import com.devdreamteam.pitajnajblize.dto.CategoryDto;
import com.devdreamteam.pitajnajblize.dto.GenericDto;
import com.devdreamteam.pitajnajblize.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.Set;

/**
 * @author Jan Capek
 */
@RestController
public class CategoryControllerImpl implements CategoryController {

    private final CategoryService categoryService;

    /**
     * @throws NullPointerException If given service is {@code null}.
     */
    @Autowired
    public CategoryControllerImpl(CategoryService categoryService) {
        this.categoryService = Objects.requireNonNull(categoryService);
    }

    @Override
    public GenericDto<Set<CategoryDto>> getAll() {
        GenericDto<Set<CategoryDto>> result = new GenericDto<>();
        result.setPayload(categoryService.getAll());
        return result;
    }

    @Override
    public CategoryDto getById(long id) {
        return categoryService.getById(id);
    }
}
