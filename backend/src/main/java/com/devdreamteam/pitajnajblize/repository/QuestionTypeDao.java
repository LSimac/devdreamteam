package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.questions.QuestionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface QuestionTypeDao extends JpaRepository<QuestionType, Long> {

    Optional<QuestionType> findByName(String name);

}
