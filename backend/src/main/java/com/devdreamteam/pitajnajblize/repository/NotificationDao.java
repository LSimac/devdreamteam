package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.Notification;
import com.devdreamteam.pitajnajblize.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationDao extends JpaRepository<Notification, Long> {

    List<Notification> findAllByRecipientOrderByCreationTimestampDesc(User user);

    boolean existsByRecipientAndEntityReferenceIdAndEntityReferenceType(User user, Long entityReferenceId, String entityReferenceType);
}
