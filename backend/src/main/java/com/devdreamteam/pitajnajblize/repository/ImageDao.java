package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Repository
public interface ImageDao extends JpaRepository<Image, Long> {

    Image findByPath(String path);
}

