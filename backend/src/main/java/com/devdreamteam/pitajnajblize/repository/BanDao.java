package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.Ban;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BanDao extends JpaRepository<Ban,Long> {

    @Query(value = "SELECT b FROM Ban b WHERE b.user.id = :userId")
    Ban findBanByUserId(@Param("userId") Long userId);
}
