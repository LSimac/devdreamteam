package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.questions.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface QuestionDao extends JpaRepository<Question, Long> {

    @Query(value = "SELECT q FROM Question q WHERE q.message.author.id = :userId")
    Set<Question> findQuestionsByUserId(@Param("userId") Long userId);

//    @Query("SELECT q FROM QUESTIONS q WHERE q.message.author.id = ?1 AND q.category IN ?2")
//    Set<Question> findQuestionsByUserId(long UserId, Set<Long> categories);

    @Query("SELECT q FROM Question q WHERE q.professionalWhoClaimed.id = :userId")
    Set<Question> findQuestionsByProfessionalWhoClaimed(@Param("userId") Long userId);


}
