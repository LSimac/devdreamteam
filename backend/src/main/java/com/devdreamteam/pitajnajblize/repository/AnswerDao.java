package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;
@Repository
public interface AnswerDao extends JpaRepository<Answer, Long> {

    @Query(value = "SELECT a FROM Answer a WHERE a.accepted = true AND a.question.id = :questionId")
    Set<Answer> findAcceptedAnswersForQuestion(@Param("questionId") Long userId);
}
