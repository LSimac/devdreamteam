package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Repository
public interface TokenDao extends JpaRepository<Token, Long> {

    Token findByToken(String token);

    @Transactional
    @Modifying
    @Query("DELETE FROM Token t WHERE t.token=:v")
    void deleteByToken(@Param("v") String tokenValue);
}
