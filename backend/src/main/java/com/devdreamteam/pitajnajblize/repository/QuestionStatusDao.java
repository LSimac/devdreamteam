package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.questions.QuestionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface QuestionStatusDao extends JpaRepository<QuestionStatus, Long> {

    Optional<QuestionStatus> findByName(String name);
}
