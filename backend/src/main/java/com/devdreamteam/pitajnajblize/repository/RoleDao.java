package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@Repository
public interface RoleDao extends JpaRepository<Role, Long> {
    Role getByName(String name);
}
