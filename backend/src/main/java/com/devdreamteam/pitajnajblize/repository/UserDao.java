package com.devdreamteam.pitajnajblize.repository;

import com.devdreamteam.pitajnajblize.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDao extends JpaRepository<User, Long> {

    User getUserByUsername(String username);

    User getUserById(long id);
}
