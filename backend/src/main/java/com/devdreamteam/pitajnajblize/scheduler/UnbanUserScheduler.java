package com.devdreamteam.pitajnajblize.scheduler;

import com.devdreamteam.pitajnajblize.entity.Ban;
import com.devdreamteam.pitajnajblize.repository.BanDao;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
public class UnbanUserScheduler {

    private final BanDao banDao;

    public UnbanUserScheduler(BanDao banDao) {
        this.banDao = banDao;
    }

    @Scheduled(fixedRate = 60000)
    public void unbanUsersWithExpiredDate(){
        List<Ban> listOfBannedUsers = banDao.findAll();

        for(Ban ban : listOfBannedUsers){
            if(Objects.isNull(ban.getExpirationDate())){
                continue;
            }

            if(ban.getExpirationDate().getTime() <= new Date().getTime()){
                banDao.delete(ban);
            }

        }
    }
}
