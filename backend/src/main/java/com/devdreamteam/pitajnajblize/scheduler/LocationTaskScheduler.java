
package com.devdreamteam.pitajnajblize.scheduler;


import com.devdreamteam.pitajnajblize.dto.mapper.QuestionDtoMapper;
import com.devdreamteam.pitajnajblize.entity.questions.Answer;
import com.devdreamteam.pitajnajblize.entity.questions.Question;
import com.devdreamteam.pitajnajblize.repository.QuestionDao;
import com.devdreamteam.pitajnajblize.service.NotificationService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class LocationTaskScheduler {

    private final QuestionDao questionDao;

    private final QuestionDtoMapper questionDtoMapper;

    private final NotificationService notificationService;

    public LocationTaskScheduler(QuestionDao questionDao, QuestionDtoMapper questionDtoMapper, NotificationService notificationService) {
        this.questionDao = questionDao;
        this.questionDtoMapper = questionDtoMapper;
        this.notificationService = notificationService;
    }

    @Scheduled(fixedRate = 30000)
    public void increaseQuestionRange(){
        List<Question> questions = questionDao.findAll();
        List<Question> unansweredQuestions = new ArrayList<>();

        questionLoop: for(Question q : questions){
            Set<Answer> answers = q.getAnswers();

            for(Answer a : answers){
                if(a.isAccepted()) continue questionLoop;
            }

            unansweredQuestions.add(q);
        }

        unansweredQuestions.forEach(this::incrementQuestionRange);

    }


    private void incrementQuestionRange(Question question){
        int level = question.getLevel();

        if(level == 8) {
            return;
        }

        level++;

        question.setLevel(level);
        question = questionDao.save(question);
        notificationService.notifyForQuestion(questionDtoMapper.apply(question));
    }
}
