package com.devdreamteam.pitajnajblize;

import com.devdreamteam.pitajnajblize.dto.RegisterLoginDto;
import com.devdreamteam.pitajnajblize.dto.UserDto;
import com.devdreamteam.pitajnajblize.dto.mapper.UserDtoMapper;
import com.devdreamteam.pitajnajblize.dto.validator.UserDtoValidator;
import com.devdreamteam.pitajnajblize.entity.Role;
import com.devdreamteam.pitajnajblize.entity.User;
import com.devdreamteam.pitajnajblize.exception.EntityNotFoundException;
import com.devdreamteam.pitajnajblize.exception.InvalidDataException;
import com.devdreamteam.pitajnajblize.repository.*;
import com.devdreamteam.pitajnajblize.security.UserDetailsWrapperDefault;
import com.devdreamteam.pitajnajblize.service.impl.UserServiceJpa;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static junit.framework.TestCase.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

/**
 * @author Marko Ivić
 * @version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    // useless
    @Mock
    private QuestionDao questionDao;
    @Mock
    private CategoryDao categoryDao;
    @Mock
    private RoleDao roleDao;
    @Mock
    private QuestionStatusDao questionStatusDao;
    @Mock
    private QuestionTypeDao questionTypeDao;
    @Mock
    private LocationDao locationDao;
    @Mock
    private UserDtoMapper userDtoMapper;
    @Mock
    private UserDtoValidator userDtoValidator;
    @Mock
    private BanDao banDao;
    @Mock
    private ImageDao imageDao;
    // useless


    @InjectMocks
    private UserServiceJpa userService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserDao userDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void setup() {
        when(userDtoMapper.apply(any(User.class))).thenReturn(getUserDto());
        when(passwordEncoder.encode(any(CharSequence.class))).thenReturn("pass1");
        when(userDao.getUserByUsername("existent")).thenReturn(new User());
        User user = new User();
        RegisterLoginDto data = getRegisterLoginDto();
        user.setUsername(data.getUsername());
        user.setPassHash("pass1");
        user.setId(1L);
        when(userDao.save(any(User.class))).thenReturn(user);
        Optional<User> optionalUser = Optional.of(user);
        when(userDao.findById(1L)).thenReturn(optionalUser);
        List<User> users = createList(user);
        when(userDao.findAll()).thenReturn(users);
    }

    private List<User> createList(User user) {
        List<User> users = new ArrayList<>();
        users.add(user);
        User user2 = new User();
        user2.setUsername("user2");
        user2.setId(2L);
        user2.setPassHash("pass");
        Role role = new Role();
        role.setName("normal");
        user.setRole(role);
        user2.setRole(role);
        users.add(user2);
        return users;
    }

    @Before
    public void removeSecurity() {
        UserDto userDto = getUserDto();
        User user = new User();
        user.setId(userDto.getId());
        Role role = new Role();
        role.setId(1L);
        role.setName("admin");
        user.setRole(role);

        UserDetailsWrapperDefault userDetailsWrapperDefault = new UserDetailsWrapperDefault(user);

        Authentication authentication = Mockito.mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userDetailsWrapperDefault);

        SecurityContextImpl securityContextImpl = new SecurityContextImpl();
        securityContextImpl.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContextImpl);
    }



    private UserDto getUserDto() {
        UserDto userDto = new UserDto();
        userDto.setUsername("user");
        userDto.setRole("normal");
        userDto.setId(1L);
        return userDto;
    }

    private RegisterLoginDto getRegisterLoginDto() {
        RegisterLoginDto registerLoginDto = new RegisterLoginDto();
        registerLoginDto.setUsername("user");
        registerLoginDto.setPassword("pass");
        registerLoginDto.setRepeatedPassword("pass");
        registerLoginDto.setRole("normal");
        return registerLoginDto;
    }

    ///////////////////////////////////////////////
    /////BEWARE OF THE TESTS BEYOND THIS POINT/////
    ///////////////////////////////////////////////

    @Test
    public void createUserTest() {
        UserDto userDto = userService.createUser(getRegisterLoginDto());
        assertEquals("user", userDto.getUsername());
    }

    @Test(expected = InvalidDataException.class)
    public void notMatchingPasswordsTest() {
        RegisterLoginDto registerLoginDto = getRegisterLoginDto();
        registerLoginDto.setRepeatedPassword("different pass");
        userService.createUser(registerLoginDto);
    }

    @Test(expected = InvalidDataException.class)
    public void userAlreadyExistsTest() {
        RegisterLoginDto registerLoginDto = getRegisterLoginDto();
        registerLoginDto.setUsername("existent");
        userService.createUser(registerLoginDto);
    }

    @Test
    public void getUserByIdTest() {
        UserDto userDto = userService.getUser(1L);
        assertEquals(1L, (long) userDto.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getNonExistentUserTest() {
        assertNull(userService.getUser(3L));
    }

    @Test
    public void getAllUsersTest() {
        List<UserDto> users = new ArrayList<>(userService.getAllUsers());
        if (users.get(0).getId() != 1L) {
            throw new InvalidDataException();
        }
    }

}
