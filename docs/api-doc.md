# REST API service doc

## Explanation of notations in JSON examples
Notation | Explanation
---------|------------
"optionA &#124; optionB" | Either will be "optionA" or "optionB"
"\[string or null\]" | Explanation of the value a field has (not necessarily type of string)

## Frequently used JSON objects
### PublicQuestionGET
```json
{
  "id": "",
  "title": "",
  "message": {
    "text": "",
    "imageHashes": ["", ""]
  },
  "location": {
    "longitude": 23.32,
    "latitude": 32.324
  },
  "status": "open | closed",
  "isAnswered": true,
  "creationTimestamp": "2015-06-02T21:34:33.616Z",
  "type": "public"
}
```
### PublicQuestionPOST
```json
{
  "title": "",
  "message": {
    "text": "",
    "imageHashes": ["", ""]
  },
  "location": {
    "longitude": 23.32,
    "latitude": 32.324
  }
}
```
### ProfessionalQuestionGET
```json
{
  "id": "",
  "title": "",
  "message": {
    "text": "",
    "imageHashes": ["", ""]
  },
  "location": {
    "longitude": 23.32,
    "latitude": 32.324
  },
  "status": "open | closed",
  "isAnswered": true,
  "creationTimestamp": "2015-06-02T21:34:33.616Z",
  "type": "professional",
  "professionalIds": [],
  "professionalsIdWhoClaimed": "[id of the professional who claimed or null]" 
}
```
### ProfessionalQuestionPOST
```json
{
  "title": "",
  "message": {
    "text": "",
    "imageHashes": ["", ""]
  },
  "location": {
    "longitude": 23.32,
    "latitude": 32.324
  },
  "requestedProfessionalsIds": ["", ""]
}
```
### AnswerGET
```json
{
  "id": "",
  "message": {
    "text": "",
    "imageHashes": ["", ""]
  },
  "creatorId": "",
  "timestamp": "",
  "questionId": "",
  "isAccepted": true
}
```
### AnswerPOST
```json
{
  "message": {
    "text": "",
    "imageHashes": ["", ""]
  }
}
```
### UserGET
```json
{
  "id": "",
  "username": "",
  "role": "normal",
  "avatarHash": ""
}
```
### UserPUT
```json
{
  "username": "always needs to be present",
  "avatarHash": "always needs to be present",
  "oldPassword": "string or null/undefined if not changed",
  "password": "string or null/undefined if not changed",
  "repeatedPassword": "string or null/undefined if not changed"
}
```
### ProfessionalGET
```json
{
  "id": "",
  "username": "",
  "role": "professional",
  "avatarHash": "",
  "categories": ["", ""],
  "description": ""
}
```
### ProfessionalPUT
```json
{
  "username": "always needs to be present",
  "avatarHash": "always needs to be present",
  "categories": ["", ""],
  "description": "always needs to be present",
  "oldPassword": "string or null/undefined if not changed",
  "password": "string or null/undefined if not changed",
  "repeatedPassword": "string or null/undefined if not changed"
}
```

## Routes
### `/registration`
#### POST
```json
{
  "username": "",
  "password": "",
  "repeatedPassword": "",
  "role": "professional | normal"
}
```
##### Responses
* `200` - ok 
    ```json
    {
      "accessToken": ""
    }
    ```
* `400` - invalid json
* `406` - invalid registration data
    ```json
    {
      "message": ""
    }
    ```

### `/login`
#### POST
```json
{
  "username": "",
  "password": ""
}
```
##### Responses
* `200` - ok
    ```json
    {
      "accessToken": ""
    }
    ```
* `406` - unacceptable data
    ```json
    {
      "message": ""
    }
    ```
  
  
  
  
### `/user/{id}`
#### GET
Gets information about user **OR** professional.
##### Responses 
* `200` - ok
    1. [UserGET](#userget)
    2. [ProfessionalGET](#professionalget)
* `401` - unauthorized
* `404` - not found
#### PUT
Updates user's information.

Request example: [UserPUT](#userput)
##### Responses
* `200` - ok
* `400` - invalid
* `401` - unauthorized
* `404` - not found


### `/professional/{id}`
#### PUT
Updates professional's information.

Request example: [ProfessionalPUT](#professionalput)
##### Responses
* `200` - ok
* `400` - invalid
* `401` - unauthorized
* `404` - not found
  
### `/professional/{id}/grade`
#### POST
```json
{
  "grade": 4
}
```
##### Responses 
* `200` - ok
* `401` - unauthorized
* `404` - not found

### `/image`
#### POST
Uploads an image.

Content-Type: multipart/form-data
##### Responses
* `200` - ok
```json
{
  "imageHash": ""
}
```
* `401` - unauthorized

  
  
  
### `/professional/questions/claimed`
#### GET
Lists all claimed questions for logged professional.
##### Responses
* `200` - ok
    ```json
    {
      "questions": ["[ProfessionalQuestionGET]", "[ProfessionalQuestionGET]"]
    }
    ```
* `401` - unauthorized

### `/user/questions`
#### GET
Gets a list of logged user's questions (public **AND** professional).
  ```json 
  {
    "categoryFilterList": ["category_id", "category_id"]
  }
  ```
##### Responses
* `200` - ok
    ```json
    {
      "questions": ["[PublicQuestionGET]", "[ProfessionalQuestionGET]"]
    }
    ```
* `401` - unauthorized

### `/questions/{id}`
#### GET
Returns data for question with given id.
##### Responses
* `200` - ok
    1. version ([ProfessionalQuestionGET](#professionalquestionget))
    2. version ([PublicQuestionGET](#publicquestionget))
* `401` - unauthorized
* `404` - not found
#### DELETE
Deletes a question with given id.
##### Responses
* `200` - ok
* `401` - unauthorized

### `/questions/{id}/close`
#### POST
Closes a question for new answers.
##### Responses
* `200` - ok
* `401` - unauthorized
* `404` - not found

### `/questions/{id}/open`
#### POST
Opens a question for new answers.
##### Responses
* `200` - ok
* `401` - unauthorized
* `404` - not found

### `/questions/{id}/claim`
#### POST
On this endpoint professional can claim a question.
##### Responses
* `200` - ok
* `400` - question is public therefore it cannot be claimed
* `401` - unauthorized
* `404` - question not found

### `/questions/{id}/decline`
#### POST
On this endpoint professional can decline a question.
##### Responses
* `200` - ok
* `400` - question is public therefore it cannot be claimed
* `401` - unauthorized
* `404` - question not found

### `/questions/public/new`
#### POST
Creates a new public question.

Request example: [PublicQuestionPOST](#publicquestionpost)
##### Responses
* `200` - newly created question's id will be returned
    ```json
    {
      "id": ""
    }
    ``` 
* `401` - unauthorized
* `400` - invalid json

### `/questions/professional/new`
#### POST
Creates a new question for professionals.

Request example: [ProfessionalQuestionPOST](#professionalquestionpost)
##### Responses
* `200` - newly created question's id will be returned
    ```json
    {
      "id": ""
    }
    ```
* `401` - not authorized to make request
* `400` - invalid json

### `/questions/{id}/answers`
#### GET
##### Responses
 * `200` - returns list of answers
    ```json
    {
      "answers": ["[AnswerGET]", "[AnswerGET]"] 
    }
    ```
* `401` - not authorized to see data
* `404` - not found
#### POST
Creates a new answer for question with given id.

Request example: [AnswerPOST](#answerpost)
##### Responses
* `200` - ok
    ```json
    {
      "id": ""
    }
    ```
* `401` - not authorized to make request
* `400` - invalid json
* `404` - question not found

### `/answers/{id}`
#### DELETE
Deletes an answer with given id.
##### Responses
* `200` - ok
* `401` - unauthorized
#### GET
Returns an answer with given id.
##### Responses
* `200` - ok
    1. [AnswerGET](#answerget)
* `401` - unauthorized
* `404` - not found

### `/answers/{id}/accept`
#### POST
##### Responses
* `200` - ok
* `401` - unauthorized
* `404` - not found