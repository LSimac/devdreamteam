import { Router } from '@angular/router';
import { Component, ViewChild, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MatSidenav } from '@angular/material';
import { User } from 'src/app/models/user/user.model';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  @ViewChild(MatSidenav, {static: false}) sidenav: MatSidenav;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private auth: AuthService, private router: Router) {
    auth.currentUserSubject.subscribe(user => this.currentUser = user);
  }

  currentUser: User;


  get isUserAdmin():boolean{
    return this.currentUser == null || this.currentUser.role === 'admin';
  }

  openUser() {
    this.sidenav.close();
    this.router.navigate(['/user/myProfile']);
  }

  appSettings() {
  }

  userSettings() {
    this.sidenav.close();
    this.router.navigate(['/settings']);
  }
  logout() {
    this.auth.logout();
    this.sidenav.close();
    this.router.navigate(['/auth']);
  }

  openSidenav() {
    this.sidenav.open();
  }
}
