import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/models/user/user.model';
import { PhotoService } from 'src/app/services/images/photo.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  currentUser: User;
  imgUrl = `${environment.apiUrl}/image`;

  constructor(private authService: AuthService, private photoService: PhotoService) {
    this.authService.currentUserSubject.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
  }

  get userImage() {
    if (this.currentUser && this.currentUser.avatarId) {
      return this.imgUrl + '/' + this.currentUser.avatarId;
    } else {
      return 'https://simpleicon.com/wp-content/uploads/user1.png';
    }
  }

}
