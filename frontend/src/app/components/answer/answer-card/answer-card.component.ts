import { MatDialog } from '@angular/material';
import { AuthService } from './../../../services/auth/auth.service';
import { PhotoService } from '../../../services/images/photo.service';
import { AnswerService } from '../../../services/answer/answer.service';
import { Component, OnInit, Input } from '@angular/core';
import { Answer } from 'src/app/models/question/answer.model';
import { catchError } from 'rxjs/operators';
import { AnswerDialogComponent } from '../answer-dialog/answer-dialog.component';
import { Question } from 'src/app/models/question/question.model';
import { User } from 'src/app/models/user/user.model';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-answer-card',
  templateUrl: './answer-card.component.html',
  styleUrls: ['./answer-card.component.css']
})
export class AnswerCardComponent implements OnInit {
  @Input() answer: Answer;
  @Input() question: Question;

  imgUrl = `${environment.apiUrl}/image`;
  private currentUser: User;

  photosSubject: Subject<File> = new Subject<File>();

  isUserAnswerOwner: boolean;
  isUserQuestionOwner: boolean;

  constructor(private answerService: AnswerService,
    private photoService: PhotoService,
    private authService: AuthService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
    this.isUserAnswerOwner = this.currentUser.id === this.answer.message.author.id;
    this.isUserQuestionOwner = this.currentUser.id === this.question.message.author.id;
  }

  acceptAnswer() {
    this.answerService.acceptAnswer(this.answer.id).then(() => this.answer.isAccepted = true);
  }

  get userImage() {
    const author = this.question.message.author;
    if (author && author.avatarId) {
      return this.imgUrl + '/' + author.avatarId;
    } else {
      return 'https://simpleicon.com/wp-content/uploads/user1.png';
    }
  }

  editAnswer() {
    const dialogRef = this.dialog.open(AnswerDialogComponent, {
      width: '500px',
      height: '500px',
      data: { question: this.question, answer: this.answer, isEdit: true }
    });
  }
}


