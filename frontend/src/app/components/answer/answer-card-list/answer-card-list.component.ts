import { AnswerService } from './../../../services/answer/answer.service';
import { Component, OnInit, Input } from '@angular/core';
import { Answer } from 'src/app/models/question/answer.model';
import { Question } from 'src/app/models/question/question.model';

@Component({
  selector: 'app-answer-card-list',
  templateUrl: './answer-card-list.component.html',
  styleUrls: ['./answer-card-list.component.css']
})
export class AnswerCardListComponent implements OnInit {
  @Input() question: Question;

  answers: Answer[];

  constructor(private answerService: AnswerService) { }

  ngOnInit() {
    this.answerService.answers.asObservable().subscribe(answers => this.answers = answers);
    this.answerService.getAnswersForQuestion(this.question.id).then(
      answers => this.answers = answers
    );
  }

}
