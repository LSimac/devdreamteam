import { AuthService } from './../../../services/auth/auth.service';
import { AnswerService } from './../../../services/answer/answer.service';
import { User } from 'src/app/models/user/user.model';
import { UsersService } from './../../../services/users.service';
import { Answer } from './../../../models/question/answer.model';
import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { Question } from 'src/app/models/question/question.model';

@Component({
  selector: 'app-answer-edit',
  templateUrl: './answer-edit.component.html',
  styleUrls: ['./answer-edit.component.css']
})
export class AnswerEditComponent implements OnInit {
  @Output() cancle: EventEmitter<void> = new EventEmitter();
  @Input() question: Question;
  @Input() answer: Answer;
  @Input() isEdit = false;

  submitAnswerSubject: Subject<void> = new Subject<void>();

  text: string;
  currentUser: User;

  constructor(private usersService: UsersService,
    private answerService: AnswerService,
    private authService: AuthService,
    private cfg: ChangeDetectorRef) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
    if (this.answer) {
      this.text = this.answer.message.text;
    }
    this.cfg.detectChanges();
  }

  private patchAnswer() {
    this.answer.message.text = this.text;
    this.answerService.patchAnswer(this.answer.id, this.answer);
  }

  private postAnswer(imgIds: number[]) {
    const answer: Answer = {
      message: {
        author: this.currentUser,
        text: this.text,
        creationTimestamp: new Date().toISOString(),
        imageIds: imgIds
      },
      isAccepted: false,
      questionId: this.question.id
    };
    this.answerService.postAnswer(answer);
  }

  imageSet(imageIds: number[]) {
    if (this.answer) {
      this.patchAnswer();
    } else {
      this.postAnswer(imageIds);
    }
    this.onCancel();
  }

  onSubmit() {
    if (this.isEdit) {
      this.patchAnswer();
      this.onCancel();
    } else {
      this.submitAnswerSubject.next();
    }
  }

  onCancel() {
    this.cancle.emit();
  }

  get valid() {
    return this.text;
  }

}
