import { Question } from './../../../models/question/question.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Answer } from 'src/app/models/question/answer.model';

@Component({
  selector: 'app-answer-dialog',
  templateUrl: './answer-dialog.component.html',
  styleUrls: ['./answer-dialog.component.css']
})
export class AnswerDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AnswerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Question) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancleDialog() {
    this.dialogRef.close();
  }

  ngOnInit() {

  }

}
