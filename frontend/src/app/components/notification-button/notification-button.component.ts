import {Component, Inject, OnInit, OnDestroy} from '@angular/core';
import {Notification} from 'src/app/models/notification.model';
import {NotificationService} from '../../services/notification.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-notification-button',
  templateUrl: './notification-button.component.html',
  styleUrls: ['./notification-button.component.css']
})
export class NotificationButtonComponent implements OnInit, OnDestroy {

  notifications: Notification[];
  intervalRef;

  private dialogRef: MatDialogRef<NotificationPanelModalComponent>;

  constructor(private notificationService: NotificationService, private  dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.startFetchingNotifications();
    this.intervalRef = setInterval(this.startFetchingNotifications.bind(this), 5 * 1000);
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalRef);
  }

  get numOfNotif() {
    if (this.notifications) {
      return this.notifications.length;
    }
    return 0;
  }

  private async startFetchingNotifications(): Promise<void> {
    this.notifications = await this.notificationService.getNotifications();
    if (this.dialogRef !== undefined && this.dialogRef.componentInstance !== null) {
      this.dialogRef.componentInstance.notifications = this.notifications;
    }
  }

  private async delay(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
  }

  openDialog(): void {
    this.dialogRef = this.dialog.open(NotificationPanelModalComponent, {
      width: '1000px',
      data: this.notifications ? this.notifications : []
    });
  }
}

@Component({
  selector: 'app-notification-panel-modal',
  template: `
    <mat-dialog-content *ngIf="notifications">
        <app-notification-panel [notifications]="this.notifications" (clickEmitter)="onNoClick()"></app-notification-panel>
    </mat-dialog-content>
    <mat-dialog-content *ngIf="notifications.length === 0">No notifications found.</mat-dialog-content>
  `
})
export class NotificationPanelModalComponent {

  constructor(private dialogRef: MatDialogRef<NotificationButtonComponent>,
              @Inject(MAT_DIALOG_DATA) public notifications: Notification[]) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
