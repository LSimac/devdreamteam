import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Notification} from 'src/app/models/notification.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notification-panel',
  templateUrl: './notification-panel.component.html',
  styleUrls: ['./notification-panel.component.css']
})
export class NotificationPanelComponent {

  @Input() notifications: Notification[];
  @Output() clickEmitter: EventEmitter<void> = new EventEmitter();

  constructor(private router: Router) {}

  onClick(n: Notification) {
    this.router.navigate(['/question/' + n.referenceEntityId]);
    this.clickEmitter.emit();
  }
}
