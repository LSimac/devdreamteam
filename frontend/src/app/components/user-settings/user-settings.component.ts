import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { User } from 'src/app/models/user/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { Category } from 'src/app/models/category.model';
import { Subject } from 'rxjs';
import { GeoMapComponent } from '../geo/geo-map/geo-map.component';

interface UserForm {
  id?: number;
  username?: string;
  role?: string;
  categories?: boolean[];
  description?: string;
  undefinedLocation: boolean;
  longitude: number;
  latitude: number;
}

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit, AfterViewInit {
  userFormGroup: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    role: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    categories: new FormArray([]),
    // undefinedLocation: new FormControl(false),
    longitude: new FormControl(0, Validators.required),
    latitude: new FormControl(0, Validators.required),
  });
  user: User;
  categories: Category[];
  submitUserSubject: Subject<void> = new Subject<void>();
  imageId: number;
  imgSelected = false;

  @ViewChild(GeoMapComponent, { static: false }) geoMap: GeoMapComponent;

  constructor(
    private router: Router,
    private auth: AuthService,
    private usersService: UsersService,
    private route: ActivatedRoute
  ) {
    auth.currentUserSubject.subscribe(
      user => this.user = user
    );
  }

  get isUserAdmin(): boolean {
    return this.user.role === 'admin';
  }

  get profCategories(): FormArray {
    return this.userFormGroup.get('categories') as FormArray;
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: { categories: Category[] }) => {
        this.categories = data.categories;
        this.initForm();
      });
  }

  ngAfterViewInit(): void {
    this.geoMap.locationEmitter.subscribe(location => {
      this.userFormGroup.get('longitude').setValue(location.longitude);
      this.userFormGroup.get('latitude').setValue(location.latitude);
    });
  }

  get isCurrentUserProf() {
    if (this.user) {
      return this.user.role === 'professional';
    }
    return false;
  }

  isProffesionalSelected() {
    return this.userFormGroup.controls.role.value === 'professional';
  }

  onSubmitForm() {
    const userForm: UserForm = this.userFormGroup.value;
    const user: User = this.mapUser(userForm);

    this.usersService.updateCurrentUser(user)
      .subscribe(
        succesUser => {
          this.auth.currentUserSubject.next(succesUser);
          localStorage.setItem('currentUser', JSON.stringify(succesUser));
          alert(succesUser.username + ', your settings are successfully updated! Enjoy!');
          this.router.navigate(['/']);
        },
        error => {
          console.log('Error occurred ' + error);
        }
      );
  }

  onCancel() {
    this.router.navigate(['/']);
  }

  get valid() {
    const controls = this.userFormGroup.controls;
    const gloabal = controls.username.valid && controls.role.valid;
    if (controls.role.value === 'normal') {
      return gloabal;
    } else {
      const prof = controls.description.valid;
      return gloabal && prof;
    }
  }

  private mapUser(userForm: UserForm): User {

    const u: User = {
      id: this.user.id,
      username: userForm.username,
      avatarId: this.imageId,
      role: userForm.role,
    };

    u.location = {
      longitude: userForm.longitude,
      latitude: userForm.latitude
    };

    // if (!userForm.undefinedLocation) {
    //   u.location = {
    //     longitude: userForm.longitude,
    //     latitude: userForm.latitude
    //   };
    // }

    if (u.role === 'professional') {
      u.categories = this.mapCategories(userForm.categories);
      u.description = userForm.description;
    }
    return u;
  }

  private mapCategories(catFlags: boolean[]) {
    if (!catFlags) {
      return [];
    }

    const res: Category[] = [];
    this.categories.forEach((c, index) => {
      if (catFlags[index]) {
        res.push(c);
      }
    });
    return res;
  }

  private initForm() {
    const username = this.user.username;
    const role = this.user.role;
    const description = this.user.description;
    const categoriesArr = new FormArray([]);
    let longitude: number;
    let latitude: number;

    // const undefinedLocation = !this.user.location;

    if (this.user.location) {
      longitude = this.user.location.longitude;
      latitude = this.user.location.latitude;
    }  else {
      longitude = 15.966568;
      latitude = 45.815399;
    }

    if (this.categories) {
      for (const category of this.categories) {
        const c = new FormControl(this.userContainsCategory(category));
        categoriesArr.push(c);
      }
    }

    this.userFormGroup = new FormGroup({
      username: new FormControl(username, Validators.required),
      role: new FormControl(role, Validators.required),
      description: new FormControl(description, Validators.required),
      categories: categoriesArr,
      // undefinedLocation: new FormControl(undefinedLocation),
      longitude: new FormControl(longitude),
      latitude: new FormControl(latitude)
    });

  }

  private userContainsCategory(category: Category): boolean {
    if (!this.user.categories) {
      return false;
    }

    let hasCatergory = false;

    this.user.categories.forEach((item, index) => {
      if (item.id === category.id) {
        hasCatergory = true;
      }
    });

    return hasCatergory;
  }

  onSubmit() {
    this.submitUserSubject.next();
  }

  imageSet(imageIds: number[]) {
    this.imageId = imageIds[0];
    this.onSubmitForm();
  }

  imageSelected() {
    this.imgSelected = true;
  }
}
