import { AdminService } from './../../../services/admin/admin.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user/user.model';
import { Ban } from 'src/app/models/user/ban.model';
import { MatTable, MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DateTimePickerDialogComponent } from '../date-time-picker-dialog/date-time-picker-dialog.component';
import { environment } from 'src/environments/environment';

export interface UserBaned {
  user: User;
  banned: boolean;
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['Avatar', 'Username', 'Role', 'Banned','BannedDate'];
  dataSource: MatTableDataSource<User>;
  expandedElement: User | null;
  banned: Map<number, boolean> = new Map();
  bannedDate: Map<number,string> = new Map();

  imgUrlDef = `${environment.apiUrl}/image`;

  @ViewChild(MatTable, { static: true, read: true }) table: MatTable<User>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private adminService: AdminService, public dialog: MatDialog) { }

  ngOnInit() {
    this.updateTable();
  }

  updateTable() {
    this.adminService.getAllUsers().then(users => this.setUpDataSource(users));
  }

  setUpDataSource(users) {
    this.dataSource = new MatTableDataSource<User>(users.payload);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.adminService.getAllBannedUser().then(bannedUsers => this.setUpBannedUser(bannedUsers));
  }

  setUpBannedUser(users) {
    let bannedUsers: Ban[] = users.payload;

    this.banned.clear();
    bannedUsers.forEach(u => this.banned.set(u.user.id, true));
    bannedUsers.forEach(u => this.bannedDate.set(u.user.id, u.date == null ? 'Temporary' : u.date));
  }

  unbanUser(userId: number) {
    this.adminService.unbanUser(userId).then(user => this.updateTable());
  }

  banUser(userId: number) {
    this.adminService.banUser(userId).then(user => this.updateTable());
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getUserAvatar(avatarId?: number): string{
      if(avatarId == null){
        return 'https://simpleicon.com/wp-content/uploads/user1.png';
      }else{
        return this.imgUrlDef + '/' + avatarId;
      }
  }

  openDialog() {

    const dialogRef = this.dialog.open(DateTimePickerDialogComponent, {
      width: 'auto',
      height: 'auto',
      maxWidth: '90%',
      maxHeight: '90%',
      data: { date: '', time: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == null || result.date == null || result.time == null) {
        return;
      }

      this.adminService.banUser(this.expandedElement.id, result.date + '' + result.time)
      .then(user => this.updateTable()).catch(user => alert('Invalid ban date'));
    });

  }

}
