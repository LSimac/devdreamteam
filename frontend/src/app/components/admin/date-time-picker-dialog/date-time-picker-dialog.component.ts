import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';

export interface DialogDate {
  date:string;
  time:string;
}

@Component({
  selector: 'app-date-time-picker-dialog',
  templateUrl: './date-time-picker-dialog.component.html',
  styleUrls: ['./date-time-picker-dialog.component.css']
})
export class DateTimePickerDialogComponent implements OnInit {
  private exportTime = { hour: 7, minute: 15, meriden: 'PM', format: 24 };


  constructor(
    public dialogRef: MatDialogRef<DateTimePickerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogDate
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancleDialog() {
    this.dialogRef.close();
  }

  addEvent(event: MatDatepickerInputEvent<Date>) {
    let dateArr:string[] = event.value.toLocaleDateString().split("/");
    let dayTemp = parseInt(dateArr[1]);
    let day:string = dateArr[1];
    let manTemp = parseInt(dateArr[0]);
    let man:string = dateArr[0];

    if(dayTemp < 10){
      day = "0"+day;
    }

    if(manTemp < 10){
      man = "0"+man;
    }

    this.data.date = dateArr[2]+"-"+man+"-"+day+"T";
  }

  onChangeHour(event){

    this.data.time = event.hour+":"+event.minute+":00"+"Z";
  }


  ngOnInit() {
  }

}
