import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Question } from 'src/app/models/question/question.model';
import { AdminService } from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class QuestionListComponent implements OnInit {
  questionDefs: Map<string, string>;

  displayedColumns: string[] = ['Creator', 'Title', 'Status', 'Date'];
  questions: Question[];

  dataSource: MatTableDataSource<Question>;
  expandedElement: Question | null;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private adminService:AdminService) { }

  ngOnInit() {
    this.updateTable();
  }

  updateTable(){
    this.adminService.getAllQuestions().then(questions => {
      this.setUpDataSource(questions);
    });
  }

  setUpDataSource(questions){
    this.dataSource = new MatTableDataSource<Question>(questions.payload);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getQuestionDefinition(columnDef: string, question:Question): string {
    if(columnDef === 'Creator'){
      return question.message.author.username;
    }

    if(columnDef === 'Title'){
      return question.title;
    }

    if(columnDef === 'Status'){
      return question.status;
    }

    if(columnDef === 'Date'){
      let parts:string[] = question.message.creationTimestamp.split("T");

      let date:string = parts[0];
      let time:string = parts[1].substr(0,parts[1].length-1);
      return date+"  "+time;
    }
  }

  deletUser(questionId:number){
    this.adminService.deleteQuestion(questionId).then(question => this.updateTable());
  }

}
