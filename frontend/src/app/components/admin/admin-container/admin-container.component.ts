import { AdminService } from './../../../services/admin/admin.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { UserListComponent } from '../user-list/user-list.component';
import { QuestionListComponent } from '../question-list/question-list.component';

@Component({
  selector: 'app-admin-container',
  templateUrl: './admin-container.component.html',
  styleUrls: ['./admin-container.component.css']
})
export class AdminContainerComponent implements OnInit {

  @ViewChild(UserListComponent, { static: true, read: false }) userListComponent: UserListComponent;
  @ViewChild(QuestionListComponent, { static: true, read: false }) questionListComponent: QuestionListComponent;

  constructor() { }

  ngOnInit() {
  }

  onTabChanged(event: MatTabChangeEvent) {
    if (event.index === 0) {
      this.userListComponent.updateTable();
    } else {
      this.questionListComponent.updateTable();
    }
  }
}
