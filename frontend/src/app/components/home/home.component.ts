import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { QuestionDialogComponent } from '../questions/question-dialog/question-dialog.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(QuestionDialogComponent, {
      width: '80%',
      height: '80%',
      data:{editMode:false}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  ngOnInit() {
  }

}
