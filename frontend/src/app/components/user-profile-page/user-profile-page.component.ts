import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user/user.model';
import {ActivatedRoute} from '@angular/router';
import {UsersService} from '../../services/users.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-user-profile-page',
  templateUrl: './user-profile-page.component.html',
  styleUrls: ['./user-profile-page.component.css']
})
export class UserProfilePageComponent implements OnInit {

  private user: User;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private userService: UsersService) {
  }

  ngOnInit(): void {
    const idParameter = this.route.snapshot.paramMap.get('id');

    if (idParameter === 'myProfile') {
      this.user = this.authService.currentUserValue;
    } else {
      const id: number = parseInt(idParameter, 10);
      this.userService.getUserById(id).then(usr => {
        this.user = usr;
        console.log('user ' + usr);
      }, () => this.user = null);
    }
  }
}
