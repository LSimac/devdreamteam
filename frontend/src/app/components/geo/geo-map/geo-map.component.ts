import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Location } from 'src/app/models/location.model';

@Component({
  selector: 'app-geo-map',
  templateUrl: './geo-map.component.html',
  styleUrls: ['./geo-map.component.css']
})
export class GeoMapComponent implements OnInit {
  @Input() zoom = 13;
  @Input() latitude = 51.678418;
  @Input() longitude = 7.809007;
  @Input() draggable = true;

  @Output() locationEmitter: EventEmitter<Location> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  locationChanged(event) {
    if (!this.draggable) {
      return;
    }

    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;

    this.locationEmitter.emit({
      longitude: this.longitude,
      latitude: this.latitude
    });
  }
}
