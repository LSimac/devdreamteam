import { MatDialog } from '@angular/material';
import { environment } from './../../../environments/environment';
import { PhotoService } from '../../services/images/photo.service';
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { ImageDialogComponent } from '../image/image-dialog/image-dialog.component';

@Component({
  selector: 'app-thumbnails',
  templateUrl: './thumbnails.component.html',
  styleUrls: ['./thumbnails.component.css']
})

export class ThumbnailsComponent implements OnInit, OnDestroy {
  @Input() events: Observable<string>;
  @Input() states: Observable<boolean>;
  @Input() imageIds: number[];
  @Output() removeIndex: EventEmitter<number> = new EventEmitter<number>();

  imgUrl = `${environment.apiUrl}/image`;

  private eventsSubscription: Subscription;
  private stateSubscription: Subscription;

  isRemoveState: boolean;

  urls: string[] = [];

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    if (this.events) {
      this.eventsSubscription = this.events.subscribe((url) => this.addFile(url));
    }

    if (this.states) {
      this.stateSubscription = this.states.subscribe((isRemoveState) => this.isRemoveState = isRemoveState);
    }
    this.isRemoveState = false;
  }

  ngOnDestroy() {
    if (this.eventsSubscription) {
      this.eventsSubscription.unsubscribe();
    }

    if (this.stateSubscription) {
      this.stateSubscription.unsubscribe();
    }
  }



  onClick(url: string) {
    if (this.isRemoveState) {
      this.remove(url);
    } else {
      this.showLargeImage(url);
    }
  }

  private showLargeImage(src: string) {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      width: 'auto',
      height: 'auto',
      maxWidth: '90%',
      maxHeight: '90%',
      data: { imgSrc: src }
    });

  }

  private remove(url): void {
    const index = this.urls.indexOf(url, 0);
    if (index > -1) {
      this.urls.splice(index, 1);
      this.removeIndex.emit(index);
    }
  }

  private addFile(url: string): void {
    this.urls.push(url);
  }
}
