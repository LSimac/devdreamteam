import { PhotoService } from '../../services/images/photo.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  @Input() submitAnswer: Observable<void>;
  @Input() singleImage = false;
  @Output() imageIds: EventEmitter<number[]> = new EventEmitter<number[]>();
  @Output() imageAdd: EventEmitter<void> = new EventEmitter<void>();

  photoFileSubject: Subject<string> = new Subject<string>();
  stateSubject: Subject<boolean> = new Subject<boolean>();

  private submitAnswerSubscription: Subscription;

  removeState = false;

  constructor(private photoService: PhotoService) { }

  imageFiles: File[] = [];

  isEmpty(): boolean {
    return this.imageFiles.length === 0;
  }

  ngOnInit() {
    this.submitAnswerSubscription = this.submitAnswer.subscribe(() => this.postPhotos());
  }

  private async postPhotos() {
    const imageIds: number[] = [];
    if (!this.isEmpty()) {

      for (const file of this.imageFiles) {
        const id = await this.photoService.postPhoto(file) as number;
        imageIds.push(id);
      }

      this.imageIds.emit(imageIds);
    } else {
      this.imageIds.emit([]);
    }
  }

  addImage(event): void {
    if (event.target.files && event.target.files[0]) {
      this.addThumbnail(event.target.files[0]);
      this.imageAdd.emit();
    }

  }

  addThumbnail(file: File) {
    if (!this.imageFiles.includes(file)) {
      this.imageFiles.push(file);
      this.addUrl(file);
    }
  }

  removeFile(index: number) {
    this.imageFiles.splice(index, 1);
  }

  addUrl(file: File) {
    const reader = new FileReader();
    reader.readAsDataURL(file); // read file as data url

    reader.onload = (data: Event) => { // called once readAsDataURL is completed
      this.photoFileSubject.next(reader.result.toString());
    };
  }

  toggleRemoveState() {
    this.removeState = !this.removeState;
    this.stateSubject.next(this.removeState);
  }
}
