import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionCardListComponent } from '../question-card-list/question-card-list.component';
import { QuestionProfCardListComponent } from '../question-prof-card-list/question-prof-card-list.component';
import { MatTabChangeEvent } from '@angular/material';
import { ProfessionalQuestionsListComponent } from '../professional-questions-list/professional-questions-list.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/models/user/user.model';

@Component({
  selector: 'app-questions-tab',
  templateUrl: './questions-tab.component.html',
  styleUrls: ['./questions-tab.component.css']
})
export class QuestionsTabComponent implements OnInit {

  @ViewChild(QuestionCardListComponent, {static: true, read: false}) questionListComponent: QuestionCardListComponent;
  @ViewChild(QuestionProfCardListComponent, {static: true, read: false}) questionProfListComponentestio: QuestionProfCardListComponent;
  @ViewChild(ProfessionalQuestionsListComponent, {static: false, read: false}) professionalQuestionList: ProfessionalQuestionsListComponent;


  currentUser: User;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.currentUser.subscribe(user => this.currentUser = user);
  }

  get isUserProfessional(): boolean {
    return this.currentUser.role === 'professional';
  }

  onTabChanged(event: MatTabChangeEvent) {
    if (event.index === 0) {
      this.questionListComponent.updateQuestions();
      return;
    }

    if (event.index === 1) {
      this.questionProfListComponentestio.updateQuestions();
      return;
    }

    if (event.index === 2) {
      this.professionalQuestionList.updateQuestions();
    }
  }

}
