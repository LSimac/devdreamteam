import { AuthService } from 'src/app/services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Question } from '../../../models/question/question.model';
import { QuestionService } from 'src/app/services/question/question.service';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category.model';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-question-card-list',
  templateUrl: './question-card-list.component.html',
  styleUrls: ['./question-card-list.component.css']
})
export class QuestionCardListComponent implements OnInit {

  questions: Question[];
  categories: Category[];

  filterCategories: Category[] = [];

  loading = false;

  constructor(private questionService: QuestionService, private categoriesService: CategoriesService, private authService: AuthService) { }

  async ngOnInit() {
    this.loading = true;

    await this.categoriesService.getCategories()
      .toPromise().then(data => this.categories = data as Category[]);
    this.questionService.questions.asObservable().subscribe(questions => this.refreshQuestions(questions));
    this.questionService.getQuestions().then(questions => this.questions = questions);
    this.loading = false;


    if (!this.authService.currentUserValue.location) {
      alert('Ne može se pristupiti lokaciji, može se postaviti u korisničkim postavkama');
    }

  }

  get filteredQuestions(): Question[] {
    if (this.filterCategories.length === 0) {
      return this.questions;
    }
    return this.questions.filter(q => {
      const questionCategories = q.categories;
      for (const questionCat of questionCategories) {
        const find = this.filterCategories.find(c => c.id === questionCat.id);
        if (find) {
          return true;
        }
      }
      return false;
    });
  }

  refreshQuestions(questions: Question[]): void {
    this.questions = questions;
  }

  updateQuestions() {
    this.questionService.getQuestions().then(questions => this.questions = questions);
  }

  onCheckbox(index: number, checked: boolean) {
    if (checked) {
      this.filterCategories.push(this.categories[index]);
    } else {
      const cat = this.categories[index];
      const i = this.filterCategories.findIndex(c => c.id === cat.id);
      if (i !== -1) {
        this.filterCategories.splice(i, 1);
      }
    }
  }
}
