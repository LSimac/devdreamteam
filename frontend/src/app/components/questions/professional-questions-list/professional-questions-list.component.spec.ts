import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalQuestionsListComponent } from './professional-questions-list.component';

describe('ProfessionalQuestionsListComponent', () => {
  let component: ProfessionalQuestionsListComponent;
  let fixture: ComponentFixture<ProfessionalQuestionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalQuestionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalQuestionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
