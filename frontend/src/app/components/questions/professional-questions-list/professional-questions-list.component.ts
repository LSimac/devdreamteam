import { Component, OnInit } from '@angular/core';
import { Question } from 'src/app/models/question/question.model';
import { QuestionService } from 'src/app/services/question/question.service';

@Component({
  selector: 'app-professional-questions-list',
  templateUrl: './professional-questions-list.component.html',
  styleUrls: ['./professional-questions-list.component.css']
})
export class ProfessionalQuestionsListComponent implements OnInit {

  questions: Question[];

  constructor(private questionService: QuestionService) { }

  ngOnInit() {
    this.questionService.professionalDirectQuestions.asObservable().subscribe(questions => this.refreshQuestions(questions));
    this.questionService.getProfessionalDirectQuestions().then(questions => this.questions = questions);
  }

  refreshQuestions(questions: Question[]): void {
    this.questions = questions;
  }

  updateQuestions() {
    this.questionService.getProfessionalDirectQuestions().then(questions => this.questions = questions);
  }

}
