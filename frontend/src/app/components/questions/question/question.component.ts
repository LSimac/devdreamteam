import { QuestionService } from 'src/app/services/question/question.service';
import { DataService } from './../../../services/data/data.service';
import { AnswerDialogComponent } from './../../answer/answer-dialog/answer-dialog.component';
import { MatDialog } from '@angular/material';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Question } from 'src/app/models/question/question.model';
import { User } from 'src/app/models/user/user.model';
import { AuthService } from 'src/app/services/auth/auth.service';
import { QuestionDialogComponent } from '../question-dialog/question-dialog.component';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit, OnDestroy {

  questionId: number;
  question: Question;
  showMap = true;
  currentUser: User;
  imgUrl = `${environment.apiUrl}/image`;
  avatarUrl: string;

  professionals: User[] = [];
  loadingProfessionals = true;

  questionUpdateInterval;

  categories = {
    Bars: 'local_cafe',
    Hotels: 'local_hotel',
    Restaurants: 'restaurant',
    Buildings: 'location_city',
    History: 'account_balance',
  };

  constructor(
    public dialog: MatDialog,
    private dataService: DataService,
    private questionService: QuestionService,
    private auth: AuthService,
    private route: ActivatedRoute,
    private userService: UsersService,
    private router: Router) {
      this.auth.currentUserSubject.subscribe(user => this.currentUser = user);
    }

  openDialog(): void {
    const dialogRef = this.dialog.open(AnswerDialogComponent, {
      width: '500px',
      height: '500px',
      data: { question: this.question }
    });
  }

  async ngOnInit() {
    this.questionId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    await this.initialize();
    this.questionUpdateInterval = setInterval(this.updateQuestion.bind(this), 5000);
    this.route.params.subscribe(params => {
      this.questionId = parseInt(params['id'], 10);
      this.initialize();
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.questionUpdateInterval);
  }

  async initialize() {
    await this.updateQuestion();
    if (this.isProfessional && !this.isCurrentUserProf && !this.question.idOfProfessionalWhoClaimed) {
      this.loadingProfessionals = true;
      this.professionals = await this.loadProfessionals();
      this.loadingProfessionals = false;
    }
  }

  get userImage() {
    const author = this.question.message.author;
    if (author && author.avatarId) {
      return this.imgUrl + '/' + author.avatarId;
    } else {
      return 'https://simpleicon.com/wp-content/uploads/user1.png';
    }
  }

  get isProfessional() {
    return this.question.type === 'professional';
  }

  get isCurrentUserProf() {
    return this.currentUser.role === 'professional';
  }

  mapQuestion(question) {
    this.question = question;
  }

  onMapButton() {
    this.showMap = !this.showMap;
  }

  isUserOwner() {
    return this.currentUser.id === this.question.message.author.id;
  }

  async loadProfessionals() {
    if (this.question.professionalIds === undefined) {
      return [];
    } else {
      return this.userService.loadUsers(this.question.professionalIds);
    }
  }

  async claim(flag: boolean) {
    this.questionService.claimQuestion(this.question.id, flag).toPromise();
    if (!flag) {
      this.router.navigate(['/']);
    } else {
      this.questionService.getQuestion(this.question.id)
        .then(question => question = this.mapQuestion(question));
    }
  }

  openDialogEdit() {

    const dialogRef = this.dialog.open(QuestionDialogComponent, {
      width: '800px',
      height: '800px',
      data: { question: this.question, editMode: true }
    });

    dialogRef.afterClosed()
      .subscribe(question =>
        this.questionService.getQuestion(this.question.id).then(q => this.mapQuestion(q))
      );
  }

  async updateQuestion() {
    return this.questionService.getQuestion(this.questionId).then(question => this.mapQuestion(question));
  }

  rateProfessional(grade: number) {
    if  (!this.question.idOfProfessionalWhoClaimed) {
      return;
    } else {
      this.userService.gradeProfessional(
        this.question.idOfProfessionalWhoClaimed,
        this.question.id,
        grade
      );
      alert('Grade ' + grade + '! Thank you for rating professional user!');
    }
  }
}
