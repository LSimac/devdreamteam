import { Component, OnInit, Inject } from '@angular/core';
import { Question } from 'src/app/models/question/question.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-question-dialog',
  templateUrl: './question-dialog.component.html',
  styleUrls: ['./question-dialog.component.css']
})
export class QuestionDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<QuestionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Question
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancleDialog() {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
