import { Component, OnInit } from '@angular/core';
import { QuestionService } from 'src/app/services/question/question.service';
import { Question } from 'src/app/models/question/question.model';

@Component({
  selector: 'app-question-prof-card-list',
  templateUrl: './question-prof-card-list.component.html',
  styleUrls: ['./question-prof-card-list.component.css']
})
export class QuestionProfCardListComponent implements OnInit {

  constructor(private questionService: QuestionService) { }

  questions: Question[];

  ngOnInit() {
    this.questionService.questionsProf.asObservable().subscribe(questions => this.refreshQuestions(questions));
    this.questionService.getProfessionalQuestions().then(questions => this.refreshQuestions(questions));
  }

  refreshQuestions(questions: Question[]): void {
    this.questions = questions;
  }

  updateQuestions(){
    this.questionService.getProfessionalQuestions().then(questions => this.refreshQuestions(questions));
  }

}
