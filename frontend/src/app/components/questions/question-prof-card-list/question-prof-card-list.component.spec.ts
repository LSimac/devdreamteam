import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionProfCardListComponent } from './question-prof-card-list.component';

describe('QuestionProfCardListComponent', () => {
  let component: QuestionProfCardListComponent;
  let fixture: ComponentFixture<QuestionProfCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionProfCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionProfCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
