import { DataService } from './../../../services/data/data.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Question } from '../../../models/question/question.model';
import { Router, Data } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.css']
})
export class QuestionCardComponent implements OnInit {
  @Input() question: Question;
  @Input() open = false;
  imgUrlDef = `${environment.apiUrl}/image`;
  avatarId: string;


  categories = {
    Bars: 'local_cafe',
    Hotels: 'local_hotel',
    Restaurants: 'restaurant',
    Buildings: 'location_city',
    History: 'account_balance',
  };
  imagesUrl: Subject<string> = new Subject<string>();

  ngOnInit(): void {
    if (this.question.message.author.avatarId == null) {

      this.avatarId = 'https://simpleicon.com/wp-content/uploads/user1.png';
    } else {
      this.avatarId = this.imgUrlDef + '/' + this.question.message.author.avatarId;

    }
  }

  constructor(private router: Router, private dataService: DataService) {

  }

  openDetails() {
    this.router.navigate(['/question/' + this.question.id]);
  }
}
