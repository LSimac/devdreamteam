import { Component, OnInit, Inject, Output, EventEmitter, ViewChild, AfterViewInit, Input } from '@angular/core';
import { Question } from 'src/app/models/question/question.model';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { User } from 'src/app/models/user/user.model';
import { QuestionService } from 'src/app/services/question/question.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Category } from 'src/app/models/category.model';
import { CategoriesService } from 'src/app/services/categories.service';
import { GeoMapComponent } from '../../geo/geo-map/geo-map.component';
import { UsersService } from 'src/app/services/users.service';
import { Subject } from 'rxjs';


interface QuestionForm {
  id?: number;
  title?: string;
  text?: string;
  categories: boolean[];
  longitude: number;
  latitude: number;
  isProfessional: boolean;
  validProfessionalUsers?: [];
}

@Component({
  selector: 'app-question-edit',
  templateUrl: './question-edit-component.html',
  styleUrls: ['./question-edit-component.css']
})
export class QuestionEditComponent implements OnInit, AfterViewInit {

  @Output() cancle: EventEmitter<void> = new EventEmitter();
  @Input() checked: boolean;
  @Input() question: Question;
  @Input() editMode = false;
  submitQuestionSubject: Subject<void> = new Subject<void>();


  questionFormGroup: FormGroup = new FormGroup({
    title: new FormControl('', Validators.required),
    text: new FormControl('', Validators.required),
    longitude: new FormControl(0.0, Validators.required),
    latitude: new FormControl(0.0, Validators.required),
    isProfessional: new FormControl(false),
    categories: new FormArray([]),
    validProfessionalUsers: new FormControl()
  });

  // question: Question;
  currentUser: User;
  valid = true;
  // editMode = false;
  categories: Category[];

  professionalUsers: User[];
  suitableProfessionalUsers: User[];
  selectedProfs: User[];
  imageIds: number[] = [];

  @ViewChild(GeoMapComponent, { static: false }) geoMap: GeoMapComponent;
  showMap = false;

  constructor(
    private questionService: QuestionService,
    private auth: AuthService,
    private categoriesService: CategoriesService,
    private userService: UsersService
  ) {
    this.currentUser = auth.currentUserValue;
  }

  async ngOnInit() {
    const catPromise = this.categoriesService.getCategories().toPromise();
    await catPromise.then(categories => this.categories = categories);
    await this.userService.getAllProfessionalUsers().then(users => this.mapProfessionalUsers(users));
    this.initForm();
  }

  ngAfterViewInit(): void {
    this.geoMap.locationEmitter.subscribe(location => {
      this.questionFormGroup.get('longitude').setValue(location.longitude);
      this.questionFormGroup.get('latitude').setValue(location.latitude);
    });
  }

  get categoriesControls(): FormArray {
    return this.questionFormGroup.get('categories') as FormArray;
  }

  onSubmit() {
    if (this.editMode) {
      this.onSubmitForm();
    } else {
      this.submitQuestionSubject.next();
    }
  }

  imageSet(imageIds: number[]) {
    this.imageIds = imageIds;
    this.onSubmitForm();
  }

  onSubmitForm() {
    const questionForm: QuestionForm = this.questionFormGroup.value;

    if (this.questionFormGroup.status === 'INVALID'
      || this.validateCategories(questionForm.categories) === false
      || (questionForm.isProfessional && questionForm.validProfessionalUsers == null)) {
      alert('Question form is not valid');
      return;
    }

    console.log(questionForm);
    const question: Question = this.mapQuestion(questionForm);
    console.log(question);

    if (this.editMode) {
      question.id = this.question.id;
      this.questionService.updateQuestion(question);
    } else {
      this.questionService.postQuestion(question);
    }
    this.onCancel();
  }

  onCancel() {
    this.cancle.emit();
  }

  onShowMap() {
    this.showMap = !this.showMap;
  }

  private mapQuestion(questionForm: QuestionForm): Question {
    const q: Question = {
      id: questionForm.id,
      title: questionForm.title,
      message: {
        text: questionForm.text,
        creationTimestamp: new Date().toISOString(),
        author: this.currentUser,
        imageIds: this.imageIds
      },
      location: {
        longitude: questionForm.longitude,
        latitude: questionForm.latitude
      },
      isAnswered: false,
      status: 'open',
      type: questionForm.isProfessional ? 'professional' : 'public',
      categories: this.mapCategories(questionForm.categories)
    };

    if (questionForm.isProfessional) {
      q.professionalIds = this.mapProfessionalUserUsernameToId(questionForm.validProfessionalUsers);
    }

    return q;
  }

  mapProfessionalUserUsernameToId(users: string[]): number[] {
    const userIDs: number[] = [];

    for (const s of users) {
      for (const prof of this.professionalUsers) {

        if (prof.username === s) {
          userIDs.push(prof.id);
        }
      }
    }

    return userIDs;
  }

  private initForm() {
    if (this.editMode) {
      this.initFormEdit();
    } else {
      this.initFormNewQuestion();
    }
  }

  private initFormEdit() {
    const categoriesArr = new FormArray([]);

    if (this.categories) {
      for (const category of this.categories) {
        categoriesArr.push(new FormControl(
          this.question.categories.find(c => c.id === category.id) !== undefined
        ));
      }
    }

    this.questionFormGroup = new FormGroup({
      title: new FormControl(this.question.title, Validators.required),
      text: new FormControl(this.question.message.text, Validators.required),
      longitude: new FormControl(this.question.location.longitude, Validators.required),
      latitude: new FormControl(this.question.location.latitude, Validators.required),
      categories: categoriesArr
    });
  }

  private initFormNewQuestion() {
    const categoriesArr = new FormArray([]);

    if (this.categories) {
      for (const category of this.categories) {
        categoriesArr.push(new FormControl(false));
      }
    }

    let longitude: number;
    let latitude: number;

    if (this.currentUser && this.currentUser.location) {
      longitude = this.currentUser.location.longitude;
      latitude = this.currentUser.location.latitude;
    } else {
      longitude = 15.966568;
      latitude = 45.815399;
    }

    this.questionFormGroup = new FormGroup({
      title: new FormControl('', Validators.required),
      text: new FormControl('', Validators.required),
      longitude: new FormControl(longitude, Validators.required),
      latitude: new FormControl(latitude, Validators.required),
      isProfessional: new FormControl(false),
      categories: categoriesArr
    });
  }

  private mapCategories(catFlags: boolean[]): Category[] {
    if (!catFlags) {
      return [];
    }

    const res: Category[] = [];
    this.categories.forEach((c, index) => {
      if (catFlags[index]) {
        res.push(c);
      }
    });
    return res;
  }

  changeQuestionType() {
  }

  mapProfessionalUsers(users) {
    this.professionalUsers = users.payload;
  }

  isQuestionProfessional() {
    return this.questionFormGroup.value.isProfessional;
  }

  getsuitableProfessionalUsers(): User[] {
    this.suitableProfessionalUsers = [];
    const categories: Category[] = this.mapCategories(this.questionFormGroup.value.categories);

    this.professionalUsers.forEach(user => {
      let valid = true;
      for (const c of categories) {
        if (user.categories.findIndex((category) => {
          return category.id === c.id;
        }) === -1) {
          valid = false;
          break;
        }
      }

      if (valid) {
        this.suitableProfessionalUsers.push(user);
      }
    });

    return this.suitableProfessionalUsers;
  }

  validateCategories(categories: boolean[]): boolean {
    for (const c of categories) {
      if (c) {
        return true;
      }
    }

    return false;
  }

}
