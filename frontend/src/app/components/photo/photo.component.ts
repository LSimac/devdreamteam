import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit, OnDestroy {

  @Input() events: Observable<string>;
  @Input() states: Observable<boolean>;
  @Input() imageIds: number[];
  @Output() removeIndex: EventEmitter<number> = new EventEmitter<number>();

  imgUrl = `${environment.apiUrl}/image`;

  private eventsSubscription: Subscription;
  private stateSubscription: Subscription;

  urls: string[]= [];

  ngOnInit() {
    if (this.events) {
      this.eventsSubscription = this.events.subscribe((url) => this.addFile(url));
    }

    if (this.states) {
      this.stateSubscription = this.states.subscribe((isRemoveState) => this.remove());
    }
  }

  ngOnDestroy() {
    if (this.eventsSubscription) {
      this.eventsSubscription.unsubscribe();
    }

    if (this.stateSubscription) {
      this.stateSubscription.unsubscribe();
    }
  }

  constructor() { }

  private remove(): void {
    this.urls = [];
    this.removeIndex.emit(0);
  }

  private addFile(url: string): void {
    this.urls = [];
    this.urls.push(url);
  }

}
