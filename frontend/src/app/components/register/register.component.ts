import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  minPw = 6;

  isLinear = true;
  userNameFormGroup: FormGroup;
  passwordFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router) { }
  ngOnInit() {
    this.userNameFormGroup = this.formBuilder.group({
      userNameCtrl: ['', Validators.required]
    });
    this.passwordFormGroup = this.formBuilder.group({
      passwordCtrl: ['', Validators.required],
      repeatedpasswordCtrl: ['', Validators.required]
    });
  }

  get userNameFormControl() {
    return this.userNameFormGroup.controls;
  }
  get passwordFormControl() {
    return this.passwordFormGroup.controls;
  }

  get valid() {
    const controls = this.passwordFormGroup.controls;
    return this.passwordFormGroup.valid &&
      (controls.passwordCtrl.value.length === controls.repeatedpasswordCtrl.value.length);
  }

  register() {
    this.auth.register(
      this.userNameFormControl.userNameCtrl.value,
      this.passwordFormControl.passwordCtrl.value,
      this.passwordFormControl.repeatedpasswordCtrl.value
      )
      .subscribe(
        sucess => {
          this.auth.loginUser(
            this.userNameFormControl.userNameCtrl.value,
            this.passwordFormControl.passwordCtrl.value,
            this.router
          );
        }, error => {
          alert('Error occurred ' + error.message);
        }
      );
  }
}
