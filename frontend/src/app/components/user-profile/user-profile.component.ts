import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user/user.model';
import {environment} from '../../../environments/environment';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  @Input() user: User;

  private userImageUrl;
  private userLocationString: string;

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit(): void {
    this.userImageUrl = this.user.avatarId ? `${environment.apiUrl}/image/${this.user.avatarId}`
      : 'https://simpleicon.com/wp-content/uploads/user1.png';
    this.userLocationString = this.user.location ? `[ ${this.user.location.longitude}, ${this.user.location.latitude} ]` : 'Unknown';
  }

  get isLogged() {
    if (this.authService.currentUserValue) {
      return this.authService.currentUserValue.id === this.user.id;
    }
    return false;
  }

  onEdit() {
    this.router.navigate(['/settings']);
  }
}
