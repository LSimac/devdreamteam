import { AdminContainerComponent } from './components/admin/admin-container/admin-container.component';
import { HomeComponent } from './components/home/home.component';
import { LoginRegistrationContainerComponent } from './components/login-registration-container/login-registration-container.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuard } from './services/auth/auth.guard';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { HomeGuard } from './services/auth/home.guard';
import { CategoriesResolverService } from './services/categories-resolver.service';
import { UserCardComponent } from './components/user-card/user-card.component';
import { QuestionComponent } from './components/questions/question/question.component';
import {UserProfilePageComponent} from './components/user-profile-page/user-profile-page.component';
import { AdminGuard } from './services/auth/admin.guard';

const routes: Routes = [
  {
    path: 'auth',
    component: LoginRegistrationContainerComponent,
    canActivate: [HomeGuard],
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'settings',
    component: UserSettingsComponent,
    resolve: {
      categories: CategoriesResolverService
    }
  },
  {
    path: 'question/:id', component: QuestionComponent //ZASADS OVAKO <- VAKO NECEMO U EVROPU
  },
  {
    path: 'user', component: UserCardComponent
  },
  {
    path: 'user/:id', component: UserProfilePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: AdminContainerComponent, canActivate: [AdminGuard]
  },
  {
    path: '**',
    redirectTo: ''
  } // otherwise redirect to home
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
