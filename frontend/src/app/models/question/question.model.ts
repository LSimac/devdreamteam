import { Message } from './message.model';
import { Location } from '../location.model';
import { Category } from '../category.model';

export interface Question {
    id: number;
    title: string;
    message: Message;
    location?: Location;
    status: string;
    isAnswered: boolean;
    type: string;
    categories?: Category[];

    professionalIds?: number[];
    idOfProfessionalWhoClaimed?: number;
}
