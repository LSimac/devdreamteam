import { User } from '../user/user.model';

export interface Message {
    text: string;
    imageIds?: number[];
    timestamp?: string;
    author: User;
    creationTimestamp?: string;
}
