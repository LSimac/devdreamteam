import { Message } from './message.model';

export interface Answer {
    id?: number;
    message: Message;
    questionId: number;
    isAccepted: boolean;
}
