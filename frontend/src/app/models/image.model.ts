export interface Image {
    id: number;
    realPath: string;
}
