export interface Notification {
  id: number;
  referenceEntityId: number;
  referenceEntityType: string;
  creationTimestamp: string;
  isSeen: boolean;
  description: string;
}
