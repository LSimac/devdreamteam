export interface AuthUser {
    username: string;
    password: string;
    repeatedPassword?: string;
    role: string;
}
