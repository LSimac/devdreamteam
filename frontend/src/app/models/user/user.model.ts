import { Category } from '../category.model';
import { Location } from '../location.model';

export interface User {
    id: number;
    username: string;
    role: string;
    avatarId?: number;

    location?: Location;

    categories?: Category[];
    description?: string;
    grade?: number;
}
