export interface UpdateUser {
    username: string;
    avatarId: number;

    oldPassword: number;
    password: string;
    repeatedPassword: string;

    categories?: number[];
    description?: string;
    grade?: number;
}
