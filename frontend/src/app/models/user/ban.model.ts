import { User } from './user.model';

export interface Ban{
    user: User;
    date?: string;
}