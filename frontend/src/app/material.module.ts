import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { MaterialTimePickerModule } from '@candidosales/material-time-picker';
import {MatBadgeModule} from '@angular/material/badge';
import {MatSliderModule} from '@angular/material/slider';

import {
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatStepperModule,
    MatListModule,
    MatExpansionModule,
    MatTabsModule,
    MatSidenavModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSlideToggleModule,
    MatAutocompleteModule, 
    MatPaginatorModule, 
    MatSortModule, 
    MatDialogModule,
    MatDatepickerModule, 
    MatNativeDateModule, 
    MatProgressSpinnerModule, 
    MatTableModule

} from '@angular/material';

const modules = [
    MatBadgeModule,
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatStepperModule,
    MatListModule,
    MatExpansionModule,
    ScrollingModule,
    MatTabsModule,
    MatSidenavModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatAutocompleteModule,
    FormsModule,
    NgSelectModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MaterialTimePickerModule,
    MatProgressSpinnerModule,
    MatSliderModule
];

@NgModule({

    imports: [
        modules
    ],
    exports: [
        modules
    ],
    declarations: [],
    providers: [MatDatepickerModule]
})

export class MaterialModule { }
