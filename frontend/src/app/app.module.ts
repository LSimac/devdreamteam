import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {MaterialModule} from './material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptorService} from './services/auth/auth-interceptor.service';

import {UserCardComponent} from './components/user-card/user-card.component';
import {UserSettingsComponent} from './components/user-settings/user-settings.component';
import {ThumbnailsComponent} from './components/thumbnails/thumbnails.component';
import {AnswerCardComponent} from './components/answer/answer-card/answer-card.component';
import {HomeComponent} from './components/home/home.component';
import {MainNavComponent} from './layout/main-nav/main-nav.component';
import {ToolbarComponent} from './layout/toolbar/toolbar.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginRegistrationContainerComponent} from './components/login-registration-container/login-registration-container.component';
import {QuestionCardComponent} from './components/questions/question-card/question-card.component';
import {QuestionCardListComponent} from './components/questions/question-card-list/question-card-list.component';
import {QuestionEditComponent} from './components/questions/question-edit/question-edit-component';
import {QuestionDialogComponent} from './components/questions/question-dialog/question-dialog.component';
import {QuestionComponent} from './components/questions/question/question.component';
import {AnswerCardListComponent} from './components/answer/answer-card-list/answer-card-list.component';
import {AnswerEditComponent} from './components/answer/answer-edit/answer-edit.component';
import {AnswerDialogComponent} from './components/answer/answer-dialog/answer-dialog.component';
import {ImageComponent} from './components/image/image.component';

import {AgmCoreModule} from '@agm/core';
import {GeoMapComponent} from './components/geo/geo-map/geo-map.component';
import {SecurePipe} from './services/secure.pipe';
import {ImageDialogComponent} from './components/image/image-dialog/image-dialog.component';
import {PhotoComponent} from './components/photo/photo.component';
import {QuestionProfCardListComponent} from './components/questions/question-prof-card-list/question-prof-card-list.component';
import {QuestionsTabComponent} from './components/questions/questions-tab/questions-tab.component';
import {NotificationComponent} from './components/notification/notification.component';
import {NotificationPanelComponent} from './components/notification-panel/notification-panel.component';
import {NotificationButtonComponent, NotificationPanelModalComponent} from './components/notification-button/notification-button.component';
import {UserProfilePageComponent} from './components/user-profile-page/user-profile-page.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';

import {UserListComponent} from './components/admin/user-list/user-list.component';
import {UserComponent} from './components/admin/user/user.component';
import {DateTimePickerDialogComponent} from './components/admin/date-time-picker-dialog/date-time-picker-dialog.component';
import {AdminContainerComponent} from './components/admin/admin-container/admin-container.component';
import {QuestionListComponent} from './components/admin/question-list/question-list.component';
import {ProfessionalQuestionsListComponent} from './components/questions/professional-questions-list/professional-questions-list.component';

const components = [
  AppComponent,
  UserCardComponent,
  UserSettingsComponent,
  AnswerCardComponent,
  HomeComponent,
  MainNavComponent,
  ToolbarComponent,
  LoginComponent,
  RegisterComponent,
  LoginRegistrationContainerComponent,
  ThumbnailsComponent,
  QuestionCardComponent,
  QuestionCardListComponent,
  QuestionEditComponent,
  QuestionDialogComponent,
  AnswerEditComponent,
  AnswerDialogComponent,
  AnswerCardListComponent,
  QuestionComponent,
  ImageComponent,
  AnswerCardListComponent,
  GeoMapComponent,
  PhotoComponent,
  QuestionProfCardListComponent,
  QuestionsTabComponent,
  ToolbarComponent,
  HomeComponent,
  MainNavComponent,
  UserCardComponent,
  UserSettingsComponent,
  NotificationComponent,
  NotificationPanelComponent,
  NotificationButtonComponent,
  NotificationPanelModalComponent,
  ImageDialogComponent,
  UserProfileComponent,
  UserProfilePageComponent,
  AppComponent,
  LoginComponent,
  RegisterComponent,
  LoginRegistrationContainerComponent,
  ToolbarComponent,
  HomeComponent,
  MainNavComponent,
  UserCardComponent,
  UserSettingsComponent,
  ProfessionalQuestionsListComponent,
  AdminContainerComponent,
  DateTimePickerDialogComponent,
  UserListComponent,
  UserComponent,
  QuestionListComponent
];

@NgModule({
  declarations: [
    components,
    SecurePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyArWR6aVPcv_opr9CRQt-FWDZSI33y1_V4'
    })
  ],
  entryComponents: [
    QuestionDialogComponent,
    AnswerDialogComponent,
    ImageDialogComponent,
    NotificationPanelModalComponent,
    DateTimePickerDialogComponent,
    ProfessionalQuestionsListComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
