import { Injectable } from '@angular/core';
import { Question } from 'src/app/models/question/question.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public question: Question;
}
