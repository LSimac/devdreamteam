import { Injectable } from '@angular/core';
import { Location } from '../models/location.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private geolocation = navigator.geolocation;
  private watchId: number;
  currentLocation = new Subject<Location>();

  constructor() { }

  startTrackingLocation() {
    if (!this.geolocation) {
      return undefined;
    }

    if (!this.watchId) {
      this.watchId = this.geolocation.watchPosition((pos) => {
        this.currentLocation.next(pos.coords as Location);
      });
    }
  }

  stopTrackingLocation() {
    if (this.watchId) {
      this.geolocation.clearWatch(this.watchId);
      this.watchId = undefined;
    }
  }
}
