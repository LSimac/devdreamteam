import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Notification} from '../models/notification.model';
import {Payload} from '../models/payload.model';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) {
  }

  public getNotifications(): Promise<Notification[]> {
    return this.http.get<Payload<Notification[]>>(`${environment.apiUrl}/user/notifications`)
      .toPromise().then(data => data.payload);
  }
}
