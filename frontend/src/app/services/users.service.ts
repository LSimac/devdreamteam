import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../models/user/user.model';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import { Question } from '../models/question/question.model';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersUrl = '/user';

  constructor(private http: HttpClient) {
  }

  getUserById(id: number): Promise<User> {
    return this.http.get<User>(`${environment.apiUrl}/user/${id}`).toPromise();
  }

  getAllProfessionalUsers(): Promise<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}` + this.usersUrl + '/professionals').toPromise();
  }

  getCurrentUser(): Observable<User> {
    return this.http.get<User>(`${environment.apiUrl}` + this.usersUrl);
  }

  updateCurrentUser(user: User): Observable<User> {
    return this.http.patch<User>(
      `${environment.apiUrl}` + this.usersUrl, user
    );
  }

  async loadUsers(ids: number[]): Promise<User[]> {
    const result: User[] = [];

    for (const id of ids) {
      let u: User;
      await this.getUserById(id).then(user => u = user);
      result.push(u);
    }
    return new Promise(
      (resolve, reject) => {
        resolve(result);
      }
    );
  }

  gradeProfessional(professionalId: number, questionId: number, grade: number) {
    return this.http.post<void>(
      `${environment.apiUrl}` + this.usersUrl + '/' + professionalId
      + '/grade?questionId=' + questionId + '&grade=' + grade, {}
    ).toPromise().then();
  }
}
