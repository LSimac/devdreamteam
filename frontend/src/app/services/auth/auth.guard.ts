import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, exhaustMap } from 'rxjs/operators';



@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot
  ): boolean | Promise<boolean> | Observable<boolean | UrlTree> {
    return this.authService.currentUserSubject
      .pipe(
        take(1),
        map(user => {
          if (user) {
            if (user.role === 'admin') {
              return this.router.createUrlTree(['/admin']);
            } else {
              return true;
            }
          }
          return this.router.createUrlTree(['/auth']);
        }));
  }
}
