import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from './auth.service';
import {catchError, exhaustMap, take} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {
  }

  prefix = 'Bearer ';

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.authService.currentUser.pipe(
      take(1),
      exhaustMap(user => {
        const token = this.authService.getToken();

        if (!token) {
          return next.handle(req);
        }

        const modifiedReq = req.clone({
          headers: new HttpHeaders()
            .set('authorization', this.prefix + token)
        });

        return next.handle(modifiedReq).pipe(
          catchError(err => {
            if (err instanceof HttpErrorResponse && err.status === 401) {
              this.authService.logout();
              this.router.navigate(['/auth']);
              return next.handle(req);
            } else {
              return throwError(err);
            }
          })
        );
      })
    );
  }
}
