import { Message } from './../../models/question/message.model';
import { AdminService } from './../admin/admin.service';
import { CategoriesService } from './../categories.service';
import { Observable, BehaviorSubject, Subscribable, forkJoin, interval, Subscription } from 'rxjs';
import { map, finalize, tap, take } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user/user.model';
import { environment } from 'src/environments/environment';
import { UsersService } from '../users.service';
import { LoginToken } from 'src/app/models/login-token';
import { UserCardComponent } from 'src/app/components/user-card/user-card.component';
import { Router } from '@angular/router';
import { Location } from 'src/app/models/location.model';
import { LocationService } from 'src/app/services/location.service';
import { findStaticQueryIds } from '@angular/compiler';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    LOCATION_UPDATE_INTERVAL = 1000;

    loginUrl = '/login';
    registerUrl = '/register';
    locationUpdateUrl = '/user/location';

    public currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        private usersService: UsersService,
        private locationService: LocationService
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(null);
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string): Observable<any> {
        const data = { username, password };
        return this.http.post<LoginToken>(`${environment.apiUrl}${this.loginUrl}`, data, httpOptions)
            .pipe(
                map(resp => {
                    localStorage.setItem('userToken', JSON.stringify(resp.payload));
                    return resp;
                })
            );
    }

    updateLocation(location: Location): Observable<User> {
        const data = {
            longitude: location.longitude,
            latitude: location.latitude
        };
        return this.http.post<User>(`${environment.apiUrl}${this.locationUpdateUrl}`, data, httpOptions);
    }

    loginUser(username: string, password: string, router: Router) {
        this.login(
            username,
            password
        ).subscribe(
            (resp) => this.storeCurrentUser()
                .subscribe(
                    user => {
                        if (user.role === 'admin') {
                            router.navigate(['/admin']);
                        } else {
                            router.navigate(['/']);
                        }
                    }
                ),
            (error: HttpErrorResponse) => {
                alert(error.error.message);
            }

        );
    }


    autoLogin() {
        const token = this.getToken();
        if (!token) {
            return;
        } else {
            const loadedUser: User = JSON.parse(localStorage.getItem('currentUser'));
            this.currentUserSubject.next(loadedUser);
            this.startLocationTracking();
        }
    }

    register(username: string, password: string, repeatedPassword: string): Observable<any> {
        const data = { username, password, repeatedPassword };
        return this.http.post<any>(`${environment.apiUrl}${this.registerUrl}`, data, httpOptions);
    }

    logout() {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userToken');
        this.locationService.stopTrackingLocation();
        this.currentUserSubject.next(null);
    }

    getToken() {
        const token = localStorage.getItem('userToken');
        if (token && token.length > 2) {
            return token.slice(1, token.length - 1);
        } else {
            return null;
        }
    }

    storeCurrentUser(): Observable<User> {
        return this.usersService.getCurrentUser()
            .pipe(
                tap(u => {
                    console.log('postavljanje trenutnog usera prije mapiranja u novom http req ->' + u.username);
                    localStorage.setItem('currentUser', JSON.stringify(u));
                    this.currentUserSubject.next(u);
                    this.startLocationTracking();
                }
                )
            );
    }

    startLocationTracking() {
        this.locationService.currentLocation.subscribe(
            (currentLocation) => {
                if (this.currentUserSubject.value) {
                    this.updateLocation(currentLocation as Location).subscribe(
                        (user) => {
                            this.currentUserSubject.next(user);
                        },
                        (error) => {
                            console.log(error);
                        }
                    );
                }
            }
        );
        this.locationService.startTrackingLocation();
    }
}
