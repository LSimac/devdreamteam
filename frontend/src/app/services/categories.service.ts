import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Category } from '../models/category.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap, map } from 'rxjs/operators';
import { Payload } from '../models/payload.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  url = '/categories';

  constructor(private http: HttpClient) {
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Payload<Category[]>>(`${environment.apiUrl}` + this.url)
    .pipe(
      map(
        (c: Payload<Category[]>) => {
          return c.payload;
        }
      )
    );
  }

  getCategory(id: number): Observable<Category> {
    return this.http.get<Category>(`${environment.apiUrl}` + this.url + '/' + id);
  }
}
