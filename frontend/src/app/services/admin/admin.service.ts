import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user/user.model';
import { Question } from 'src/app/models/question/question.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  public getAllUsers(): Promise<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/user/all`).toPromise();
  }

  public getAllQuestions(): Promise<Question[]> {
    return this.http.get<Question[]>(`${environment.apiUrl}/questions/all`).toPromise();
  }

  public banUser(userId: number, expiresOn?: string): Promise<void> {
    const dateValue: string = expiresOn == null ? '' : `?expiresOn=${expiresOn}`;
    return this.http.post<void>(`${environment.apiUrl}/user/${userId}/ban` + dateValue, []).toPromise();
  }

  public unbanUser(userId: number): Promise<void> {
    return this.http.post<void>(`${environment.apiUrl}/user/${userId}/unban`, []).toPromise();
  }

  public getAllBannedUser(): Promise<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/user/banned`).toPromise();
  }

  public deleteQuestion(questionId: number): Promise<void> {
    return this.http.delete<void>(`${environment.apiUrl}/questions/${questionId}`).toPromise();
  }

}
