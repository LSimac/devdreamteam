import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Image } from '../../models/image.model';
import { Injectable } from '@angular/core';
import { Payload } from 'src/app/models/payload.model';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private http: HttpClient) { }

  public postPhoto(imgFile: File): Promise<number> {
    return this.http.post<Payload<number>>(`${environment.apiUrl}/image`, this.fileToFormData(imgFile)).toPromise()
    .then(payload => payload.payload);
  }

  public getPhoto(id: number): Promise<File> {
    return this.http.get<File>(`${environment.apiUrl}/image/${id}`).toPromise();
  }

  private fileToFormData(file: File): FormData {
    const formData = new FormData();
    formData.append('image', file);
    return formData;
  }
}
