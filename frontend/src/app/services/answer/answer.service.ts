import { Subject } from 'rxjs';
import { Answer } from './../../models/question/answer.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Payload } from '../../models/payload.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  answers: Subject<Answer[]> = new Subject<Answer[]>();

  constructor(private http: HttpClient) {
  }

  public deleteAnswer(answerId: number): Promise<void> {
    return this.http.delete<void>(`${environment.apiUrl}/answers/${answerId}`).toPromise();
  }

  public getAnswersForQuestion(questionId: number): Promise<Answer[]> {
    return this.http.get<Payload<Answer[]>>(`${environment.apiUrl}/questions/${questionId}/answers`)
      .toPromise().then(payload => payload === null ? [] : payload.payload);
  }

  public getAnswer(id: number): Promise<Answer> {
    return this.http.get<Answer>(`${environment.apiUrl}/answers/${id}`).toPromise();
  }

  public postAnswer(answer: Answer): Promise<void | Answer> {
    return this.http.post<Answer>(`${environment.apiUrl}/questions/${answer.questionId}/answers`, answer).toPromise()
      .then(value => this.updateAnswers(value.questionId)).then(value => value);
  }

  public patchAnswer(answerId: number, answerPatch: Answer): Promise<Answer> {
    return this.http.patch<Answer>(`${environment.apiUrl}/answers/${answerId}`, answerPatch).toPromise();
  }

  public acceptAnswer(answerId: number): Promise<void | Answer> {
    const shouldAccept = true;
    const body = new HttpParams().set('shouldAccept', shouldAccept.toString());
    return this.http.post<Answer>(`${environment.apiUrl}/answers/${answerId}/accept`, body).toPromise();
  }

  private updateAnswers(questionId: number) {
    this.getAnswersForQuestion(questionId).then(newAnswers => this.answers.next(newAnswers));
  }
}
