import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Question } from 'src/app/models/question/question.model';
import { environment } from 'src/environments/environment';
import { Payload } from 'src/app/models/payload.model';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  questions: Subject<Question[]> = new Subject<Question[]>();

  questionsProf: Subject<Question[]> = new Subject<Question[]>();

  professionalDirectQuestions: Subject<Question[]> = new Subject<Question[]>();

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  public postQuestion(question: Question): Promise<void | Question> {
    return this.http.post<Question>(`${environment.apiUrl}/questions`, question).toPromise()
    .then( () => this.updateQuestions())
    .then( () => this.updateProfessionalQuestions())
    .then( () => this.updateProfessionalDirectQuestions())
    .then(value => value);
  }

  public getQuestions(categoryIdFilters?: number[]): Promise<Question[]> {
    return this.http.get<Payload<Question[]>>(`${environment.apiUrl}/questions`).toPromise()
      .then(payload => payload === null ? [] : payload.payload);
  }

  private updateQuestions() {
    this.getQuestions().then(newQuestions => this.questions.next(newQuestions));
  }

  private updateProfessionalQuestions() {
    this.getProfessionalQuestions().then(newQuestions => this.questionsProf.next(newQuestions));
  }

  private updateProfessionalDirectQuestions() {
    if (this.auth.currentUserValue.role !== 'professional') {
      return;
    }
    this.getProfessionalDirectQuestions().then(newQuestions => this.professionalDirectQuestions.next(newQuestions));
  }

  public updateQuestion(question: Question): Promise<void | Question> {
    return this.http.patch<Question>(`${environment.apiUrl}/questions/${question.id}`, question)
      .toPromise().then( () => this.updateQuestions()).then(value => value);
  }

  public getQuestion(questionId: number): Promise<void | Question> {
    return this.http.get<Question>(`${environment.apiUrl}/questions/${questionId}`).toPromise().then(value => value);
  }

  public getProfessionalQuestions(categoryIdFilters?: number[]): Promise<Question[]> {
    return this.http.get<Payload<Question[]>>(`${environment.apiUrl}/questions/prof`).toPromise()
      .then(payload => payload === null ? [] : payload.payload);
  }

  public getProfessionalDirectQuestions(categoryIdFilters?: number[]): Promise<Question[]> {
    return this.http.get<Payload<Question[]>>(`${environment.apiUrl}/questions/professional`).toPromise()
      .then(payload => payload === null ? [] : payload.payload);
  }

  claimQuestion(questionId: number, shouldClaim: boolean): Observable<Question> {
    return this.http
      .post<Question>(`${environment.apiUrl}/questions/` + questionId + '/claim?shouldClaim=' + shouldClaim, {});
  }
}
