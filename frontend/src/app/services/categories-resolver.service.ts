import { Observable } from 'rxjs';
import { Category } from './../models/category.model';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CategoriesService } from './categories.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesResolverService implements Resolve<Category[]> {

  constructor(private categoriesService: CategoriesService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Category[]> | Observable<never> {
    console.log('Resolving...');
    return this.categoriesService.getCategories();
  }
}
