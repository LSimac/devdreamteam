# Contribution Guide

## Table of Contents
* [Branching](#branching)
    + [Branch naming](#branch-naming)
* [Commits](#commits)
    + [Commit Message](#commit-message)
        - [Message Format](#message-format)
        - [Message Subject](#message-subject)
            * [Example](#example)
        - [Message Body](#message-body)
            * [Example](#example-1)
        - [Message Footer](#message-footer)
            * [Breaking Change](#breaking-change)
                + [Example](#example-2)
            * [Referencing Issues](#referencing-issues)
                + [Example](#example-3)
            * [Footer Example](#footer-example)
        - [Commit Message Example](#commit-message-example)
* [Pull Requests](#pull-requests)
          
## Branching
Each logical chunk ("topic") you work on should have its 
own branch which should branch off of `develop` and 
then be merged back into `develop`. 

### Branch naming
Name your branches `<prefix>/<topic>`.

Topic is something you are working on e.g. "api-doc", "db" etc.
Use `-` to connect multiple words.

**Branch `<prefix>` values**
* **feat** (feature you're adding/extending)
* **bug** (bug fix you're working on)
* **junk** (throwaway branch created to experiment - DON'T push to remote)

Example of a branch name: `feat/api-doc`, `bug/memory-leak`.

## Commits
Keep your commits granular. If you do some part of the work
and what you implemented works, commit it.

If you find your commit subject contains 
`... add <insert> and change <insert> and ...`
that means that your commits are too big and you should
break it into multiple smaller commits.

### Commit Message
#### Message Format
```
<type>(<branch>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```
Footer and body are optional but ensure that the subject line describes
well enough changes you made.

#### Message Subject
Commit subject should roughly describe what the commit is about.

**Guidelines**
* Try not to make it longer than 60 characters
* Use imperative: "change" not "changed", "add" not "added"
* Do not capitalize first letter and don't end subject with '.'

**Allowed `<type>` values**
* **feat** (new feature for the user, not a new feature for build script)
* **fix** (bug fix for the user, not a fix to a build script)
* **docs** (changes to the documentation)
* **style** (formatting, missing semi colons, etc; no production code change)
* **refactor** (refactoring production code, eg. renaming a variable)
* **test** (adding missing tests, refactoring tests; no production code change)
* **chore** (updating grunt tasks etc; no production code change)
##### Example
```
chore(develop): move documentation to separate directory
```

#### Message Body
Explain what and why you changed something.

**Guidelines**
* Don't put empty lines in the body
* Use imperative

##### Example
```
Add one new dependency, use `range-parser` (Express dependency) to compute
range. It is more well-tested in the wild.
```

#### Message Footer
##### Breaking Change
If you make a breaking change put in footer the following.
```
BREAKING CHANGE: <explanation>
```
Always do this when you do a breaking change.

Each breaking change has to be mentioned. If you have more
put mention them one after another like so:
```
BREAKING CHANGE: <explanation>
BREAKING CHANGE: <explanation>
```
###### Example
```
BREAKING CHANGE: isolate scope bindings definition has changed and
the inject option for the directive controller injection was removed.

To migrate the code follow the example below:
Before:
    scope: {
      myAttr: 'attribute',
      myBind: 'bind',
      myExpression: 'expression',
      myEval: 'evaluate',
      myAccessor: 'accessor'
    }
After:
    scope: {
      myAttr: '@',
      myBind: '@',
      myExpression: '&',
      // myEval - usually not useful, but in cases where the expression is assignable, you can use '='
      myAccessor: '=' // in directive's template change myAccessor() to myAccessor
    }
```
##### Referencing Issues
If your change solves an issue(s) add to the first line of
the footer the following (on separate line). 
```
Closes #<issue_number_1>, #<issue_number_2>
```
###### Example
```
Closes #123, #225
```
##### Footer Example
```
Closes #123, #225
BREAKING CHANGE: isolate scope bindings definition has changed and
the inject option for the directive controller injection was removed.

To migrate the code follow the example below:
Before:
    scope: {
      myAttr: 'attribute',
      myBind: 'bind',
      myExpression: 'expression',
      myEval: 'evaluate',
      myAccessor: 'accessor'
    }
After:
    scope: {
      myAttr: '@',
      myBind: '@',
      myExpression: '&',
      // myEval - usually not useful, but in cases where the expression is assignable, you can use '='
      myAccessor: '=' // in directive's template change myAccessor() to myAccessor
    }
```

#### Commit Message Example
```
feat(feat/something): simplify isolate scope bindings

Changed the isolate scope binding options to:
  - @attr - attribute binding (including interpolation)
  - =model - by-directional model binding
  - &expr - expression execution binding

This change simplifies the terminology as well as
number of choices available to the developer. It
also supports local name aliasing from the parent.

Closes #234
BREAKING CHANGE: isolate scope bindings definition has changed and
the inject option for the directive controller injection was removed.
...
```

## Pull Requests
Before making a pull request ensure that the branch you
want to merge to e.g. `develop` branch is up to date 
with `develop`.

To do that first try to merge up to date `develop` branch
into your feature branch, resolve conflicts if necessary and than
make a pull request.

For merging develop into the feature branch use following:
```shell script
git checkout <feature-branch>
git merge develop
```

For creating a pull request you can use GitLab/GitHub's create 
pull request feature or use the command line.